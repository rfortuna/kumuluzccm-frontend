# kumuluzccm-frontend

To run the application in production do:
1. npm run build
2. NODE_PATH=./src NODE_ENV=production PORT=[desired port] APIPORT=[api port] APIHOST=[api host] KEYCLOAKHOST=[keycloak host] KEYCLOAKPORT=[keycloak port] npm run start-prod
   * replace [..] with proper values
