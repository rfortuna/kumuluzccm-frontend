FROM node:4-wheezy
RUN mkdir /app

WORKDIR /app

ADD ./ /app

EXPOSE 3003

CMD ["npm", "run", "start-prod"]