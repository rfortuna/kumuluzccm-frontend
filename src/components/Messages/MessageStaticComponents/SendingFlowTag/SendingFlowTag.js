
/**
 * Created by rokfortuna on 14/03/16.
 */

import React, {Component, PropTypes} from 'react';
import styles from './SendingFlowTag.scss';
import lodash from 'lodash';

export default class SendingFlowTag extends Component {

  static propTypes = {
    stepInfo: PropTypes.object
  }

  render() {
    const {stepInfo} = this.props;

    return (
      <div className="col-lg-2 text-center">
        {stepInfo.active &&
        <div>
          <div className={styles.circleActive}>
            <i className={`${styles.iconActive} fa ${stepInfo.icon}`}></i>
          </div>
          <p className={styles.labelActive}>{stepInfo.label}</p>
        </div>}
        {!stepInfo.active && !stepInfo.touched &&
        <div>
          <div className={styles.circleBasic}>
            <i className={`${styles.iconBasic} fa ${stepInfo.icon}`}></i>
          </div>
          <p className={styles.labelBasic}>{stepInfo.label}</p>
        </div>}
        {!stepInfo.active && stepInfo.touched && !lodash.isEmpty(stepInfo.errors) &&
        <div>
          <div className={styles.circleWarning}>
            <i className={`${styles.iconWarning} fa ${stepInfo.icon}`}></i>
          </div>
          <p className={styles.labelWarning}>{stepInfo.label}</p>
        </div>}
        {!stepInfo.active && stepInfo.touched && lodash.isEmpty(stepInfo.errors) &&
        <div>
          <div className={styles.circleSuccess}>
            <i className={`${styles.iconSuccess} fa ${stepInfo.icon}`}></i>
          </div>
          <p className={styles.labelSuccess}>{stepInfo.label}</p>
        </div>}
      </div>
    );
  }
}
