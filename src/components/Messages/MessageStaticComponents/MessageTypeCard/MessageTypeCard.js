
/**
 * Created by rokfortuna on 14/03/16.
 */

import React, {Component, PropTypes} from 'react';
import styles from './MessageTypeCard.scss';

import {toastr} from 'react-redux-toastr';


export default class MessageTypeCard extends Component {

  static propTypes = {
    messageType: PropTypes.object,
    click: PropTypes.func
  };

  render() {
    const {messageType, disabled} = this.props;

    let click;
    if (!disabled) {
      click = () => {
        this.props.click(messageType.id);
      };
    } else {
      click = () => {
        toastr.info('Sms sending name is not accepted yet. Please go to tenant settings.', {timeOut: 4000});
      }
    }

    return (
        <div className="ibox">
          <div className={`ibox-content product-box active ${styles.cardContainer} ${disabled && styles.cardDisabled}`}>

            <div className={`product-imitation ${styles.messageImage}`}>
              {messageType.icons.map((icon) =>
                <i className={`fa ${icon} clickable ${styles.icon} ${messageType.icons.length > 1 && styles.smallIcon}`} onClick={click} />
              )}
            </div>
            <div className="product-desc">
              <a href="#" className="product-name" onClick={click}>{messageType.label}</a>

              <div className="small m-t-xs" style={{height: '50px'}}>
                {messageType.description}
              </div>

            </div>
          </div>
          <a href="#" className={`btn btn-xs btn-outline btn-primary ${styles.buttonSend}`} onClick={click}>Send <i className="fa fa-long-arrow-right"></i> </a>
        </div>
    );
  }
}
