/**
 * Created by urbanmarovt on 11/02/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, ButtonToolbar, Button, Glyphicon} from 'react-bootstrap';

import { DatePicker, MultiSelect, SimpleSelect, TextEditor} from 'components';

import styles from './MessageNewForm.scss';

import { validateTimeStamp, minLength, empty } from 'utils/validation';

const validate = (values) => {

  const errors = {};

  if (values.receiversType === 'specific' && !values.receivers) {
    errors.receivers = 'Required';
  } else if (values.receiversType === 'group' && !values.receiversGroups) {
    errors.receiversGroups = 'Required';
  }

  if (!values.campaign) {
    errors.campaign = 'Required';
  }

  if (!empty(values.deliverAfter) && !validateTimeStamp(values.deliverAfter)) {
    errors.deliverAfter = 'Invalid date format';
  }

  if (!values.subject) {
    errors.subject = 'Required';
  } else if (minLength(5)(values.subject)) {
    errors.subject = 'Subject should be at least 5 characters long';
  }

  if (!values.contentShort) {
    errors.contentShort = 'Required';
  }

  if (!values.contentLong) {
    errors.contentLong = 'Required';
  }

  return errors;
};

@reduxForm({
  form: 'MessageNew',
  fields: ['receiversType', 'receivers', 'receiversGroups', 'ignoreReceiverPrefs', 'channels',
    'campaign', 'deliverAfter', 'subject', 'contentShort', 'contentLong'],
  validate: validate
})
export default class MessageNewForm extends Component {

  static propTypes = {
    allReceivers: PropTypes.array,
    allCampaigns: PropTypes.array,
    allChannels: PropTypes.array,
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    fields: PropTypes.object,
    submitting: PropTypes.bool,
    errors: PropTypes.object,
  }

//  componentWillReceiveProps(nextProps){
//    const {dispatch} = this.props;
//    dispatch(initialize)
//  }

  render() {
    const {fields: {receiversType, receivers, receiversGroups,
      ignoreReceiverPrefs, channels, campaign, deliverAfter, subject, contentShort, contentLong},
      handleSubmit, handleCancel, submitting, allReceivers, allChannels, allCampaigns, errors, ...props} = this.props;

    // validation
    const subjectValidation = {};
    const contentShortValidation = {};
    const contentLongValidation = {};


    if (subject.touched && subject.error) {
      subjectValidation.help = subject.error;
      subjectValidation.bsStyle = 'error';
    }

    if (contentShort.touched && contentShort.error) {
      contentShortValidation.help = contentShort.error;
      contentShortValidation.bsStyle = 'error';
    }

    if (contentLong.touched && contentLong.error) {
      contentLongValidation.help = contentShort.error;
      contentLongValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <div className="row">
          <div className="col-sm-2">
            <label className={styles.label}>Receivers:</label>
          </div>
          <div className="col-sm-10">
            <label>
              <Input type="radio" {...receiversType} label="Specific" labelClassName={styles.radioButton} value="specific" checked={receiversType.value === 'specific'}/>
            </label>
            <label>
              <Input type="radio" {...receiversType} label="Group" labelClassName={styles.radioButton} value="group" checked={receiversType.value === 'group'}/>
            </label>
            <label>
              <Input type="radio" {...receiversType} label="All" labelClassName={styles.radioButton} value="all" checked={receiversType.value === 'all'}/>
            </label>
            { receiversType.value === 'specific' &&
            <MultiSelect placeholder="Select users ..." wrapperClassName="col-xs-12" {...receivers} data={allReceivers}/>
            }
            { receiversType.value === 'group' &&
            <SimpleSelect placeholder="Select group ..." wrapperClassName="col-xs-12" {...receiversGroups} data={allReceivers}/>
            }
          </div>
        </div>

        <Input type="checkbox" label="Ignore receivers prefs" labelClassName="col-xs-offset-2 col-xs-10" {...ignoreReceiverPrefs}/>
        <MultiSelect placeholder="Select channel types ..." wrapperClassName="col-xs-10" labelClassName="col-xs-2"
                      label="Channel" {...channels} data={allChannels}/>
        <SimpleSelect placeholder="Select campaign ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Campaign" {...campaign} data={allCampaigns}/>
        <DatePicker labelClassName="col-xs-2" wrapperClassName="col-xs-10" label="Delivery schedule" {...deliverAfter} />
        <Input type="text" label="Subject" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...subject} {...subjectValidation} />
        <Input type="textarea" label="Short message" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...contentShort} {...contentShortValidation}/>
        <TextEditor {...contentLong} label="Long message" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...contentLongValidation} />
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
