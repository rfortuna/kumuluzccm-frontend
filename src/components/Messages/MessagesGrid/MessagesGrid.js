/**
 * Created by urbanmarovt on 10/02/16.
 */
import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';
import {DateLabel, ChannelTypeLabel, MessageStatusLabel} from 'components';
import {Pagination} from 'react-bootstrap';

export default class MessagesGrid extends Component {
  static propTypes = {
    messages: PropTypes.array.isRequired,
    onMessageClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number,
    onSortClick: PropTypes.func
  };

  render() {
    const {messages, onMessageClick, paginationOnPageChange, paginationPages, paginationActivePage, onSortClick} = this.props;
    return (
      <div style={{height: '500px'}}>
        <Table className="table" hover>
          <thead>
            <tr>
              <th onClick={() => onSortClick('deliveredAt')} className="clickable" >
                <strong className="name-header">Date sent &nbsp;<i className="fa fa-sort"></i></strong>
              </th>
              <th>
                <strong className="name-header">Receiver &nbsp;<i className="fa fa-sort"></i></strong>
              </th>
              <th>
                <strong className="name-header">Subject</strong>
              </th>
              <th>
                <strong className="name-header">Channel type</strong>
              </th>
              <th>
                <strong className="name-header">Status</strong>
              </th>
            </tr>
          </thead>
          <tbody>
            {
              messages.map(
                (message) =>
                  <tr className="clickable" onClick={() => onMessageClick(message.id)} key={message.id}>
                    <td>
                      <DateLabel date={message.deliveredAt}/>
                    </td>
                    <td>
                      {message.receiver.email}
                    </td>
                    <td>
                      {message.subject}
                    </td>
                    <td>
                      <ChannelTypeLabel type={message.channelType} />
                    </td>
                    <td>
                      <MessageStatusLabel type={message.deliveryStatus} />
                    </td>
                  </tr>
              )
            }
          </tbody>
        </Table>
        <div style={{textAlign: 'center'}}>
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={paginationPages}
            maxButtons={6}
            activePage={paginationActivePage}
            onSelect={paginationOnPageChange} />
        </div>
      </div>
    );
  }
}
