/**
 * Created by rokfortuna on 23/05/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {SimpleSelectPure} from 'components';


@reduxForm({
    form: 'TopNavProfileForm',
    fields: ['profile']
  },
  state => ({}))
export default class TopNavProfileForm extends Component {

  static propTypes = {
    onSelectProfile: PropTypes.func,
    fields: PropTypes.object,
    profileOptions: PropTypes.array
  };

  makeOnChangeSelect = (field) => {
    const {onSelectProfile} = this.props;
    return (value) => {
      field.onChange(value);
      onSelectProfile(value.value);
    };
  }

  render() {
    const {fields: {profile},  onSelectProfile, profileOptions} = this.props;

    return (
      <form>
        <SimpleSelectPure
        {...profile}
        data={profileOptions}
        onChange={this.makeOnChangeSelect(profile)}
        clearable={false}
        searchable={false}
        />
      </form>
    );
  }
}
