/**
 * Created by urbanmarovt on 22/02/16.
 */

import React, {Component, PropTypes} from 'react';
import { NavItem } from 'react-bootstrap';
import {Input} from 'react-bootstrap';
import {Link} from 'react-router';
import styles from './TopMenu.scss';
import _ from 'lodash';

import {refactorProfileOptions} from 'utils/refactor';
import {TopNavProfileForm} from 'components';

export default class TopMenu extends Component {

  static propTypes = {
    username: PropTypes.string,
    logout: PropTypes.func,
    showSideMenu: PropTypes.bool,
    profileOptions: PropTypes.array,
    onSelectProfile: PropTypes.func,
    selectedProfileId: PropTypes.string
  };

  render() {
    const { logout, profileOptions, onSelectProfile, selectedProfileId, showSideMenu} = this.props;
    const option = _.find(profileOptions, function(option) { return option.id === selectedProfileId });

    const profileImgSrc = option ? option.logo : '';

    return (
      <div className="row border-bottom">
        <nav className="navbar navbar-static-top white-bg" role="navigation" style={{marginBottom: 0}}>
          <div className="navbar-header">

            {showSideMenu &&
              <a className="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i className="fa fa-bars"></i>
              </a>
            }
            {!showSideMenu &&
              <Link to="/"><img src="/logo_dark.png" className={styles.logo} /></Link>
            }

          </div>

          <ul className="nav navbar-top-links navbar-right">
            <NavItem eventKey={1} href="#">
              { !profileImgSrc &&
                <i className={`fa fa-lg fa-user ${styles.icon}`} />
              }
              {
                profileImgSrc &&
                <img src={profileImgSrc} className={styles.profileImg} />
              }
            </NavItem>

            <NavItem eventKey={2} style={{width: '150px', height: '42px'}}>
              <TopNavProfileForm
                onSelectProfile={onSelectProfile}
                profileOptions={refactorProfileOptions(profileOptions)}
                initialValues={{profile: selectedProfileId}}
               />
            </NavItem>

            <NavItem eventKey={3} onClick={logout}>
              <i className={`fa fa-lg fa-sign-out ${styles.icon}`}/>
            </NavItem>
          </ul>

        </nav>
      </div>
    );
  }
}
