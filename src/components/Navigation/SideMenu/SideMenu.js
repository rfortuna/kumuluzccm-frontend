/**
 * Created by urbanmarovt on 22/02/16.
 */

import React, {Component, PropTypes} from 'react';

import {Link} from 'react-router';

const menuConfig = [
  {label: 'Dashboard', icon: 'fa-th-large', link: '/app/tenant'},
  {label: 'Campaigns', icon: 'fa-globe',
    children: [ {label: 'All campaigns', link: '/app/tenant/campaigns'} ,
                {label: 'New campaign', link: '/app/tenant/campaigns/new'},
                {label: 'New template', link: '/app/tenant/templates/new'}]},
  {label: 'Newsletters', icon: 'fa-envelope',
    children: [ {label: 'All newsletters', link: '/app/tenant/newsletters'} ,
                {label: 'New newsletter', link: '/app/tenant/newsletter/new'}, ]},
  {label: 'Receivers', icon: 'fa-users',
    children: [ {label: 'All receivers', link: '/app/tenant/receivers'},
                {label: 'New receiver', link: '/app/tenant/receivers/new'} ]},
  {label: 'Lists', icon: 'fa-list',
    children: [ {label: 'All lists', link: '/app/tenant/lists'},
                {label: 'New list', link: '/app/tenant/lists/new'} ]},
  {label: 'Apps', icon: 'fa-cube',
    children: [ {label: 'Products', link: '/app/tenant/products'},
                {label: 'New product', link: '/app/tenant/products/new'},
                {label: 'New platform', link: '/app/tenant/products/newPlatform'} ]},
  {label: 'Settings', icon: 'fa-cog', link: '/app/tenant/settings'}
];

// <li className="menu-nested-items">
//   <a href="#"><i className="fa fa-cube"></i> <span className="nav-label">Apps</span><span className="fa arrow"></span></a>
//   <ul className="nav nav-second-level collapse">
//     <li><Link to={`/app/tenant/products`}>Products</Link></li>
//     <li><Link to={`/app/tenant/products/new`}>New product</Link></li>
//     <li><Link to={`/app/tenant/products/newPlatform`}>New platform</Link></li>
//   </ul>
// </li>

export default class SideMenu extends Component {

  static propTypes = {
    pushState: PropTypes.func.isRequired,
    routerLocation: PropTypes.string
  };

  componentDidMount() {
    if (__CLIENT__) {
      window.$('#side-menu').metisMenu({
        preventDefault: true
      });
    }
  }

  render() {

    const {routerLocation} = this.props;

    return (
      <nav className="navbar-default navbar-static-side" role="navigation">
        <div className="sidebar-collapse">
          <ul className="nav metismenu" id="side-menu">
            <li className="nav-header">
              <div className="profile-element">
                <Link to="/"><img src="/logo.png" style={{height: 80}} /></Link>
                <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                  <span className="clear">
                    <span className="text-muted text-xs block" style={{fontSize: 12}} >Messaging simplified.</span>
                  </span>
                </a>
              </div>
            </li>

            {menuConfig.map((menuItem) => {
              if (menuItem.children) {
                let isActive = menuItem.children.find((child) => routerLocation.indexOf(child.link) > -1);
                return (
                  <li className={`menu-nested-items ${ isActive ? 'active' : ''}`}>
                    <a href="#"><i className={`fa ${menuItem.icon}`}></i> <span className="nav-label">{menuItem.label}</span><span className="fa arrow"></span></a>
                    <ul className={`nav nav-second-level collapse ${isActive ? 'in': ''}`}>
                      {menuItem.children.map((child) =>
                        <li className={`${routerLocation.indexOf(child.link) > -1  ? 'active' : ''}`}><Link to={child.link}>{child.label}</Link></li>
                      )}
                    </ul>
                  </li>);
                }
              else {
                return (
                  <li className={`menu-single-item ${ menuItem.link === routerLocation ? 'active': ''}`}>
                    <Link to={menuItem.link}>
                      <i className={`fa ${menuItem.icon}`}></i> <span className="nav-label">{menuItem.label}</span>
                    </Link>
                  </li>
                )
              }
            }
            )}
          </ul>
        </div>
      </nav>
    );
  }
}
