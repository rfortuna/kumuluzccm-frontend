/**
 * Created by rokfortuna on 3/24/16.
 */

/**
 * Created by urbanmarovt on 01/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';

const validate = (values) => {

  const errors = {};

  if (!values.username) {
    errors.username = 'Required';
  }

  if (!values.password) {
    errors.password = 'Required';
  }

  return errors;

};

@reduxForm({
  form: 'LoginForm',
  fields: ['username', 'password'],
  validate: validate
})
export default class LoginForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    authenticationData: PropTypes.object,
    fields: PropTypes.object,
    errors: PropTypes.object
  }

  render() {
    const {fields: {username, password}, authenticationData: {loggingIn}} = this.props;


    return (
      <form onSubmit={this.props.handleSubmit} className="m-t">
        <div className="form-group">
          <input type="text" {...username} className="form-control" placeholder="Username" />
        </div>
        <div className="form-group">
          <input type="password" {...password} className="form-control" placeholder="Password" />
        </div>

        <button type="submit" disabled={loggingIn} className="btn btn-primary block full-width m-b">
          {!loggingIn &&
            <span>Login</span>
          }
          {loggingIn &&
            <i className="fa fa-spinner fa-spin"></i>
          }
        </button>
        {/*

        <a href="#"><small>Forgot password?</small></a>
        <p className="text-muted text-center"><small>Do not have an account?</small></p>
        <a className="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
         */}
      </form>
    );
  }
}
