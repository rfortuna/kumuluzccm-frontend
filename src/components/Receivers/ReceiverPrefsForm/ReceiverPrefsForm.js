/**
 * Created by urbanmarovt on 07/03/16.
 */
/**
 * Created by urbanmarovt on 01/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, FormControls, ButtonToolbar, Glyphicon} from 'react-bootstrap';
import {DatePicker} from 'components';
import styles from './ReceiverPrefsForm.scss';

import { validateTimeStamp } from 'utils/validation';

const validate = (values) => {

  const errors = {};

  if (!values.from) {
    errors.from = 'Required';
  } else if (!validateTimeStamp(values.from)) {
    errors.from = 'Invalid time format';
  }

  if (!values.to) {
    errors.to = 'Required';
  } else if (!validateTimeStamp(values.to)) {
    errors.to = 'Invalid time format';
  }

  return errors;
};

@reduxForm({
  form: 'ReceiverPrefsForm',
  fields: ['channelType', 'from', 'to', 'monday', 'tuesday', 'wednesday', 'thursday',
    'friday', 'saturday', 'sunday', 'disabled'],
  validate: validate
})
export default class ReceiversPrefsForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func,
    handleCancel: PropTypes.func,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object,
    initialize: PropTypes.func
  };

  render() {
    const {fields: {channelType, from, to, monday, tuesday, wednesday,
      thursday, friday, saturday, sunday, disabled},
      handleSubmit, handleCancel, submitting, errors} = this.props;

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <FormControls.Static label="Channel type" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={channelType.value}/>
        <div className="row">
          <div className="col-sm-2">
            <label className={styles.label}>Deliver days</label>
          </div>
          <div className="col-sm-10">
            <label>
              <Input type="checkbox" {...monday} label="Monday" labelClassName={styles.checkbox}/>
            </label>
            <label>
              <Input type="checkbox" {...tuesday} label="Tuesday" labelClassName={styles.checkbox} />
            </label>
            <label>
              <Input type="checkbox" {...wednesday} label="Wednesday" labelClassName={styles.checkbox}/>
            </label>
            <label>
              <Input type="checkbox" {...thursday} label="Thursday" labelClassName={styles.checkbox}/>
            </label>
            <label>
              <Input type="checkbox" {...friday} label="Friday" labelClassName={styles.checkbox} />
            </label>
            <label>
              <Input type="checkbox" {...saturday} label="Saturday" labelClassName={styles.checkbox}/>
            </label>
            <label>
              <Input type="checkbox" {...sunday} label="Sunday" labelClassName={styles.checkbox}/>
            </label>
          </div>
        </div>
        <DatePicker mode="time" inputFormat="HH:mm" labelClassName="col-xs-2" wrapperClassName="col-xs-10" label="Delivery schedule" {...from} />
        <DatePicker mode="time" inputFormat="HH:mm" labelClassName="col-xs-2" wrapperClassName="col-xs-10" label="Deliver before" {...to} />
        <Input type="checkbox" {...disabled} label="Disabled" labelClassName="col-xs-offset-2 col-xs-10"/>
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
