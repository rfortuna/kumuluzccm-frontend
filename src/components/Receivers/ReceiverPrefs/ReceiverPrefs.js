/**
 * Created by urbanmarovt on 07/03/16.
 */

import React, {Component, PropTypes} from 'react';

import {ReceiverPref} from 'components';

import Sortable from 'react-anything-sortable';


export default class ReceiversPrefs extends Component {

  static propTypes = {
    receiverPrefs: PropTypes.array.isRequired,
    onCreateClick: PropTypes.func,
    onUpdateClick: PropTypes.func,
    handleSort: PropTypes.func
  };

  render() {
    const {receiverPrefs, onCreateClick, onUpdateClick, handleSort} = this.props;

    return (
      <Sortable onSort={handleSort}>
        {receiverPrefs.map(
          (preference, index) =>
            <ReceiverPref sortData={preference} receiverPref={preference} sort={this.sort} key={index}
                          onCreateClick={onCreateClick}
                          onUpdateClick={onUpdateClick}/>
        )}
      </Sortable>
    );
  }
}
