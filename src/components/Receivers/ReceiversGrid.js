/**
 * Created by urbanmarovt on 10/02/16.
 */
import React, {Component, PropTypes} from 'react';
import {Pagination, Table} from 'react-bootstrap';

export default class ReceiversGrid extends Component {
  static propTypes = {
    receivers: PropTypes.array.isRequired,
    onReceiverClick: PropTypes.func.isRequired,
    paginationOnPageChange: PropTypes.func.isRequired,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number,
    onSortClick: PropTypes.func,
    actions: PropTypes.func
  };

  render() {
    const {receivers, onReceiverClick, paginationOnPageChange, paginationPages, paginationActivePage, onSortClick, actions} = this.props;

    return (
      <div style={{marginBottom: '70px'}}>
        <Table className="table" hover>

          <thead>
            <tr>
              <th onClick={() => onSortClick('lastName')} className="clickable">
                <strong className="name-header">Name &nbsp; <i className="fa fa-sort"></i></strong>
              </th>
              <th>
                <strong className="name-header">Email</strong>
              </th>
              <th>
                <strong className="name-header">Mobile number</strong>
              </th>
              { actions &&
                <th>
                  <strong className="name-header">Actions</strong>
                </th>
              }
            </tr>
          </thead>
          <tbody>
          {
            receivers.map(
              (receiver) =>
                <tr onClick={() => onReceiverClick(receiver.id)} key={receiver.id} className="clickable">
                  <td>
                    {` ${receiver.lastName}, ${receiver.firstName}`}
                  </td>
                  <td>
                    {receiver.email}
                  </td>
                  <td>
                    {receiver.mobilePhone}
                  </td>
                  { actions &&
                    <td>
                    {actions(receiver)}
                    </td>
                  }
                </tr>
            )
          }
          </tbody>
        </Table>
        <div style={{textAlign: 'center'}}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          items={paginationPages}
          maxButtons={5}
          activePage={paginationActivePage}
          onSelect={paginationOnPageChange} />
          </div>
      </div>
    );
  }
}
