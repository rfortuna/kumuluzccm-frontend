
/**
 * Created by urbanmarovt on 29/02/16.
 */

import React, {Component, PropTypes} from 'react';
import __ from 'underscore.string';

export default class ChannelStatistics extends Component {

  static propTypes = {
    label: PropTypes.string,
    channelCount: PropTypes.number,
    channelPercent: PropTypes.number
  }

  render() {
    const {label, channelCount, channelPercent} = this.props;
    return (
      <div>
        <h2 className="no-margins">{channelCount}</h2>
        <small>{label}</small>
        <div className="stat-percent">{`${__.numberFormat(channelPercent, 1, ',', ',')} %`} <i className="fa fa-level-up text-navy"></i></div>
        <div className="progress progress-mini">
          <div style={{width: channelPercent + '%'}} className="progress-bar"></div>
        </div>
      </div>
    );
  }
}


