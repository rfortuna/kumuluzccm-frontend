/**
 * Created by rokfortuna on 02/03/16.
 */

import React, {Component, PropTypes} from 'react';
import __ from 'underscore.string';

export default class MessageStatistic extends Component {

  static propTypes = {
    label: PropTypes.string,
    totalCount: PropTypes.number,
    statisticCount: PropTypes.number,
    lowerBound: PropTypes.number,
    upperBound: PropTypes.number
  }


  render() {
    const {label, totalCount, statisticCount, lowerBound, upperBound} = this.props;
    const perc = totalCount === 0 ? 0 : statisticCount / totalCount * 100;
    return (
        <div>
          {(perc >= upperBound || totalCount === 0 ) &&
          <div className="stat-percent font-bold text-navy">
            {`${__.numberFormat(perc, 1, ',', ',')} % `}
            <i className="fa fa-check"></i></div>}
          {perc > lowerBound && perc < upperBound && totalCount !== 0 &&
          <div className="stat-percent font-bold text-warning">
            {`${__.numberFormat(perc, 1, ',', ',')} % `}
            <i className="fa fa-warning"></i></div>}
          {perc <= lowerBound && totalCount !== 0 &&
          <div className="stat-percent font-bold text-danger">
            {`${__.numberFormat(perc, 1, ',', ',')} % `}
            <i className="fa fa-times"></i></div>}
          <small>{label}</small>
        </div>
    );
  }
}


