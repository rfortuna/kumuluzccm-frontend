/**
 * Created by rokfortuna on 25/02/16.
 */

import React, {Component, PropTypes} from 'react';
import __ from 'underscore.string';
import MessageStatistic from './MessageStatistic';

export default class MessageStatistics extends Component {

  static propTypes = {
    title: PropTypes.string,
    totalCount: PropTypes.number,
    clickedCount: PropTypes.number,
    openedCount: PropTypes.number
  }


  render() {
    const {title, totalCount, clickedCount, openedCount} = this.props;
    return (
      <div className="ibox float-e-margins">
        {title &&
        <div className="ibox-title">
          <h5>{title}</h5>
        </div>}
        <div className="ibox-content">
          <h1 className="m-b-sm m-t-none m-l-none m-r-none">{__.numberFormat(totalCount, 0, '.', ',')}</h1>
          <MessageStatistic label="Opened:" statisticCount={openedCount} totalCount={totalCount} upperBound={10} lowerBound={1} />
          <MessageStatistic label="Delivered:" statisticCount={clickedCount} totalCount={totalCount} upperBound={5} lowerBound={0.5} />
        </div>
      </div>
    );
  }
}


