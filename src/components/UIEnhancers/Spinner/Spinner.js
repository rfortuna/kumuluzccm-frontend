/**
* Created by rokfortuna on 29/06/16.
*/

import React, {Component, PropTypes} from 'react';
import styles from './Spinner.scss';

export default class Spinner extends Component {

render() {
  return (
      <div className={styles.spinner}/>
  );
}
}
