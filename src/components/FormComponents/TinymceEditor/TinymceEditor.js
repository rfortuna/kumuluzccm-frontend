/**
 * Created by rokfortunar on 27/06/16.
 */

import React, {Component, PropTypes} from 'react';
if (__CLIENT__) {
  import tinymce from 'tinymce/tinymce';
  import 'tinymce/themes/modern/theme';
  import 'tinymce/plugins/paste/plugin'
  import 'tinymce/plugins/link/plugin';
  import 'tinymce/plugins/autoresize/plugin';
  import 'tinymce/plugins/emoticons/plugin';
  import 'tinymce/plugins/fullpage/plugin';
  import 'tinymce/plugins/fullscreen/plugin';
  import 'tinymce/plugins/colorpicker/plugin';
  import 'tinymce/plugins/autoresize/plugin';
  import 'tinymce/plugins/hr/plugin';
  import 'tinymce/plugins/insertdatetime/plugin';
  import 'tinymce/plugins/layer/plugin';
  import 'tinymce/plugins/link/plugin';
  import 'tinymce/plugins/media/plugin';
  import 'tinymce/plugins/print/plugin';
  import 'tinymce/plugins/preview/plugin';
  import 'tinymce/plugins/table/plugin';
  import 'tinymce/plugins/image/plugin';
  import 'tinymce/plugins/textcolor/plugin';
  import 'tinymce/plugins/visualblocks/plugin';
  import 'tinymce/plugins/visualchars/plugin';
  import 'tinymce/plugins/contextmenu/plugin';
  // import 'tinymce/plugins/wordcount/plugin';
}

import styles from './TinymceEditor.scss';

export default class TinymceEditor extends Component {

  static propTypes = {
    visible: PropTypes.bool,
    content: PropTypes.string,
    saveContentToStore: PropTypes.func
  };

  renderTinymce = () => {
    const {content} = this.props;
    // check in case of switching page & timeout
    if (window.$('#tinymce-editor')){
      tinymce.init({
        selector: '#tinymce-editor',
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | fullscreen',
        image_advtab: true,
        plugins: ['paste', 'link', 'autoresize', 'autoresize', 'emoticons',
                  'fullpage', 'fullscreen', 'colorpicker', 'image', 'link',
                  'media', 'print', 'preview', 'table', 'textcolor', 'visualblocks',
                  'visualchars', 'contextmenu' /*'wordcount'*/]
      });
      setTimeout(()=> {
        if (tinymce.get('tinymce-editor')) {
          tinymce.get('tinymce-editor').setContent(content);
        }
        else {
          this.renderTinymce();
        }
      }, 100);
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    if (__CLIENT__ && nextProps.visible !== this.props.visible) {
      if (!nextProps.visible) {
        this.saveContent();
        window.$('.mce-tinymce').hide();
      }else {
        window.$('.mce-tinymce').show();
      }
      return false;
    }
    return true;
  }

  componentWillUnmount() {
    let editor = tinymce.get('tinymce-editor');
    if (editor) {
      this.saveContent();
      editor.hide();
    }
    tinymce.EditorManager.editors = [];
  }

  saveContent = () => {
    const {saveContentToStore} = this.props;
    let editor = tinymce.get('tinymce-editor');
    if (editor) {
      saveContentToStore(editor.getContent())
    }
  }

  render() {
    const {visible} = this.props;

    if(__CLIENT__) {
      this.renderTinymce();
    }

    return (
      <textarea id="tinymce-editor" className={styles.tinymce}>
      </textarea>
    );
  }
}
