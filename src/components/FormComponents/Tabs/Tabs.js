/**
 * Created by rokfortuna on 11/07/16.
 */

import React, {Component, PropTypes} from 'react';


export default class Tabs extends Component {

  static propTypes = {
    changeFieldValue: PropTypes.func,
    field: PropTypes.object,
    formName: PropTypes.string,
    options: PropTypes.array,
    onFormStateChange: PropTypes.func,
    onTabChange: PropTypes.func
  };

  changeActiveTab = (value) => {
    return () => {

      const {field, formName, changeFieldValue, onFormStateChange, onTabChange} = this.props;
      changeFieldValue(formName, field.name, value );
      if (onFormStateChange) {
        onFormStateChange(field, value);
      }
      if (onTabChange) {
        onTabChange();
      }
    };
  };

  render() {
    const {options, field} = this.props;

    return (
      <div className="tabs-container">
        <ul role="tablist" className="nav nav-tabs">
          {options.map((option) =>
            <li role="presentation" className={field.value === option.value && 'active'} onClick={this.changeActiveTab(option.value)}>
              <a> {option.label} </a>
            </li>
          )}
        </ul>
        <div className="tab-content">
          <div role="tabpanel" className="tab-pane active">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}
