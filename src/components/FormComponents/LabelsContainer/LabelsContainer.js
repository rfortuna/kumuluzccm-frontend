/**
 * Created by urbanmarovt on 21/05/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

import styles from './LabelsContainer.scss';

export default class LabelsContainer extends Component {

  static propTypes = {
    label: PropTypes.string,
    labelClassName: PropTypes.string,
    wrapperClassName: PropTypes.string
  };

  render() {
    const {labelClassName, wrapperClassName, label} = this.props;
    return (
      <div className={`${styles.marginBottom} row`}>
        <div className={labelClassName}>
          <label className={`${styles.label}`}>{ label }</label>
        </div>
        <div className={`${styles.marginTop} ${wrapperClassName}`}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
