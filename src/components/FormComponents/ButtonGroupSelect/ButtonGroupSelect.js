/**
 * Created by rokfortuna on 27/06/16.
 */

import React, {Component, PropTypes} from 'react';


export default class ButtonGroupSelect extends Component {

  static propTypes = {
    changeFieldValue: PropTypes.func,
    field: PropTypes.object,
    formName: PropTypes.string,
    options: PropTypes.array,
    onFormStateChange: PropTypes.func
  };

  changeActiveButton = (value) => {
    return () => {

      const {field, formName, changeFieldValue, onFormStateChange} = this.props;
      changeFieldValue(formName, field.name, value );
      if (onFormStateChange) {
        onFormStateChange(field, value);
      }
    };
  };

  render() {
    const {options, field} = this.props;

    return (
      <div className="btn-group">
        {options.map((option)=>
          <button key={`button_group_${option.value}`} className={`btn ${field.value === option.value && 'btn-primary' || 'btn-white' }`} type="button" onClick={this.changeActiveButton(option.value)}>{option.label}</button>)
        }
      </div>
    );
  }
}
