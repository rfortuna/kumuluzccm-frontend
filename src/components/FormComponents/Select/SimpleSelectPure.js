/**
 * Created by urbanmarovt on 11/02/16.
 */

import React, {Component, PropTypes} from 'react';
import Select from 'react-select';

import styles from './Selects.scss';

export default class SimpleSelectPure extends Component {

  static propTypes = {
    data: PropTypes.array,
    onBlur: PropTypes.func,
    touched: PropTypes.bool,
    error: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string
  };

  selectOnChange = (value) => {
    this.setState({value});
    this.props.onChange(value);
  }
  render() {
    const {onChange, data, value, onBlur, error, touched, handleOnChange, ...props} = this.props;

    return (
          <Select
            {...props}
            className={styles.select}
            onChange={this.selectOnChange}
            value={value}
            onBlur={() => onBlur(value)}
            options = {data}
          />
    );
  }
}
