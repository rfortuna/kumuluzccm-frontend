/**
 * Created by rokfortuna on 3/24/16.
 */

import React, {Component, PropTypes} from 'react';
import styles from './DropzoneCustom.scss';
import Dropzone from 'react-dropzone';
import _ from 'underscore';

export default class DropzoneCustom extends Component {

  static propTypes = {
    onDrop: PropTypes.func,
    label: PropTypes.string,
    field: PropTypes.object,
    file: PropTypes.object,
    accept: PropTypes.string
  }

  render() {
    const {field, onDrop, label, file, accept} = this.props;
    return (
      <Dropzone
        className={styles.dropzone}
        { ...field }
        onDrop={ onDrop }
        multiple={false}
        accept={accept || ''}>
        <div className={styles.label}>
          {_.isEmpty(file) && label}
          {!_.isEmpty(file) && `File selected: ${file.name}`}
        </div>
      </Dropzone>
    );
  }
}
