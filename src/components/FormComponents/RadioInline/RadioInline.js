/**
 * Created by rokfortuna on 5/11/16.
 */

import React, {Component, PropTypes} from 'react';
import {Input} from 'react-bootstrap';
import styles from './RadioInline.scss';

export default class RadioInline extends Component {

  static propTypes = {
    field: PropTypes.object,
    label: PropTypes.string,
    options: PropTypes.array,
    onChange: PropTypes.func
  };

  render() {
    const {field, label, options, onChange} = this.props;

    return (
      <div className="form-group">
        <label className="col-sm-2 control-label">{label}</label>
        <div className={`col-sm-10 ${styles.paddingLeft}`}>
          {options.map((option)=>
            <label>
              <Input type="radio" {...field} label={option.label}
                     labelClassName={styles.radioButton}
                     value={option.value} checked={field.value === option.value}
                     onChange={onChange ? onChange : (event) => {field.onChange(event)}}
              />
            </label>
          )}
        </div>
      </div>
    );
  }
}
