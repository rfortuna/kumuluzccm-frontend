/**
 * Created by urbanmarovt on 12/05/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon, FormControls} from 'react-bootstrap';

import {DropzoneCustom, SimpleSelect, Iframe} from 'components';

import styles from './TemplateNewForm.scss';

import $ from 'jquery';

const validate = (values) => {

  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  }

  if (!values.campaign) {
    errors.campaign = 'Required';
  }

  if (!values.contentType) {
    errors.contentType = 'Required';
  }

  if ((values.contentType === 'SMS' || values.contentType === 'PUSH_NOTIFICATION') && !values.contentShort) {
    errors.contentShort = 'Required';
  }

  if(values.contentType === 'EMAIL' && !values.contentEMAIL) {
    errors.contentEMAIL = 'Required';
  }

  return errors;
};

@reduxForm({
    form: 'TemplateForm',
    fields: ['name', 'contentType', 'contentShort', 'contentEMAIL', 'campaign'],
    validate: validate
  },
  state => ({
  }))
export default class TemplateNewForm extends Component {

  static propTypes = {
    allCampaigns: PropTypes.array,
    handleSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object,
    handleCancel: PropTypes.func,
    templateFile: PropTypes.object,
    showContentPreview: PropTypes.func,
    handleFileClear: PropTypes.func,
    templateTypes: PropTypes.array
  };

  onDrop = ( filesToUpload ) => {
    const {showContentPreview, fields: {contentEMAIL}} = this.props;
    contentEMAIL.onChange(filesToUpload);
    $.get( filesToUpload[0].preview, ( data ) => {
      showContentPreview(data, filesToUpload[0]);
    });
  };

  render() {
    const {fields: {name, campaign, contentType, contentShort, contentEMAIL}, templateTypes, handleSubmit, submitting, errors, handleCancel, allCampaigns, templateFile, contentPreview, handleFileClear} = this.props;

    // validation
    const nameValidation = {},
      contentShortValidation = {};

    if (name.touched && name.error) {
      nameValidation.help = name.error;
      nameValidation.bsStyle = 'error';
    }

    if (contentShort.touched && contentShort.error) {
      contentShortValidation.help = contentShort.error;
      contentShortValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <Input type="text" label="Name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...name} {...nameValidation} />
        <SimpleSelect placeholder="Select campaign ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Campaign" {...campaign} data={allCampaigns}/>
        <SimpleSelect placeholder="Select template type ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Template type" {...contentType} data={templateTypes}/>
        {(contentType.value === 'SMS' || contentType.value === 'PUSH_NOTIFICATION') &&
        <Input type="textarea" label="Short message" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...contentShort} {...contentShortValidation}/>}
        { contentType.value === 'EMAIL' &&
        <div className={`${styles.dropzone} row`}>
          <div className="col-lg-2">
            <label className={`${styles.label}`}>Long message</label>
          </div>
          <div className="col-lg-10">
            <DropzoneCustom
              onDrop={ this.onDrop }
              label={'Drop the HTML template here, or click to upload it.'}
              field={contentEMAIL}
              file={templateFile}/>
          </div>
        </div> }
        {contentType.value === 'EMAIL' && !contentEMAIL.error &&
        <div>
          <div className="row">
            <div className="col-xs-offset-2 col-xs-10">
              <ButtonToolbar>
                <Button type="button" onClick={handleFileClear}>
                  <Glyphicon glyph="remove" />&nbsp;Clear
                </Button>
              </ButtonToolbar>
            </div>
          </div>
          <FormControls.Static label="Preview" labelClassName="col-xs-2"
                               wrapperClassName="col-xs-10">
            <Iframe content={contentPreview} />
          </FormControls.Static>
        </div>}
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button onClick={()=> handleCancel()}>
                <Glyphicon glyph="remove" />&nbsp;Cancel
              </Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
