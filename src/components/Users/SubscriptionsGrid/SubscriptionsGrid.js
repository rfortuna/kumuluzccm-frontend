/**
 * Created by urbanmarovt on 31/03/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';
import {Pagination} from 'react-bootstrap';

export default class SubscriptionsGrid extends Component {
  static propTypes = {
    subscriptions: PropTypes.array.isRequired,
    onSubscriptionClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number,
    onSortClick: PropTypes.func
  };

  render() {
    const {subscriptions, onSubscriptionClick, paginationOnPageChange, paginationPages, paginationActivePage} = this.props;
    return (
      <div style={{height: '500px'}}>
        <Table className="table" hover>
          <thead>
          <tr>
            <th>
              <strong className="name-header">Tenant name </strong>
            </th>
            <th>
              <strong className="name-header">Tenant logo </strong>
            </th>
            <th>
              <strong className="name-header">Subscribed email</strong>
            </th>
            <th>
              <strong className="name-header">Subscribed mobile number</strong>
            </th>
          </tr>
          </thead>
          <tbody>
          {
            subscriptions.map(
              (subscription) =>
                <tr className="clickable" onClick={() => onSubscriptionClick(subscription.tenant.id)} key={subscription.tenant.id}>
                  <td>
                    {subscription.tenant.name}
                  </td>
                  <td>
                    <img src={subscription.tenant.logo} style={{height: 20}} />
                  </td>
                  <td>
                    {subscription.email}
                  </td>
                  <td>
                    {subscription.mobilePhone}
                  </td>
                </tr>
            )
          }
          </tbody>
        </Table>
        <div style={{textAlign: 'center'}}>
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={paginationPages}
            maxButtons={6}
            activePage={paginationActivePage}
            onSelect={paginationOnPageChange} />
        </div>
      </div>
    );
  }
}
