/**
 * Created by urbanmarovt on 08/04/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon, FormControls} from 'react-bootstrap';

import { validateTimeStamp, validateEmail, minLength, empty } from 'utils/validation';

import { SimpleSelect } from 'components';
import {messageTypes} from 'utils/constants';

import _ from 'underscore';

const staticFields = ['product', 'channelId', 'mailFrom', 'mailUser', 'smsUser'];

const validate = (values) => {

  const errors = {};

  if (!values.channelId) {
    return {channelId: "ChannelId is missing!"};
  }

  const messageType = messageTypes.find((type) => {
    return type.id === values.channelId
  });

  if (messageType.email && !values.mailFrom) {
    errors.mailFrom = 'Required';
  } else if (messageType.email && validateEmail(values.mailFrom)) {
    errors.mailFrom = 'Invalid email format';
  }

  if (messageType.email && !values.mailUser) {
    errors.mailUser = 'Required';
  }

//  if (messageType.sms && !values.smsUser) {
//    errors.smsUser = 'Required';
//  }

  if (messageType.push) {
    let dynamicFields = Object.keys(values).filter((key) => !staticFields.find((item) => item === key));
    // not all are undefined and there is no field containing value
    if (!dynamicFields.every((key) => values[key] === undefined) && !dynamicFields.find((key) => values[key]) ) {
      errors.product = 'At least one platform must be selected.';
    }
  }

  if (messageType.push && (!values.product || values.product.length == 0)) {
    errors.product = 'Required';
  }

  return errors;
};

function reformatPrefillData(initialValues, channelId, campaign, tenant) {
  if (_.isEmpty(initialValues)) return {
    channelId: channelId,
    mailFrom: campaign.mailFrom || tenant.mailFrom,
    mailUser: campaign.mailUser || tenant.mailUser
//    smsUser: campaign.smsUser
  };
  const formattedData = {
    ...initialValues,
    channelId: channelId,
    mailFrom: campaign.mailFrom || tenant.mailFrom,
    mailUser: campaign.mailUser || tenant.mailUser
  };
  return formattedData;
}

@reduxForm({
    form: 'SendingChannelSetup',
    validate: validate
  },
  state => ({
    initialValues: reformatPrefillData(state.views.sending.channelSetup.data, state.router.location.query.channelId,
      state.models.campaigns.details.data,  state.models.tenants.details.data)
  }))
export default class SendingChannelSetupForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object,
    fields: PropTypes.object,
    handleForm: PropTypes.func,
    handleNext: PropTypes.func,
    allProducts: PropTypes.array,
    handleNotificationProductSelected: PropTypes.func,
    campaign: PropTypes.object,
    tenant: PropTypes.object,
    addPushCustomProperty: PropTypes.func,
    removePushCustomProperty: PropTypes.func,
    numOfCustomProperties: PropTypes.number,
    pushCustomPropertiesString: PropTypes.string
  };

  componentWillUnmount() {
    const {fields, errors, handleForm, numOfCustomProperties} = this.props;
    const data = {};

    Object.keys(fields).forEach((name) => {
      data[name] = fields[name].value
    });

    let pushCustomProperties = {};
    for (let i = 0; i < numOfCustomProperties; i++) {
      let key = this.refs[`pushCustomProperty_key_${i}`].value;
      let value = this.refs[`pushCustomProperty_value_${i}`].value;
      pushCustomProperties[key] = value;
    }
    data.pushCustomPropertiesString = JSON.stringify(pushCustomProperties);

    handleForm(data, errors, Object.keys(fields));
  }


  //set initial values for dynamic fields
  componentDidMount() {
    const {pushCustomPropertiesString, numOfCustomProperties} = this.props;
    let values = JSON.parse(pushCustomPropertiesString);

    // hack due to this.refs being unavailable in componentDidMount (REACT ISSUE)
    setTimeout(()=> {
      Object.keys(values).forEach((key, index) => {
        this.refs[`pushCustomProperty_key_${index}`].value = key ;
        this.refs[`pushCustomProperty_value_${index}`].value = values[key] ;
      });
      }, 10);
  }

  render() {
    const {fields, fields: {product, mailFrom, mailUser, channelId},
    handleNext, errors, allProducts, handleNotificationProductSelected,
    tenant, addPushCustomProperty, removePushCustomProperty ,numOfCustomProperties} = this.props;

    const mailFromValidation = {},
      mailUserValidation = {},
      smsUserValidation = {};

    if (mailFrom.touched && mailFrom.error) {
      mailFromValidation.help = mailFrom.error;
      mailFromValidation.bsStyle = 'error';
    }

    if (mailUser.touched && mailUser.error) {
      mailUserValidation.help = mailUser.error;
      mailUserValidation.bsStyle = 'error';
    }

//    if (smsUser.touched && smsUser.error) {
//      smsUserValidation.help = smsUser.error;
//      smsUserValidation.bsStyle = 'error';
//    }

    const messageType = messageTypes.find((type) => {
      return type.id === channelId.value
    });

    return (
      <form className="form-horizontal">
        {messageType && messageType.email &&
          <div>
            <Input type="text" label="Sending email" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
              {...mailFrom} {...mailFromValidation} />
            <Input type="text" label="Sending email name" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
              {...mailUser} {...mailUserValidation} />
          </div>
        }
        {messageType && messageType.sms &&
          <FormControls.Static label="Sending sms name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.smsUser}/>
//          <Input type="text" label="Sending sms name" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
//          {...smsUser} {...smsUserValidation} />
        }
        {messageType && messageType.push &&
          <div>
            <SimpleSelect placeholder="Select product ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Product" {...product} data={allProducts} handleOnChange={handleNotificationProductSelected}/>
            {
              Object.keys(fields).map((name, index) => {
                if(index < 4) {
                  return;
                }
                const field = fields[name];
                return (<Input key={`platform_${index}`}type="checkbox" label={name} labelClassName="col-xs-offset-2 col-xs-10" {...field}/>);
              })
            }
            <p>{errors.chosenPlatforms}</p>
            <FormControls.Static label="Custom properties" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
              <Button type="button" onClick={addPushCustomProperty}>
                <Glyphicon glyph="plus" />&nbsp;Add
              </Button>
              &nbsp;
              <Button type="button" onClick={removePushCustomProperty} disabled={numOfCustomProperties == 0}>
                <Glyphicon glyph="minus" />&nbsp;Remove
              </Button>
              <br/>
              <small>Enter the properties (key-value pairs), that your app consumes to enhance the user experience.</small>
            </FormControls.Static>
            {
              Array(numOfCustomProperties).fill(1).map((el, index) =>
              <div className="row" key={`pushCustomProperty_${index}`} style={{marginBottom: '5px'}}>
                <div className="col-xs-offset-2 col-xs-10">
                  <span>
                    <input type="text" className="form-control" ref={`pushCustomProperty_key_${index}`} style={{width: '200px', display: 'inline-block'}} />
                  </span>
                   &nbsp;:&nbsp;
                   <span>
                    <input type="text" className="form-control" ref={`pushCustomProperty_value_${index}`} style={{width: '200px', display: 'inline-block'}}/>
                   </span>
                </div>
              </div>
            )}
          </div>

        }
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="button" disabled={Object.keys(errors).length !== 0} onClick={handleNext}>
                <Glyphicon glyph="save" />&nbsp;Next
              </Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
