/**
 * Created by urbanmarovt on 15/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm, change} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';
import _ from 'underscore';

import { validateTimeStamp, minLength, empty } from 'utils/validation';
import { DatePicker, SimpleSelect} from 'components';

const validate = (values) => {

  const errors = {};

  if (!values.campaign) {
    errors.campaign = 'Required';
  }

  if (!empty(values.deliverAfter) && !validateTimeStamp(values.deliverAfter)) {
    errors.deliverAfter = 'Invalid date format';
  }

  if (!values.subject) {
    errors.subject = 'Required';
  } else if (minLength(5)(values.subject)) {
    errors.subject = 'Subject should be at least 5 characters long';
  }


  return errors;
};

function reformatPrefillData(initialValues) {
  if (_.isEmpty(initialValues)) return {};
  const formattedData = {
    ...initialValues
  };
  return formattedData;
}

@reduxForm({
  form: 'SetupForm',
  fields: ['campaign', 'subject', 'deliverAfter', 'ignoreReceiverPrefs', 'timezone'],
  validate: validate
},
state => ({
  initialValues: reformatPrefillData(state.views.sending.setup.data)
}))
export default class SendingSetupForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object,
    fields: PropTypes.object,
    handleForm: PropTypes.func,
    handleNext: PropTypes.func,
    allCampaigns: PropTypes.array,
    allTimezones: PropTypes.array
  };

  resetDeliverAfter = () => {
    this.props.dispatch(change('SetupForm', 'deliverAfter', null))
  };

  componentWillUnmount() {
    const {fields: {campaign, subject, deliverAfter, ignoreReceiverPrefs, timezone}, errors, handleForm} = this.props;

    handleForm(
      { campaign: campaign.value,
        subject: subject.value,
        deliverAfter: deliverAfter.value,
        ignoreReceiverPrefs: ignoreReceiverPrefs.value,
        timezone: timezone.value
      }, errors);
  }


  render() {
    const {
      fields: {campaign, subject, deliverAfter, ignoreReceiverPrefs, timezone},
      handleNext, errors, allCampaigns, allTimezones} = this.props;
    // validation
    const subjectValidation = {};

    if (subject.touched && subject.error) {
      subjectValidation.help = subject.error;
      subjectValidation.bsStyle = 'error';
    }

    return (
      <form className="form-horizontal">
        <SimpleSelect placeholder="Select campaign ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Campaign" {...campaign} data={allCampaigns}/>
        <Input type="text" label="Subject" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...subject} {...subjectValidation} />
        <DatePicker labelClassName="col-xs-2" wrapperClassName="col-xs-10" label="Delivery schedule" {...deliverAfter} />
        <SimpleSelect placeholder="Select timezone ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Timezone" {...timezone} data={allTimezones}/>
        <Input type="checkbox" label="Ignore receivers prefs" labelClassName="col-xs-offset-2 col-xs-10" {...ignoreReceiverPrefs}/>
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="button" disabled={Object.keys(errors).length !== 0} onClick={handleNext}>
                <Glyphicon glyph="save" />&nbsp;Next
              </Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
