/**
 * Created by urbanmarovt on 15/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';
import _ from 'underscore';

import {SimpleSelect, MultiSelect, RadioInline} from 'components';

import styles from './SendingReceiverForm.scss'



const validate = (values) => {

  const errors = {};

  if (values.receiverType === 'lists' && !values.receiverLists) {
    errors.receiverLists = 'Required';
  }

  return errors;

};

function reformatPrefillData(initialValues) {
  if (_.isEmpty(initialValues)) return {receiverType: 'lists'};
  const formattedData = {
    ...initialValues,
  };
  return formattedData;
}

@reduxForm({
  form: 'SendingReceiverForm',
  fields: ['receiverType', 'receiverLists'],
  validate: validate
},
state => ({
  initialValues: reformatPrefillData(state.views.sending.lists.data)
}))
export default class SendingReceiverForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object,
    fields: PropTypes.object,
    handleForm: PropTypes.func,
    handleNext: PropTypes.func,
    allLists: PropTypes.array
  };

  componentWillUnmount() {
    const {fields: {receiverLists, receiverType}, handleForm} = this.props;
    handleForm(
      {
        receiverLists: receiverLists.value,
        receiverType: receiverType.value
      }
    );
  }

  render() {
    const {fields: {receiverLists, receiverType}, handleNext, errors, allLists, channelId} = this.props;

    return (
      <form className="form-horizontal">
        <RadioInline label="Recipients" options={[{label: 'Lists', value: 'lists'}, {label: 'All', value: 'all'}, {label: 'Application', value: 'application'}]} field={receiverType}/>
        { receiverType.value === 'lists' &&
        <div>
          <div className="row">
            <p className="col-sm-offset-2 col-sm-10">Select lists of receivers to send the newsletter to. </p>
          </div>
          <MultiSelect label="Lists" placeholder="Select lists ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10" {...receiverLists} data={allLists}/>
        </div>
        }
        { receiverType.value === 'all' &&
        <div className="row">
          <div className="col-sm-offset-2 col-sm-10">
            <p>Newsletter will be send to all receivers through selected channel.</p>
          </div>
        </div>
        }
        { receiverType.value === 'application' &&
        <div className="row">
          <div className="col-sm-offset-2 col-sm-10">
            <p>Newsletter will be send to all users of previously specified applications.</p>
          </div>
        </div>
        }

        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="button" disabled={Object.keys(errors).length !== 0} onClick={() => handleNext()}>
                <Glyphicon glyph="save" />&nbsp;Next
              </Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
