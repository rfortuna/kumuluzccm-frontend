/**
 * Created by urbanmarovt on 15/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon, FormControls} from 'react-bootstrap';
import _ from 'underscore';
import $ from 'jquery';

import { DropzoneCustom, SimpleSelect, LabelsContainer, TinymceEditor,
  ButtonGroupSelect, RadioInline, Iframe, UploadImageModal, Gallery} from 'components';
import {messageTypes} from 'utils/constants';
import styles from './SendingContentForm.scss';

const SMS_LENGTH_LIMIT = 70;
const PUSH_LENGTH_LIMIT = 1000;

const validateDynamicProperties = (values, errors, fieldName) => {
  const propertiesRegex = /\$\{([^\}]+)\}/g;
  const usedPropertiesShort = values[fieldName].match(propertiesRegex);

  if (usedPropertiesShort) {
    usedPropertiesShort.forEach((usedProperty) => {
      const prop = usedProperty.substring(2).slice(0, -1);

      if (!values.allowedProperties.includes(prop)) {
        errors[fieldName] = `You can't use property ${prop}`;
      }
    });
  }
}

const validate = (values) => {
  const errors = {};


  if (!values.channelId) {
    return {channelId: "ChannelId is missing!"};
  }

  const messageType = messageTypes.find((type) => {
    return type.id === values.channelId
  });

  //SMS VALIDATION
  if (messageType.sms) {
    if (!values.contentSMS) {
      errors.contentSMS = 'Required';
    } else if (values.contentSMS) {
        validateDynamicProperties(values, errors, 'contentSMS');
        if (values.contentSMS.length > SMS_LENGTH_LIMIT ) {
          errors.contentSMS = `SMS can only hold ${SMS_LENGTH_LIMIT} characters.`;
        }
    }

  }
  //PUSH VALIDATION
  if (messageType.push) {
    if (!values.contentPUSH) {
      errors.contentPUSH = 'Required';
    } else if (values.contentPUSH) {
      validateDynamicProperties(values, errors, 'contentPUSH');
      let contentLength = values.contentPUSH.length + (values.subject ? values.subject.length : 0) + values.pushCustomPropertiesString.length;
      if (contentLength > PUSH_LENGTH_LIMIT ) {
        errors.contentPUSH = `Too long content: ${contentLength} characters. Push notification content length (taking into account custom property lengths) + subject length can be at most ${PUSH_LENGTH_LIMIT} characters.`;
      }
    }
  }

  //EMAIL VALIDATION
  if(messageType.email) {
    if (!values.contentEMAILPreview) {
      errors.contentEMAILPreview = 'Required';
    } else if (values.contentEMAILPreview) {
      validateDynamicProperties(values, errors, 'contentEMAILPreview');
    }

    if (!values.contentEMAIL && values.contentEMAILType !== 'custom') {
      errors.contentEMAIL = 'Required';
    }
  }

  if ( messageType.email && !values.contentEMAILType) {
    errors.contentEMAILType = 'Required';
  }

  if ( messageType.sms && !values.contentSMSType) {
    errors.contentSMSType = 'Required';
  }

  if ( messageType.push && !values.contentPUSHType) {
    errors.contentPUSHType = 'Required';
  }

  return errors;
};

function reformatPrefillData(initialValues, channelId, allowedProperties) {
  if (_.isEmpty(initialValues)) return {channelId: channelId, allowedProperties: allowedProperties};
  const formattedData = {
    ...initialValues,
    channelId: channelId,
    allowedProperties: allowedProperties
  };
  return formattedData;
}

@reduxForm({
  form: 'ContentForm',
  fields: ['contentSMS', 'contentPUSH' ,'contentEMAIL',
    'contentSMSType', 'contentPUSHType' ,'contentEMAILType',
    'channelId', 'allowedProperties', 'contentEMAILPreview',
    'isInPreview', 'subject',
    //added for validation of PUSH content length
    'pushCustomPropertiesString',
    'emailEditorState', 'subject'],
  validate: validate
},
state => ({
  uploadImageModalVisible: state.views.modals.uploadImageModalVisible,
  uploadImage: state.models.tenants.images.new,
  initialValues: {
    ...reformatPrefillData(
      state.views.sending.content.data,
      state.router.location.query.channelId,
      [...state.models.lists.properties.list.staticFields,
      ...state.models.lists.properties.list.customFields]),
    emailEditorState: 'edit',
    // for content length validation
    subject: state.views.sending.setup.data.subject,
    contentSMSType: 'predefined',
    contentPUSHType: 'predefined',
    contentEMAILType: 'predefined',
    // push content length validation
    pushCustomPropertiesString: state.views.sending.channelSetup.data.pushCustomPropertiesString
  }
}))
export default class SendingContentForm extends Component {

  static propTypes = {
    handleNext: PropTypes.func.isRequired,
    handleForm: PropTypes.func.isRequired,
    errors: PropTypes.object,
    fields: PropTypes.object,
    showContentEMAILPreview: PropTypes.func,
    contentEMAILPreview: PropTypes.string,
    templateFile: PropTypes.object,
    handleFileClear: PropTypes.func,
    campaignTemplatesLong: PropTypes.array,
    campaignTemplatesShort: PropTypes.array,
    allowedProperties: PropTypes.array,
    changeFieldValue: PropTypes.func,
    saveContentEMAILPreview: PropTypes.func,
    isSMS: PropTypes.bool,
    isEMAIL: PropTypes.bool,
    isPUSH: PropTypes.bool,
    showUploadImageModal: PropTypes.func,
    closeUploadImageModal: PropTypes.func,
    uploadNewImage: PropTypes.func,
    showImagePreview: PropTypes.func,
    uploadImage: PropTypes.object,
    clearUploadImage: PropTypes.func,
    tenantImages: PropTypes.array,
    addImageToTenantImages: PropTypes.func
  };

  componentWillUnmount() {
    const {fields: {contentSMS, contentPUSH, contentEMAILType, contentSMSType, contentPUSHType, allowedProperties}, contentEMAILPreview, errors, handleForm} = this.props;

    //TODO modify when backend changes
    handleForm(
      { contentShort: contentSMS.value ? contentSMS.value : contentPUSH.value,
        contentSMS: contentSMS.value,
        contentPUSH: contentPUSH.value,
        contentSMSType: contentSMSType.value,
        contentPUSHType: contentPUSHType.value,
        contentEMAIL: contentEMAILPreview,
        contentEMAILType: contentEMAILType.value,
        contentShortType: contentSMSType.value ? contentSMSType.value : contentPUSHType.value,
        contentEMAILPreview: contentEMAILPreview
      }, errors);
  }

  onDrop = ( filesToUpload ) => {
    this.props.fields.contentEMAIL.onChange(filesToUpload);
    const {showContentEMAILPreview} = this.props;
    $.get( filesToUpload[0].preview, ( data ) => {
      this.props.fields.contentEMAILPreview.onChange(data);
      showContentEMAILPreview(data, filesToUpload[0]);
    });
  };

  handleTemplateSMSSelectChange = (item) => {
    if (item) {
      this.props.fields.contentSMS.onChange(item.value);
    } else {
      this.props.fields.contentSMS.onChange('');
    }
  };

  handleTemplatePUSHSelectChange = (item) => {
    if (item) {
      this.props.fields.contentPUSH.onChange(item.value);
    } else {
      this.props.fields.contentPUSH.onChange('');
    }
  };

  handleTemplateEMAILSelectChange = (item) => {
    console.log(item.value);
    const {showContentEMAILPreview} = this.props;
    showContentEMAILPreview(item.value, null);
  };

  makeOnChangeInput = (field) => {
    //TODO refactor
    const {saveContentEMAILPreview} = this.props;
    return (event) => {
      field.onChange(event);
      saveContentEMAILPreview('');
    };
  };

  render() {
    const {fields: {contentSMS, contentPUSH, contentSMSType, contentPUSHType, contentEMAIL, contentEMAILType, emailEditorState},
      handleNext, errors, isSMS, isPUSH, isEMAIL,
      contentEMAILPreview, templateFile, handleFileClear,
      campaignTemplatesLong, campaignTemplatesShort, allowedProperties,
      changeFieldValue, saveContentEMAILPreview, tenantImages,
      // Upload modal props
      uploadImageModalVisible, showUploadImageModal, closeUploadImageModal, addImageToTenantImages,
      uploadNewImage, showImagePreview, uploadImage, clearUploadImage} = this.props;

    // validation
    const contentSMSValidation = {};
    const contentPUSHValidation = {};
    const contentEMAILValidation = {};

    if (contentSMS.touched && contentSMS.error) {
      contentSMSValidation.help = contentSMS.error;
      contentSMSValidation.bsStyle = 'error';
    }

    if (contentPUSH.touched && contentPUSH.error) {
      contentPUSHValidation.help = contentPUSH.error;
      contentPUSHValidation.bsStyle = 'error';
    }

    if (contentEMAIL.touched && contentEMAIL.error) {
      contentEMAILValidation.help = contentEMAIL.error;
      contentEMAILValidation.bsStyle = 'error';
    }

    return (
      <div>
        <div className="row">
          <div className="col-sm-offset-2 col-sm-10">
            <h4> Message personalization </h4>
            <div>
              <p>You can use the follwing properties by typing $&#123;name&#125; in content and they will be filled from receivers' data.</p>
              <h5>Allowed properties</h5>
              <ul>
              { allowedProperties.map(property => (
               <li key={property}>
                 <h6>{property}</h6>
               </li>
              ))}
              </ul>
            </div>
          </div>
        </div>
        <form className="form-horizontal">
          {isSMS &&
            <div className="row">
              <RadioInline field={contentSMSType} label="SMS template"
                options={[{label: 'Predefined template', value: 'predefined'}, {label: 'From scratch', value: 'custom'}]} onChange={this.makeOnChangeInput(contentSMSType)} />
                { contentSMSType.value === 'predefined' &&
                <SimpleSelect placeholder="Select template ..." wrapperClassName="col-xs-offset-2 col-xs-10" {...contentSMS}
                              data={campaignTemplatesShort} handleOnChange={this.handleTemplateSMSSelectChange}/>
                }
                { !contentSMSType.error &&
                <Input type="textarea" wrapperClassName="col-xs-offset-2 col-xs-10"
                  {...contentSMS} {...contentSMSValidation}/>}
            </div>
          }
          {isPUSH &&
              <div className="row">
                <RadioInline field={contentPUSHType} label="PUSH template"
                  options={[{label: 'Predefined template', value: 'predefined'}, {label: 'From scratch', value: 'custom'}]} onChange={this.makeOnChangeInput(contentPUSHType)} />
                { contentPUSHType.value === 'predefined' &&
                <SimpleSelect placeholder="Select template ..." wrapperClassName="col-xs-offset-2 col-xs-10" {...contentPUSH}
                              data={campaignTemplatesShort} handleOnChange={this.handleTemplatePUSHSelectChange}/>
                }
                { !contentPUSHType.error &&
                <Input type="textarea" className={styles.textareaPUSH} wrapperClassName="col-xs-offset-2 col-xs-10"
                  {...contentPUSH} {...contentPUSHValidation}/>}
              </div>
          }
          {isEMAIL &&
          <div>
            <div>
              <div className="row">
                <div className="col-xs-12">
                <RadioInline field={contentEMAILType} label="Email template"
                  options={[{label: 'Predefined template', value: 'predefined'}, {label: 'Upload template', value: 'upload'}, {label: 'From scratch', value: 'custom'}]} onChange={this.makeOnChangeInput(contentEMAILType)} />
                    <div className="row">
                      <div className="col-xs-offset-2 col-xs-10">
                        { contentEMAILType.value === 'predefined' &&
                        <SimpleSelect placeholder="Select template ..." wrapperClassName="col-xs-8" {...contentEMAIL}
                                      data={campaignTemplatesLong} handleOnChange={this.handleTemplateEMAILSelectChange}/>
                        }
                        { contentEMAILType.value === 'upload' &&
                        <div className={`${styles.dropzone} row`}>
                          <div className="col-xs-8">
                            <DropzoneCustom
                              onDrop={ this.onDrop }
                              label={'Drop the HTML template here, or click to upload it.'}
                              field={contentEMAIL}
                              file={templateFile}/>
                            { contentEMAIL.error &&
                              <p style={{color: "red"}}>
                                {contentEMAIL.error}
                              </p>
                            }
                          </div>
                        </div>
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {(contentEMAIL.error !== 'Required' && contentEMAILType.value !== 'custom') &&
                <div className="row">
                  <div className="col-xs-offset-2 col-xs-10">
                    <ButtonToolbar>
                      <Button type="button" onClick={handleFileClear}>
                        <Glyphicon glyph="remove" />&nbsp;Clear
                      </Button>
                    </ButtonToolbar>
                  </div>
                </div>
                }

                {(contentEMAILType.value && (contentEMAIL.error !== 'Required' || contentEMAILType.value === 'custom')) &&
                  <div>
                    <LabelsContainer label="Content" labelClassName="col-xs-2"
                                         wrapperClassName="col-xs-10">
                      <ButtonGroupSelect options={[{label: 'Edit', value: 'edit'}, {label: 'Preview & Save', value: 'preview'}, {label: 'Image gallery', value: 'gallery'}]} changeFieldValue={changeFieldValue} field={emailEditorState} formName={'ContentForm'} />
                      <Button style={{float: 'right'}} onClick={showUploadImageModal}>Upload image</Button>
                      <br/>
                      { emailEditorState.value === 'preview' &&
                        <Iframe content={contentEMAILPreview} /> ||
                        emailEditorState.value === 'gallery' &&
                          <Gallery images={tenantImages} />
                      }
                      <TinymceEditor visible={emailEditorState.value === 'edit'} content={contentEMAILPreview} saveContentToStore={saveContentEMAILPreview} />
                    </LabelsContainer>
                    <UploadImageModal uploadImageModalVisible={uploadImageModalVisible}
                                      closeUploadImageModal={closeUploadImageModal}
                                      uploadNewImage={uploadNewImage}
                                      showImagePreview={showImagePreview}
                                      uploadImage={uploadImage}
                                      changeFieldValue={changeFieldValue}
                                      clearUploadImage={clearUploadImage}
                                      addImageToTenantImages={addImageToTenantImages}></UploadImageModal>
                  </div>
                }
              </div>
          }
          <div className="row">
            <div className="col-xs-offset-2 col-xs-10">
              <ButtonToolbar>
                <Button type="button" disabled={Object.keys(errors).length !== 0} onClick={handleNext} >
                  <Glyphicon glyph="save" />&nbsp;Next
                </Button>
              </ButtonToolbar>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
