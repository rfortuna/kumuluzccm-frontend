/**
 * Created by urbanmarovt on 30/06/16.
 */

import React, {Component, PropTypes} from 'react';

import styles from './Chart.scss';

export default class ChartPlaceholder extends Component {

  static propTypes = {
    placeholderMessage: PropTypes.string
  };

  render() {
    const {placeholderMessage} = this.props;

    return (
      <div className={styles.placeholderContainer}>
        <div className={styles.placeholderContent}>
          <i className={`fa fa-info-circle ${styles.icon}`} aria-hidden="true"></i>
          <p className={styles.text}>{placeholderMessage}</p>
        </div>
      </div>
    );
  }
};
