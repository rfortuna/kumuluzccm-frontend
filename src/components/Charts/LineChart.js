import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';

import styles from './Chart.scss';

let lineChart, formatter;
export default class LineChart extends Component {

  static propTypes = {
    id: PropTypes.string,
    subchart: PropTypes.bool,
    data: PropTypes.object,
    selectedTime: PropTypes.string
  }

  shouldComponentUpdate (nextProps, nextState){
    this.updateChart(nextProps.data.data.columns, nextProps.selectedTime);
    return false;
  }

  componentDidMount() {
    const {data, id} = this.props;
    this.renderChart(data, id);
  }

  updateChart = (newColumns, selectedTime) => {

    lineChart.load({
      columns: newColumns
    });

    if (selectedTime) {
      this.updateFormatter(selectedTime);
      lineChart.flush();
    }

  };

  renderChart = (data, id) => {
    import c3 from './../../../node_modules/c3/c3';
    const { subchart } = this.props;

    lineChart = c3.generate({
      bindto: `#${id}`,
      subchart: {
        show: subchart || false
      },
      ...data
    })
  };

  updateFormatter = (selectedTime) => {
    formatter = (x) => moment(x).format(selectedTime === 'd' ? 'LT' : 'L');
  };

  render() {

    const { id, data, selectedTime} = this.props;

    if(__CLIENT__) {
      if (selectedTime) {
        this.updateFormatter(selectedTime);
        data.axis.x.tick.format = (x) => {
          return formatter(x)
        };
      }
      this.renderChart(data, id);
    }

    return (
      <div className={styles.chartContainer}>
        <div id={id}></div>
      </div>
    );

  }
};

