/**
 * Created by rokfortuna on 4/11/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon, FormControls} from 'react-bootstrap';

const validate = (values) => {

  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  }

  return errors;
};


@reduxForm({
    form: 'ListForm',
    fields: ['name'],
    validate: validate
  },
  state => ({
    initialValues: {
      name: state.models.lists.details.name
    }
  }))
export default class CampaignForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object,
    receiverCount: PropTypes.string
  };

  render() {
    const {fields: {name}, receiverCount, handleSubmit, handleCancel, submitting, errors} = this.props;

    // validation
    const nameValidation = {};

    if (name.touched && name.error) {
      nameValidation.help = name.error;
      nameValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <Input type="text" label="Name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...name} {...nameValidation} />
        <FormControls.Static label="Total receivers" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={receiverCount} />
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
