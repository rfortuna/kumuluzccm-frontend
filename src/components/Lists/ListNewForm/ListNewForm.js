/**
 * Created by rokfortuna on 4/7/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';
import $ from 'jquery';
import CSV from 'comma-separated-values';

import {DropzoneCustom, ListPreviewGrid, RadioInline} from 'components';
import {refactorCSVListData} from 'utils/refactor';


const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Required';
  }

  return errors;
};


@reduxForm({
  form: 'ListForm',
  fields: ['name', 'csvData', 'importType'],
  validate: validate
},
  state => ({
    initialValues: {
      importType: 'INTEGRATE'
    }
  }))
export default class ListNewForm extends Component {

  static propTypes = {
    showCSVPreview: PropTypes.func,
    csvPreview: PropTypes.array,
    csvFile: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object
  }

  onDrop = ( filesToUpload, e ) => {
    const {showCSVPreview, fields: {csvData}} = this.props;
    csvData.onChange(filesToUpload);
    $.get( filesToUpload[0].preview, function( data ) {
      showCSVPreview(new CSV(data).parse(), filesToUpload[0]);
    });
  }


  render() {
    const {fields: {name, csvData, importType}, csvFile, csvPreview, handleSubmit, handleCancel, submitting, errors} = this.props;

    // validation
    const nameValidation = {};

    if (name.touched && name.error) {
      nameValidation.help = name.error;
      nameValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <Input type="text" label="Name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...name} />


        <RadioInline
          field={importType}
          label="Import type"
          options={[{label: 'Integration', value: 'INTEGRATE'}, {label: 'From file', value: 'FILE'}]}
        />
        {importType.value === 'FILE' &&
        <div className="form-group">
          <label className="col-sm-2 control-label"> Import list </label>
          <div className="col-sm-10">
            <DropzoneCustom
              onDrop={this.onDrop }
              label={'Drop the list-containing CSV file here, or click to upload it (compulsory fields: email, mobile).'}
              field={csvData}
              file={csvFile}
            />
            <br/>
            <small>Note: The CSV file should be encoded with UTF-8. </small>
          </div>
        </div>
        }
        {importType.value === 'INTEGRATE' &&
          <span>
            <label className="col-sm-2 control-label"> Integration type </label>
            <div className="col-sm-10">
              <Button>REST</Button> &nbsp;
              <Button>SOAP</Button> &nbsp;
              <Button>EJB Remote</Button> &nbsp;
              <Button><i className="fa fa-database" aria-hidden="true"></i>&nbsp; Database</Button>
            </div>
          </span>
        }
        { csvFile &&
        <div className="form-group">
          <label className="col-sm-2 control-label"> Preview </label>
          <div className="col-sm-10">
            <ListPreviewGrid data={refactorCSVListData(csvPreview, 10)}/>
          </div>
        </div>
        }

        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
