/**
 * Created by rokfortuna on 11/06/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon, FormControls} from 'react-bootstrap';

import {ButtonGroupSelect} from 'components';

//TODO temporary (we will need to pass validation details to a field - not yet supported)
const compulsoryFields = ['firstName', 'lastName', 'mobilePhone', 'email'];

const validate = (values) => {

  const errors = {};
  compulsoryFields.forEach((fieldName) => {
    if (!values[fieldName]) {
      errors[fieldName] = 'Required';
    }
  });
  return errors;
};


@reduxForm({
    form: 'ListAddNewReceiverForm',
    validate: validate
  },
  state => ({
    initialValues: {
    }
  }))
export default class ListAddNewReceiverForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func,
    changeFormField: PropTypes.func
  };

  render() {
    const { fields : {...dynamicFields }, handleSubmit, errors, changeFormField} = this.props;

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
          {Object.keys(dynamicFields).map( (field) =>
            <Input type="text" key={`dynamic_field${field}`} label={dynamicFields[field].name} {...dynamicFields[field]} labelClassName="col-xs-2 text-align-right" wrapperClassName="col-xs-10"/>
          )}
          <div className="row">
            <div className="col-xs-offset-2 col-xs-10">
              <ButtonToolbar>
                <Button type="submit" disabled={Object.keys(errors).length !== 0} >
                  <Glyphicon glyph="plus" />&nbsp;Add
                </Button>
              </ButtonToolbar>
            </div>
          </div>
      </form>
    );
  }
}
