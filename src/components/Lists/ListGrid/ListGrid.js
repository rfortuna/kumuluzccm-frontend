/**
 * Created by rokfortuna on 4/11/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table, Pagination} from 'react-bootstrap';

export default class ListGrid extends Component {
  static propTypes = {
    data: PropTypes.array,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number,
    actions: PropTypes.func
  };

  render() {
    const {data, paginationOnPageChange, paginationPages, paginationActivePage, handleOnReceiverClick, actions} = this.props;
    const [headers, ...receivers] = data;
    return (
      <div style={{height: '500px', marginBottom: '70px'}}>
        <Table hover>
          <thead>
          <tr>
            { headers.map(
              (header) =>
                <th key={header}>
                  <strong>{header}</strong>
                </th>
            )}
            {actions && <th>Actions</th>}
          </tr>
          </thead>
          <tbody>
          { receivers.map(
            (receiver) =>
              <tr key={receiver.id} onClick={() => handleOnReceiverClick(receiver.id)}>
                { headers.map(
                  (header) =>
                    <td key={`${receiver.id}_${header}`}>
                      {receiver[header] === null && '-' || `${receiver[header]}`}
                    </td>
                )}
                {actions && <td>{actions(receiver)}</td>}
              </tr>
          )}
          </tbody>
        </Table>
        <div style={{textAlign: 'center'}}>
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={paginationPages}
            maxButtons={10}
            activePage={paginationActivePage}
            onSelect={paginationOnPageChange} />
        </div>
      </div>
    );
  }
}
