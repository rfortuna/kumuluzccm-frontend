/**
 * Created by rokfortuna on 3/24/16.
 */


import React, {Component, PropTypes} from 'react';
import {Table} from 'reactable';

export default class ListPreviewGrid extends Component {
  static propTypes = {
    data: PropTypes.array
  };

  render() {
    const {data} = this.props;
    return (
      <div>
        <Table className="table" hover data={data}/>
      </div>
    );
  }
}
