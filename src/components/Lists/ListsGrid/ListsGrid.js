/**
 * Created by rokfortuna on 3/10/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table, Pagination} from 'react-bootstrap';

export default class ListsGrid extends Component {
  static propTypes = {
    lists: PropTypes.array,
    onPackageClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number,
    onListClick: PropTypes.func
  };

  render() {
    const {lists, onListClick, paginationOnPageChange, paginationPages, paginationActivePage} = this.props;
    return (
      <div style={{height: '500px'}}>
        <Table className="table" hover>
          <thead>
            <tr>
              <th>
                <strong>Name</strong>
              </th>
            </tr>
          </thead>
          <tbody>
          { lists.map(
            (list) =>
              <tr key={list.id} onClick={()=> onListClick(list.id)} className="clickable">
                <td>{list.name}</td>
              </tr>
          )
          }
          </tbody>
        </Table>
        <div style={{textAlign: 'center'}}>
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={paginationPages}
            maxButtons={6}
            activePage={paginationActivePage}
            onSelect={paginationOnPageChange} />
        </div>
      </div>
    );
  }
}
