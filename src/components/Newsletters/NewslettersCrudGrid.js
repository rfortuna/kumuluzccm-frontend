/**
 * Created by rokfortuna on 5/6/16.
 */

import React, {Component, PropTypes} from 'react';
import {Pagination, Table} from 'react-bootstrap';
import {DateLabel, ChannelTypeLabel, ListDisplayGrid, ChannelDisplayGrid, MessageStatusLabel} from 'components';

export default class NewslettersGrid extends Component {
  static propTypes = {
    pagination: PropTypes.bool,
    newsletters: PropTypes.array,
    onNewsletterClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number
  };

  render() {
    const {newsletters, onNewsletterClick, paginationOnPageChange, paginationPages, paginationActivePage, pagination} = this.props;
    return (
      <div style={{height: '500px'}}>
        <Table className="table" hover>
          <thead>
          <tr>
            <th>
              <strong>Date</strong>
            </th>
            <th>
              <strong>Campaign</strong>
            </th>
            <th>
              <strong>Subject</strong>
            </th>
            <th>
              <strong>Status</strong>
            </th>
            <th>
              <strong>Receiver lists</strong>
            </th>
            <th>
              <strong>Channels</strong>
            </th>
          </tr>
          </thead>
          <tbody>
          { newsletters.map(
            (newsletter) =>
              <tr key={newsletter.id} onClick={()=> onNewsletterClick(newsletter.campaign.id, newsletter.id)} className="clickable">
                <td>
                  <DateLabel date={newsletter.createdAt}/>
                </td>
                <td>{newsletter.campaign.name}</td>
                <td>{newsletter.subject}</td>
                <td>
                  <MessageStatusLabel type={newsletter.status} />
                </td>
                <td><ListDisplayGrid lists={newsletter.receiverLists} /></td>
                <td>
                  {!newsletter.preferred &&
                  <ChannelDisplayGrid channels={newsletter.channels} /> ||
                  <ChannelDisplayGrid channels={[{id: '1', type: 'PREFERRED_CHANNEL'}]}/>}
                </td>
              </tr>
          )
          }
          </tbody>
        </Table>
        {pagination &&
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          items={paginationPages}
          maxButtons={5}
          activePage={paginationActivePage}
          onSelect={paginationOnPageChange} />}
      </div>
    );
  }
}
