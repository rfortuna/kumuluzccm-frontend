import React, {Component, PropTypes} from 'react';
import {Pagination, Table} from 'react-bootstrap';
import {DateLabel, ChannelTypeLabel, ListDisplayGrid, ChannelDisplayGrid} from 'components';

export default class NewslettersGrid extends Component {
  static propTypes = {
    pagination: PropTypes.bool,
    newsletters: PropTypes.array,
    onNewsletterClick: PropTypes.func,
    paginationOnPageChange: PropTypes.func,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number
  };

  render() {
    const {newsletters, onNewsletterClick, paginationOnPageChange, paginationPages, paginationActivePage, pagination} = this.props;
    return (
      <div>
        <Table className="table" hover>
          <thead>
            <tr>
              <th>
                <strong>Date</strong>
              </th>
              <th>
                <strong>Campaign</strong>
              </th>
              <th>
                <strong>Subject</strong>
              </th>
              <th>
                <strong>Receiver lists</strong>
              </th>
              <th>
                <strong>Channels</strong>
              </th>
              <th>
                <strong>Number of messages</strong>
              </th>
            </tr>
          </thead>
          <tbody>
          { newsletters.map(
            (newsletter) =>
              <tr key={newsletter.newsletter.id} onClick={()=> onNewsletterClick(newsletter.newsletter.campaign.id, newsletter.newsletter.id)} className="clickable">
                <td>
                  <DateLabel date={newsletter.newsletter.createdAt}/>
                </td>
                <td>{newsletter.newsletter.campaign.name}</td>
                <td>{newsletter.newsletter.subject}</td>
                <td><ListDisplayGrid lists={newsletter.newsletter.receiverLists} /></td>
                <td>
                  {!newsletter.newsletter.preferred &&
                  <ChannelDisplayGrid channels={newsletter.newsletter.channels} /> ||
                  <ChannelDisplayGrid channels={[{id: '1', type: 'PREFERRED_CHANNEL'}]}/>}
                  </td>
                <td>{newsletter.numMessages} </td>
              </tr>
            )
          }
          </tbody>
        </Table>
        {pagination &&
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={paginationPages}
            maxButtons={5}
            activePage={paginationActivePage}
            onSelect={paginationOnPageChange} />}
      </div>
    );
  }
}
