/**
 * Created by urbanmarovt on 27/06/16.
 */


import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

import styles from './Iframe.scss';


export default class Iframe extends Component {

  static propTypes = {
    content: PropTypes.string
  };

  render() {
    const {content} = this.props;
    const frameSrc = `data:text/html,${content}`
    return (
      <div>
        <iframe
          ref="preview"
          src={frameSrc}
          frameBorder="0"
          className={styles.iframe}
        >
      </iframe>
    </div>)
  }
}
