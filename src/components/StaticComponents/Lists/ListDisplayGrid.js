/**
 * Created by urbanmarovt on 15/04/16.
 */

import React, {Component, PropTypes} from 'react';

import {ListLabel} from 'components';

export default class ListDisplayGrid extends Component {

  static propTypes = {
    lists: PropTypes.array
  };

  render() {
    const { lists} = this.props;
    return (
      <span>
        {lists && lists.map((list) =>
          <ListLabel key={`list_${list.id}`} list={list} />
        )}
      </span>
    );
  }
}
