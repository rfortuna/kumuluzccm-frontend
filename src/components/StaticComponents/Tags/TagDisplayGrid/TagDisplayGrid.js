/**
 * Created by rokfortuna on 25/02/16.
 */

import React, {Component, PropTypes} from 'react';

import Tag from '../Tag/Tag';

export default class TagDisplayGrid extends Component {

  static propTypes = {
    tags: PropTypes.array
  }

  render() {
    const { tags} = this.props;
    return (
        <div>
            {tags && tags.map((tag) =>
              <Tag key={`tag_${tag.id}`} tag={tag} />
            )}
        </div>
    );
  }
}
