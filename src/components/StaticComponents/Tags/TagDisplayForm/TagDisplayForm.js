/**
 * Created by rokfortuna on 12/02/16.
 */

import React, {Component, PropTypes} from 'react';
import styles from './TagDisplayForm.scss';

import Tag from '../Tag/Tag';

export default class TagDisplay extends Component {

  static propTypes = {
    labelClassName: PropTypes.string,
    wrapperClassName: PropTypes.string,
    tags: PropTypes.array
  }

  render() {
    const {labelClassName, wrapperClassName, tags} = this.props;
    return (
      <div className={`${styles.marginBottom} row`}>
        <div className={labelClassName}>
          <label className={styles.label}>Tags</label>
        </div>

        <div className={`${wrapperClassName} ${styles.paddingTop5} `}>
            {tags && tags.map((tag) =>
                  <Tag key={tag.id} tag={tag} />
            )}
        </div>

      </div>
    );
  }
}
