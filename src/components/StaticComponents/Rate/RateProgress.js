/**
 * Created by urbanmarovt on 19/04/16.
 */

import React, {Component, PropTypes} from 'react';
import __ from 'underscore.string';

export default class RateProgress extends Component {

  static propTypes = {
    rate: PropTypes.number
  };

  render() {
    const {rate, label} = this.props;
    return (
      <div>
        <small>{label}</small>
        <div className="stat-percent">{`${__.numberFormat(rate * 100, 1, ',', ',')} %`}</div>
        <div className="progress progress-mini">
          <div style={{width: rate * 100 + '%'}} className="progress-bar"></div>
        </div>
      </div>
    );
  }
}
