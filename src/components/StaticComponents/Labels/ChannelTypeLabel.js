/**
 * Created by rokfortuna on 3/7/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

export default class ChannelTypeLabel extends Component {

  static propTypes = {
    type: PropTypes.string
  };

  types = {
    EMAIL: {icon: 'fa-envelope', label: 'EMAIL'},
    SMS: {icon: 'fa-comments-o', label: 'SMS'},
    PUSH_NOTIFICATION: {icon: 'fa-mobile-phone', label: 'PUSH'},
    PREFERRED_CHANNEL: {icon: 'fa-star', label: 'PREFERRED'}
  };

  render() {
    const {type} = this.props;
    return (
      <span style={{paddingRight: '10px'}}>
        <Label>
          <i className={`fa ${this.types[type].icon}`}/> &nbsp;
          {this.types[type].label}
        </Label>
      </span>
    );
  }
}
