/**
 * Created by rokfortuna on 3/22/16.
 */
/**
 * Created by rokfortuna on 22/03/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';
import _ from 'underscore';

export default class MessageStatusLabel extends Component {

  static propTypes = {
    type: PropTypes.string
  }

  types = {
    OPENED: {className: 'label-primary', label: 'opened'},
    DELIVERED: {className: 'label-success', label: 'delivered'},
    OTHER: {className: 'label-warning', label: 'other'}
  }

  render() {
    const {type} = this.props;
    const types = this.types;
    let curType;
    if (!_.has(types, type)){
      curType = types['DELIVERED'];
    } else {
      curType = types[type];
    }
    return (
      <div className={`label ${curType.className}`}>{curType.label}</div>
    );
  }
}
