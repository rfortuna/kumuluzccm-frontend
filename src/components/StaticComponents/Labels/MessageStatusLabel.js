/**
 * Created by rokfortuna on 3/7/16.
 */

import React, {Component, PropTypes} from 'react';
import _ from 'underscore';

export default class MessageStatusLabel extends Component {

  static propTypes = {
    type: PropTypes.string
  };

  types = {
    SENT: {className: 'label-primary', label: 'SENT'},
    PENDING: {className: 'label-warning', label: 'PENDING'},
    DRAFT: {className: 'label-success', label: 'DRAFT'}
  };

  render() {
    const {type} = this.props;
    const types = this.types;
    let curType;
    if (!_.has(types, type)) {
      curType = types.SENT;
    } else {
      curType = types[type];
    }
    return (
      <span className={`label ${curType.className}`}>{curType.label}</span>
    );
  }
}
