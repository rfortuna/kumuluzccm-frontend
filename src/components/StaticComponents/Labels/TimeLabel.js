/**
 * Created by urbanmarovt on 08/03/16.
 */

import React, {Component, PropTypes} from 'react';
import {timeFromTimestamp} from 'utils/datetimeFormat';

export default class TimeLabel extends Component {

  static propTypes = {
    time: PropTypes.number
  }

  render() {
    const {time} = this.props;
    return (
      <span>
        {time && timeFromTimestamp(time)}
        {!time && '-'}
        </span>
    );
  }
}
