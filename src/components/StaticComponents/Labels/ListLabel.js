/**
 * Created by urbanmarovt on 15/04/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

/* Component for wrapping elements in grids (to display them correctly).*/
export default class ListLabel extends Component {

  static propTypes = {
    type: PropTypes.string
  };

  render() {
    const {list} = this.props;
    return (
      <span>
        <Label>
          {list.name}
        </Label>
      </span>
    );
  }
}
