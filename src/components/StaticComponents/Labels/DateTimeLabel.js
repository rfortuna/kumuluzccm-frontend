/**
 * Created by rokfortuna on 3/15/16.
 */
import React, {Component, PropTypes} from 'react';
import {dateTimeFromTimestamp} from 'utils/datetimeFormat';

export default class DateTimeLabel extends Component {

  static propTypes = {
    date: PropTypes.number
  }

  render() {
    const {date} = this.props;
    return (
      <span>
          <i className="fa fa-icon fa-clock-o"/>
        &nbsp; &nbsp;
        {date && dateTimeFromTimestamp(date)}
        {!date && '-'}
        </span>
    );
  }
}
