/**
 * Created by rokfortuna on 01/07/16.
 */

 import React, {Component, PropTypes} from 'react';
 import _ from 'underscore';

 const statuses = {
   IMPORTED: {className: 'label-primary' , label: 'IMPORTED'},
   NOT_IMPORTED: {className: 'label-error', label: 'NOT IMPORTED'},
   IN_PROGRESS: {className: 'label-warning', label: 'IN PROGRESS'}
 }

 export default class ListImportStatusLabel extends Component {

   static propTypes = {
     status: PropTypes.string,
     linesProcessed: PropTypes.number,
     linesTotal: PropTypes.number
   };


   render() {
     const {status, linesProcessed, linesTotal} = this.props;
     let curStatus = statuses[status];
     if (!curStatus) {
       curStatus = statuses['IMPORTED'];
     }
     return (
       <div>
        <span className={`label ${curStatus.className}`}>{`${curStatus.label} `}</span>
        &nbsp; <small>{`${linesProcessed}/${linesTotal} lines processed`}</small>
       </div>
     );
   }
 }
