/**
 * Created by urbanmarovt on 22/04/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

export default class PlatformLabel extends Component {

  static propTypes = {
    type: PropTypes.string
  };

  render() {
    const {platform} = this.props;
    return (
      <span>
        <Label>
          {platform.type}
        </Label>
      </span>
    );
  }
}
