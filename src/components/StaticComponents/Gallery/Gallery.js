/**
 * Created by urbanmarovt on 04/07/16.
 */

import React, {Component, PropTypes} from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import {toastr} from 'react-redux-toastr';

import styles from './Gallery.scss';

export default class Gallery extends Component {

  static propTypes = {
    images: PropTypes.arraynpm
  };

  render() {
    const {images} = this.props;
    return (
      <div className={`row ${styles.galleryContainer}`}>
        <div className="col-xs-12">
          <p>Click on image to copy it's url to clipboard</p>
        </div>
        {images.map((image) =>
          <div className="col-xs-6 col-sm-4 col-md-3 col-2">
            <CopyToClipboard text={image.url}
                             onCopy={() => toastr.success('You have successfully copied url to clipboard', {timeOut: 4000}) }>
              <img src={image.url} className={styles.image}/>
            </CopyToClipboard>
          </div>
        )}
      </div>
      )
  }
}
