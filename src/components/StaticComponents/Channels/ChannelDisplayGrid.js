/**
 * Created by urbanmarovt on 15/04/16.
 */

import React, {Component, PropTypes} from 'react';

import {ChannelTypeLabel} from 'components';

import styles from './ChannelDisplayGrid.scss';

export default class ChannelDisplayGrid extends Component {

  static propTypes = {
    channels: PropTypes.array
  };

  render() {
    const {channels} = this.props;
    return (
      <span>
        {channels && channels.map((channel) =>
          <ChannelTypeLabel key={`channel_${channel.id}`} type={channel.type} className={styles.padding} />
        )}
      </span>
    );
  }
}
