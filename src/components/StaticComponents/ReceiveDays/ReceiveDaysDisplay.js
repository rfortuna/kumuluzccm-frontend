/**
 * Created by urbanmarovt on 18/04/16.
 */

import React, {Component, PropTypes} from 'react';
import {Label} from 'react-bootstrap';

import styles from './ReceiveDaysDisplay.scss';

export default class ListDisplayGrid extends Component {

  static propTypes = {
    lists: PropTypes.array
  };

  days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  render() {
    const {receiveDays} = this.props;
    return (
      <span>
        {receiveDays && receiveDays.split('').map((receiving, index) =>
            <span key={`day_${index}`} className={styles.item}>
              <Label style={{backgroundColor: Number(receiving) ? '#5cb85c' : '#d9534f'}}>
                {this.days[index]}
              </Label>
            </span>
        )}
      </span>
    );
  }
}
