import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';

import { DatePicker, MultiSelect } from 'components';
import { validateTimeStamp, validateEmail } from 'utils/validation';
import _ from 'underscore';

const validate = (values) => {

  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  }

  if (values.startDate && !validateTimeStamp(values.startDate)) {
    errors.startDate = 'Invalid date format';
  }

  if (values.endDate && !validateTimeStamp(values.endDate)) {
    errors.endDate = 'Invalid date format';
  }

  if (values.mailFrom && validateEmail(values.mailFrom)) {
    errors.mailFrom = 'Invalid email format';
  }

  return errors;
};

function reformatPrefillData(initialValues) {
  if (_.isEmpty(initialValues)) return {};
  const formattedData = {
    ...initialValues,
    startDate: initialValues.startDate,
    endDate: initialValues.endDate,
    tags: initialValues.tags.reduce( (prev, curr) => `${prev},${curr.id}`, '')
  };
  return formattedData;
}

@reduxForm({
  form: 'CampaignForm',
  fields: ['name', 'startDate', 'endDate', 'tags', 'mailFrom', 'mailUser'],
  validate: validate
},
state => ({
  initialValues: reformatPrefillData(state.models.campaigns.details.data)
}))
export default class CampaignForm extends Component {

  static propTypes = {
    allTags: PropTypes.array,
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object
  };

  render() {
    const {fields: {name, startDate, endDate, tags, mailFrom, mailUser}, handleSubmit, handleCancel, submitting, allTags, errors} = this.props;

    // validation
    const nameValidation = {},
      mailFromValidation = {};

    if (name.touched && name.error) {
      nameValidation.help = name.error;
      nameValidation.bsStyle = 'error';
    }

    if (mailFrom.error) {
      mailFromValidation.help = mailFrom.error;
      mailFromValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <Input type="text" label="Name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...name} {...nameValidation} />
        <DatePicker mode="date" inputFormat="DD. MM. YYYY" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...startDate} label="Start date"/>
        <DatePicker mode="date" inputFormat="DD. MM. YYYY" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
          {...endDate} label="End date"/>
        <MultiSelect labelClassName="col-xs-2" wrapperClassName="col-xs-10" {...tags} data={allTags} label="Tags"/>
        <div className="hr-line-dashed"></div>
        <Input type="text" label="Sending email" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...mailFrom} {...mailFromValidation} />
        <Input type="text" label="Sending email name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...mailUser}  />
        {
//        <Input type="text" label="Sending sms name" labelClassName="col-xs-2"
//               wrapperClassName="col-xs-10" {...smsUser}  />
        }
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
