/**
 * Created by urbanmarovt on 10/02/16.
 */
import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';
import {DateLabel, TagDisplayGrid} from 'components';
import {Pagination} from 'react-bootstrap';

export default class CampaignsGrid extends Component {
  static propTypes = {
    campaigns: PropTypes.array.isRequired,
    onCampaignClick: PropTypes.func.isRequired,
    paginationOnPageChange: PropTypes.func.isRequired,
    paginationPages: PropTypes.number,
    paginationActivePage: PropTypes.number,
    onSortClick: PropTypes.func
  };

  render() {
    const {campaigns, onCampaignClick, paginationOnPageChange, paginationPages, paginationActivePage, onSortClick} = this.props;
    return (
      <div style={{height: '550px'}}>
        <Table className="table" sortable hover>
          <thead>
            <tr>
              <th onClick={() => onSortClick('name')} className="clickable">
                <strong className="name-header">Name &nbsp;<i className="fa fa-sort"></i></strong>
              </th>
              <th>
                <strong className="name-header">Start date</strong>
              </th>
              <th>
                <strong className="name-header">End date</strong>
              </th>
              <th>
                <strong className="name-header">Tags</strong>
              </th>
            </tr>
          </thead>
          <tbody>
          {
            campaigns.map(
              (campaign) =>
                <tr onClick={() => onCampaignClick(campaign.id)} key={`campaign_${campaign.id}`} className="clickable">
                  <td>
                    {campaign.name}
                  </td>
                  <td>
                    <DateLabel date={campaign.startDate}/>
                  </td>
                  <td>
                    <DateLabel date={campaign.endDate} />
                  </td>
                  <td>
                    <TagDisplayGrid tags={campaign.tags} />
                  </td>
                </tr>
            )
          }
          </tbody>
        </Table>
        <div style={{textAlign: 'center'}}>
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={paginationPages}
            maxButtons={10}
            activePage={paginationActivePage}
            onSelect={paginationOnPageChange} />
        </div>
      </div>
    );
  }
}
