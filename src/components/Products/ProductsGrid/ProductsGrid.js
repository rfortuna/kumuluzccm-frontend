/**
 * Created by urbanmarovt on 13/05/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';

export default class ProductsGrid extends Component {
  static propTypes = {
    products: PropTypes.array.isRequired,
    onProductClick: PropTypes.func.isRequired
  };

  render() {
    const {products, onProductClick} = this.props;
    return (
      <div>
        <Table className="table" sortable hover>
          <thead>
          <tr>
            <th>
              <strong className="name-header">Name</strong>
            </th>
          </tr>
          </thead>
          <tbody>
          {
            products.map(
              (product) =>
                <tr onClick={() => onProductClick(product.id)} key={product.id} className="clickable">
                  <td>
                    {product.name}
                  </td>
                </tr>
            )
          }
          </tbody>
        </Table>
      </div>
    );
  }
}
