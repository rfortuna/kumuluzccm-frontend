/**
 * Created by urbanmarovt on 13/05/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table} from 'react-bootstrap';

export default class PlatformsGrid extends Component {
  static propTypes = {
    platforms: PropTypes.array.isRequired,
    onPlatformClick: PropTypes.func.isRequired
  };

  render() {
    const {platforms, onPlatformClick} = this.props;
    return (
      <div>
        <Table className="table" sortable hover>
          <thead>
          <tr>
            <th>
              <strong className="name-header">Name</strong>
            </th>
          </tr>
          </thead>
          <tbody>
          {
            platforms.map(
              (platform) =>
                <tr onClick={() => onPlatformClick(platform.id)} key={platform.id} className="clickable">
                  <td>
                    {platform.type}
                  </td>
                </tr>
            )
          }
          </tbody>
        </Table>
      </div>
    );
  }
}
