/**
 * Created by rokfortuna on 5/12/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';
import $ from 'jquery';

import {DropzoneCustom, SimpleSelect} from 'components';
import {refactorProducts} from 'utils/dataSelectRefactor';

import styles from './PlatformNewForm.scss';

const validate = (values) => {
  const errors = {};

  if (values.platform === 'IOS') {
    if (!values.certPass) {
      errors.certPass = 'Required';
    }

    if (!values.certFile) {
      errors.certFile = 'Required';
    }
  } else {
    if (!values.apiKey) {
      errors.apiKey = 'Required';
    }
  }

  return errors;
};


@reduxForm({
    form: 'PlatformNewForm',
    fields: ['platform', 'apiKey', 'certFile' , 'certPass', 'product', 'production'],
    validate: validate
  },
  state => ({
    initialValues: {}
  }))
export default class PlatformNewForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object,
    setCertFileData: PropTypes.func,
    certFileData: PropTypes.object,
    products: PropTypes.array
  };


  onDrop = ( filesToUpload, e ) => {
    const {fields: {certFileData}, setCertFileData} = this.props;
    setCertFileData(filesToUpload[0]);
  };

  render() {
    const {fields: { platform, apiKey, certFile, certPass, product, production},
      certFileData, handleSubmit, handleCancel, submitting, errors,
      products} = this.props;

    // validation
    const apiKeyValidation = {};

    if (apiKey.touched && apiKey.error) {
      apiKeyValidation.help = apiKey.error;
      apiKeyValidation.bsStyle = 'error';
    }

    const certPassValidation = {};
    if (certPass.touched && certPass.error ) {
      certPassValidation.help = certPass.error;
      certPassValidation.bsStyle = 'error';
    }


    return (
      <form onSubmit={handleSubmit} className="form-horizontal">

        <SimpleSelect labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Choose product" {...product} data={refactorProducts(products)}/>

        <SimpleSelect labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                      label="Choose platform" {...platform} data={[{label: 'iOS', value: 'IOS'}, {label: 'Android', value: 'ANDROID'}]}/>

        {platform.value === 'ANDROID' &&
          <Input type="text" label="API key" labelClassName="col-xs-2"
                 wrapperClassName="col-xs-10" {...apiKey} {...apiKeyValidation} />
        }
        {platform.value === 'IOS' &&
          <span>
            <div className="form-group">
              <label className="col-sm-2 control-label"> Certificate</label>
              <div className="col-sm-10">
                <DropzoneCustom
                  onDrop={this.onDrop}
                  label={'Drop or click here to insert the certificate of your application.'}
                  field={certFile}
                  file={certFileData}
                />
              </div>
            </div>
            <Input type="password" label="Certificate password" labelClassName="col-xs-2"
                   wrapperClassName="col-xs-10" {...certPass} {...certPassValidation} />
            <Input bsClass="icheckbox_square-green" type="checkbox" label="Production" {...production} labelClassName="col-xs-offset-2 col-xs-10"/>
          </span>
        }

        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}

//<RadioInline
//  field={platform}
//  label="Choose platform"
//  options={[{label: 'iOS', value: 'IOS'}, {label: 'Android', value: 'ANDROID'}]}
///>

//<Input type="text" label="Name" labelClassName="col-xs-2"
//       wrapperClassName="col-xs-10" {...name} />
