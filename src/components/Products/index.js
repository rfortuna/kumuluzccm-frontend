/**
 * Created by rokfortuna on 5/12/16.
 */

export PlatformNewForm from './PlatformNewForm/PlatformNewForm';
export ProductNewForm from './ProductNewForm/ProductNewForm';
export ProductsGrid from './ProductsGrid/ProductsGrid';
export PlatformsGrid from './PlatformsGrid/PlatformsGrid';
