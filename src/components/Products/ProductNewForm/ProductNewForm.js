/**
 * Created by urbanmarovt on 13/05/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';
import $ from 'jquery';

import {DropzoneCustom, RadioInline, SimpleSelect} from 'components';

const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Required';
  }

  return errors;
};


@reduxForm({
    form: 'ProductNewForm',
    fields: ['name'],
    validate: validate
  },
  state => ({}))
export default class ProductNewForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object
  };


  render() {
    const {fields: {name}, handleSubmit, handleCancel, submitting, errors} = this.props;

//     validation
    const nameValidation = {};

    if (name.touched && name.error) {
      nameValidation.help = name.error;
      nameValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">

        <Input type="text" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
               label="Name" {...name} {...nameValidation} />

        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              <Button type="button" disabled={submitting} onClick={handleCancel}><Glyphicon glyph="remove-circle" />&nbsp;Cancel</Button>
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}

//<RadioInline
//  field={platform}
//  label="Choose platform"
//  options={[{label: 'iOS', value: 'IOS'}, {label: 'Android', value: 'ANDROID'}]}
///>

//<Input type="text" label="Name" labelClassName="col-xs-2"
//       wrapperClassName="col-xs-10" {...name} />
