/**
 * Created by rokfortuna on 4/4/16.
 */

import React, {Component, PropTypes} from 'react';
import {Table, Button, Glyphicon} from 'react-bootstrap';


export default class TenantsGrid extends Component {
  static propTypes = {
    tenants: PropTypes.array.isRequired,
  };

  render() {
    const {tenants} = this.props;

    return (
      <div style={{height: '500px'}}>
        <Table className="table">
          <thead>
          <tr>
            <th>
              Logo
            </th>
            <th>
              Name
            </th>
          </tr>
          </thead>
          <tbody>
          {
            tenants.map(
              (tenant) =>
                <tr key={tenant.id}>
                  <td>
                    <img src={tenant.logo} style={{height: '35px'}} />
                  </td>
                  <td>
                    {tenant.name}
                  </td>
                </tr>
            )
          }
          </tbody>
        </Table>
      </div>
    );
  }
}

/*
* <Button onClick={() => toastr.confirm('Are you sure you want to delete the tenant?',
 {
 onOk: () => {onDeleteTenantClick(tenant.id); console.log(`deleted a tenant ${tenant.id}`)},
 })}>
 <Glyphicon glyph="remove" />
 </Button>
 <Button><Glyphicon glyph="edit" /></Button>*/