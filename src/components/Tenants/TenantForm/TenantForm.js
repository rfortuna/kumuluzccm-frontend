/**
 * Created by rokfortuna on 3/31/16.
 */

import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Input, Button, ButtonToolbar, Glyphicon} from 'react-bootstrap';

import {DropzoneCustom} from 'components';
import { validateEmail } from 'utils/validation';


const validate = (values) => {

  const errors = {};
  if (!values.name) {
    errors.name = 'Required';
  }

  if (values.mailFrom && validateEmail(values.mailFrom)) {
    errors.mailFrom = 'Invalid email format';
  }

  return errors;
};

@reduxForm({
  form: 'TenantForm',
  fields: ['name', 'logoImage', 'mailFrom', 'mailUser', 'smsUser'],
  validate: validate
},
state => ({

}))
export default class TenantForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object,
    submitting: PropTypes.bool,
    fields: PropTypes.object,
    logoFile: PropTypes.object,
    showLogoPreview: PropTypes.func,
    handleResetForm: PropTypes.func,
    handleCancel: PropTypes.func
  };

  onDrop = ( filesToUpload ) => {
    const {showLogoPreview, fields: {logoImage}} = this.props;
    logoImage.onChange(filesToUpload);
    showLogoPreview(filesToUpload[0]);
  };

  render() {
    const {fields: {name, logoImage, mailFrom, mailUser, smsUser}, handleSubmit, submitting, errors, logoFile, handleResetForm, handleCancel} = this.props;

    // validation
    const nameValidation = {},
      mailFromValidation = {};

    if (name.touched && name.error) {
      nameValidation.help = name.error;
      nameValidation.bsStyle = 'error';
    }

    if (mailFrom.error) {
      mailFromValidation.help = mailFrom.error;
      mailFromValidation.bsStyle = 'error';
    }

    return (
      <form onSubmit={handleSubmit} className="form-horizontal">
        <Input type="text" label="Name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...name} {...nameValidation} />
        <div className="form-group">
          <label className="col-sm-2 control-label"> Logo </label>
          <div className="col-sm-10">
            <DropzoneCustom
              onDrop={ this.onDrop }
              label={'Drop the logo image here, or click to select it.'}
              field={logoImage}
              file={logoFile}
            />
          </div>
        </div>
        {logoFile &&
        <div className="form-group">
          <label className="col-sm-2 control-label"> Preview </label>
          <div className="col-sm-10">
            { logoFile &&
            <img src={logoFile.preview} style={{height: '60px'}}/>
            }
          </div>
        </div>}
        <div className="hr-line-dashed"></div>
        <Input type="text" label="Sending email" labelClassName="col-xs-2"
                wrapperClassName="col-xs-10" {...mailFrom} {...mailFromValidation} />
        <Input type="text" label="Sending email name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...mailUser} />
        <Input type="text" label="Sending sms name" labelClassName="col-xs-2"
               wrapperClassName="col-xs-10" {...smsUser}/>
        <div className="hr-line-dashed"></div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-10">
            <ButtonToolbar>
              <Button type="submit" disabled={submitting || Object.keys(errors).length !== 0}>
                <Glyphicon glyph="save" />&nbsp;Save
              </Button>
              {handleResetForm &&
              <Button onClick={()=> handleResetForm('TenantForm')}>
                <Glyphicon glyph="remove" />&nbsp;Clear
              </Button>}
              {handleCancel &&
              <Button onClick={()=> handleCancel()}>
                <Glyphicon glyph="remove" />&nbsp;Cancel
              </Button>
              }
            </ButtonToolbar>
          </div>
        </div>
      </form>
    );
  }
}
