/**
 * Created by urbanmarovt on 03/07/16.
 */


import React, {Component, PropTypes} from 'react';
import {Modal, ButtonGroup, Button} from 'react-bootstrap';
import {reduxForm} from 'redux-form';
import CopyToClipboard from 'react-copy-to-clipboard';
import {toastr} from 'react-redux-toastr';

import {DropzoneCustom} from 'components';

import styles from './UploadImageModal.scss';

@reduxForm({
    form: 'UploadImageForm',
    fields: ['image']
  },
  state => ({}))
export default class UploadImageModal extends Component {

  static propTypes = {
    uploadImageModalVisible: PropTypes.bool,
    closeUploadImageModal: PropTypes.func,
    uploadNewImage: PropTypes.func,
    showImagePreview: PropTypes.func,
    imageUploading: PropTypes.bool,
    uploadImage: PropTypes.object,
    changeFieldValue: PropTypes.func,
    clearUploadImage: PropTypes.func,
    addImageToTenantImages: PropTypes.func
  };

  onDrop = ( filesToUpload ) => {
    const {showImagePreview, fields: {image}} = this.props;
    image.onChange(filesToUpload);
    showImagePreview(filesToUpload[0]);
  };

  upload = () => {
    const {uploadNewImage, fields: {image}, addImageToTenantImages} = this.props;

    uploadNewImage(image.value[0]).then((res) => {
      if (res.error) {
        console.log(res.error);
      } else {
        addImageToTenantImages(res.result.body);
      }
    });
  };

  close = () => {
    const {changeFieldValue, closeUploadImageModal, clearUploadImage} = this.props;
    changeFieldValue('UploadImageForm', 'image', '');
    clearUploadImage();
    closeUploadImageModal();
  };

  clear = () => {
    const {changeFieldValue, clearUploadImage} = this.props;
    changeFieldValue('UploadImageForm', 'image', '');
    clearUploadImage();
  };

  render() {
    const {fields: {image}, uploadImageModalVisible, uploadImage} = this.props;

    return (
      <Modal show={uploadImageModalVisible} onHide={this.close}>
        <Modal.Header closeButton>
          <Modal.Title>Upload image</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {!uploadImage.data &&
          <div className="row">
            <div className="col-xs-12">
              <DropzoneCustom
                onDrop={ this.onDrop }
                label={'Drop the image here, or click to upload it.'}
                field={image}
                file={uploadImage.imageFile}
                accept="image/*"/>
            </div>
          </div> ||
            <div className="row">
              <div className="col-xs-8">
                <p>{uploadImage.data.url}</p>
              </div>
              <div className="col-xs-4">
                <CopyToClipboard text={uploadImage.data.url}
                                 onCopy={() => toastr.success('You have successfully copied url to clipboard', {timeOut: 4000}) }>
                  <Button>Copy to clipboard</Button>
                </CopyToClipboard>
              </div>
            </div>
          }
          <div className="row">
          {uploadImage.imageFile &&
            <div className={styles.preview}>
              <label className="col-sm-2 control-label"> Preview </label>
              <div className="col-sm-10">
                { uploadImage.imageFile &&
                <img src={uploadImage.imageFile.preview} className={styles.image} />
                }
              </div>
            </div>}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button disabled={!uploadImage.imageFile} onClick={this.upload}>
            {uploadImage.creating && <i className="fa fa-spinner fa-spin"></i> || <span>Upload</span>}
          </Button>
          <Button onClick={this.clear}>Clear</Button>
          <Button onClick={this.close}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
