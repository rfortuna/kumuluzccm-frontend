/**
 * Created by rokfortuna on 13/07/16.
 */


import React, {Component, PropTypes} from 'react';
import {Modal, ButtonGroup, Button, FormControls, Input, Glyphicon} from 'react-bootstrap';
import {reduxForm} from 'redux-form';
import {toastr} from 'react-redux-toastr';

import styles from './AddExistingReceiverModal.scss';

@reduxForm({
    form: 'AddExistingReceiverForm',
  },
  state => ({
    addExistingReceiverModal: state.views.modals.addExistingReceiverModal,
    initialValues: state.views.modals.addExistingReceiverModal.receiver
  }))
export default class AddExistingReceiverModal extends Component {

  static propTypes = {
    addExistingReceiverModalVisible: PropTypes.bool,
    closeAddExistingReceiverModal: PropTypes.func,
    handleSubmit: PropTypes.func
  };

  close = () => {
    const {closeAddExistingReceiverModal} = this.props;
    closeAddExistingReceiverModal();
    // const {changeFieldValue, closeUploadImageModal, clearUploadImage} = this.props;
    // changeFieldValue('UploadImageForm', 'image', '');
    // clearUploadImage();
    // closeUploadImageModal();
  };

  clear = () => {
    // const {changeFieldValue, clearUploadImage} = this.props;
    // changeFieldValue('UploadImageForm', 'image', '');
    // clearUploadImage();
  };

  render() {
    const {fields} = this.props;
    const {...dynamicFields} = fields;
    const { addExistingReceiverModalVisible, addExistingReceiverModal, handleSubmit} = this.props;
    
    return (
      <Modal show={addExistingReceiverModalVisible} onHide={this.close}>
        <Modal.Header closeButton>
          <Modal.Title>Add existing receiver to list</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <form onSubmit={handleSubmit} className="form-horizontal">
          <FormControls.Static label="First name" labelClassName="col-xs-4" wrapperClassName="col-xs-8" value={addExistingReceiverModal.receiver.firstName} />
          <FormControls.Static label="Last name" labelClassName="col-xs-4" wrapperClassName="col-xs-8" value={addExistingReceiverModal.receiver.lastName}/>
          <FormControls.Static label="Email" labelClassName="col-xs-4" wrapperClassName="col-xs-8" value={addExistingReceiverModal.receiver.email}/>
          <FormControls.Static label="Mobile phone" labelClassName="col-xs-4" wrapperClassName="col-xs-8" value={addExistingReceiverModal.receiver.mobilePhone}/>
          {Object.keys(dynamicFields).map( (field) =>
            <Input type="text" key={`dynamic_field${field}`} label={dynamicFields[field].name} {...dynamicFields[field]} labelClassName="col-xs-4 text-align-right" wrapperClassName="col-xs-8"/>
          )}
        </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleSubmit}>
            <Glyphicon glyph="plus" />&nbsp;Add
          </Button>
          <Button onClick={this.close}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
