import {isTokenExpired} from 'utils/token';

import {refreshToken} from 'redux/modules/authentication';

export default function clientMiddleware(client) {
  return ({dispatch, getState}) => {
    return next => action => {

      if (typeof action === 'function') {
        return action(dispatch, getState);
      }

      const { promise, types, ...rest } = action; // eslint-disable-line no-redeclare

      if (!promise) {
        return next(action);
      }

      const [REQUEST, SUCCESS, FAILURE] = types;
      next({...rest, type: REQUEST});

      return new Promise((resolve, reject) => {

        const token = getState().authentication.token;
        const tokenExpiresAt = getState().authentication.tokenExpiresAt;

        console.log("TOKEN");
        console.log(token);
        //token is not defined
        if (!token) {
          resolve();
        } else if (!isTokenExpired(tokenExpiresAt)) {
          console.log('TOKEN IS OK');
          resolve();
        } else {
          console.log('TOKEN HAS EXPIRED');
          const refreshTokenAction = refreshToken(getState().authentication.refreshToken);
          const [TOKEN_REQUEST, TOKEN_SUCCESS, TOKEN_FAILURE] = refreshTokenAction.types;

          next({...rest, type: TOKEN_REQUEST});
          refreshTokenAction.promise(client).then(
            (result) => {
              console.log("RESULT ON REFRESH");
              console.log(result);
              next({...rest, result, type: TOKEN_SUCCESS});
              resolve();
            },
            (error) => {
              console.log("ERROR ON REFRESH");
              console.log(error);
              next({...rest, error, type: TOKEN_FAILURE});
              reject();
            }
          );
        }
      }).then(() => {
        return promise(client, getState().authentication.token, getState().global.profile.id).then(
          (result) => next({...rest, result, type: SUCCESS}),
          (error) => next({...rest, error, type: FAILURE})
        ).catch((error)=> {
          console.error('MIDDLEWARE ERROR:', error);
          next({...rest, error, type: FAILURE});
        });
      });

    };
  };
}
