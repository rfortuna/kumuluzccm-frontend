/**
 * Created by rokfortuna on 3/31/16.
 */



const SET_CONFIG = 'kumuluzccm/global/config/SET_CONFIG';


const initialState = {
  websocketUrl :  null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_CONFIG:
      return {
        ...state,
        ...action.data
      }
    default:
      return state;
  }
}

export function setConfig(config) {
  return {
    type: SET_CONFIG,
    data: config
  }
}