/**
 * Created by rokfortuna on 3/31/16.
 */

import cookie from 'react-cookie';

const SET_SELECTED_TENANT_ID = 'kumuluzccm/global/tenants/SET_SELECTED_TENANT_ID';

const LOAD_TENANTS = 'kumuluzccm/global/tenants/LOAD_TENANTS';
const LOAD_TENANTS_SUCCESS = 'kumuluzccm/global/tenants/LOAD_TENANTS_SUCCESS';
const LOAD_TENANTS_FAIL = 'kumuluzccm/global/tenants/LOAD_TENANTS_FAIL';

const initialState = {
  loading: false,
  // TODO change back when api starts working
//  options: [],
  options: [{value: 'id1', label: 'Abanka'}, {value: 'id2', label: 'Mercator'}]
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case SET_SELECTED_TENANT_ID:
      cookie.save('tenant_id', action.data.tenantId, { path: '/' });
      return {
        ...state,
        selectedTenantId: action.data.tenantId
      };
    case LOAD_TENANTS:
      return {
        ...state,
        loading: true
      };
    case LOAD_TENANTS_SUCCESS:
      return {
        ...state,
        loading: false,
        options: action.result.body
      };
    case LOAD_TENANTS_FAIL:
      return {
        ...state,
        // TODO change back when api starts working
//        options: [],
        options: [{value: 'id1', label: 'Abanka'}, {value: 'id2', label: 'Mercator'}],
        loading: false
      };
    default:
      return state;
  }
}

export function load() {
  return {
    types: [LOAD_TENANTS, LOAD_TENANTS_SUCCESS, LOAD_TENANTS_FAIL],
    promise: (client, token) => client.get(`/v1/tenants`, {token: token})
  };
}

export function isLoaded(globalState) {
  return globalState.global.tenants.options.length > 0;
}



