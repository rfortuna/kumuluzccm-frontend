/**
 * Created by rokfortuna on 3/31/16.
 */

import { combineReducers } from 'redux';

import profileReducer from './profile';
import configReducer from './config';

export default combineReducers({
  profile: profileReducer,
  config: configReducer
});
