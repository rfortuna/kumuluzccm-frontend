/**
 * Created by rokfortuna on 3/31/16.
 */

import cookie from 'react-cookie';
import {refactorProfileOptions} from 'utils/dataSelectRefactor';

const LOAD = 'kumuluzccm/global/profile/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/global/profile/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/global/profile/LOAD_FAIL';
const LOAD_DATA = 'kumuluzccm/global/profile/LOAD_DATA';
const LOAD_DATA_SUCCESS = 'kumuluzccm/global/profile/LOAD_DATA_SUCCESS';
const LOAD_DATA_FAIL = 'kumuluzccm/global/profile/LOAD_DATA_FAIL';
const SET_PROFILE_VALUES = 'kumuluzccm/global/profile/SET_PROFILE_VALUES';
const CLEAR_STATE = 'kumuluzccm/global/profile/CLEAR_STATE';

const initialState = {
  loading: false,
  // TODO change back when api starts working
//  username: null,
  type: 'user',
  id: null,
  options: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_PROFILE_VALUES:
      console.log(action.data);
      return {
        ...state,
        type: action.data.type,
        id: action.data.id
      };
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        id: action.result.body.id,
        type: 'user',
        options: refactorProfileOptions(action.result.body)
      };
    case LOAD_FAIL:
      cookie.remove('profile_type', {path: '/'});
      cookie.remove('profile_id', {path: '/'});
      return {
        ...state,
        loading: false,
        options: [],
        id: null,
        type: null

      };
    case LOAD_DATA:
      return {
        ...state,
        loading: true
      };
    case LOAD_DATA_SUCCESS:
      return {
        ...state,
        options: refactorProfileOptions(action.result.body)
      };
    case LOAD_DATA_FAIL:
      return {
        ...state,
        loading: false,
        options: [],
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function load(token) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`/v1/users`, {token: token})
  };
}

export function loadData() {
  return {
    types: [LOAD_DATA, LOAD_DATA_SUCCESS, LOAD_DATA_FAIL],
    promise: (client, token) => client.get(`/v1/users`, {token: token})
  };
}

export function setProfileValues(profileType, profileId) {
  return {
    type: SET_PROFILE_VALUES,
    data: {
      type: profileType,
      id: profileId
    }
  }
}

export function isDataLoaded(globalState) {
  return globalState.global.profile.options.length > 0;
}

export function resetProfileState() {
  return {
    type: CLEAR_STATE
  }
}
