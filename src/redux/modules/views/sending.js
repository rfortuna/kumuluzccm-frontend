/**
 * Created by urbanmarovt on 14/03/16.
 */

const SAVE_LISTS = 'kumuluzccm/sending/SAVE_LISTS';
const SAVE_SETUP = 'kumuluzccm/sending/SAVE_SETUP';
const SAVE_CONTENT = 'kumuluzccm/sending/SAVE_CONTENT';
const SAVE_CHANNEL_SETUP = 'kumuluzccm/sending/SAVE_CHANNEL_SETUP';
const UPDATE_CHANNEL_FIELDS = 'kumuluzccm/sending/UPDATE_CHANNEL_FIELDS';
const CLEAR_DATA = 'kumuluzccm/sending/CLEAR_DATA';

const SHOW_RECEIVERS_CONFIRM = 'kumuluzccm/sending/SHOW_RECEIVERS_CONFIRM';
const HIDE_RECEIVERS_CONFIRM = 'kumuluzccm/sending/HIDE_RECEIVERS_CONFIRM';
const SHOW_CONTENT_SHORT_PREVIEW = 'kumuluzccm/sending/SHOW_CONTENT_SHORT_PREVIEW';
const SHOW_CONTENT_EMAIL_PREVIEW = 'kumuluzccm/sending/SHOW_CONTENT_EMAIL_PREVIEW';
const SAVE_CONTENT_EMAIL_PREVIEW = 'kumuluzccm/sending/SAVE_CONTENT_EMAIL_PREVIEW';

const CLEAR_SHORT_CONTENT_FILE = 'kumuluzccm/sending/CLEAR_SHORT_CONTENT_FILE';
const CLEAR_CONTENT_EMAIL = 'kumuluzccm/sending/CLEAR_CONTENT_EMAIL';
const PREFILL_SENDING = 'kumuluzccm/sending/PREFILL_SENDING';

const ADD_PUSH_CUSTOM_PROPERTY = 'kumuluzccm/sending/ADD_PUSH_CUSTOM_PROPERTY';
const REMOVE_PUSH_CUSTOM_PROPERTY = 'kumuluzccm/sending/REMOVE_PUSH_CUSTOM_PROPERTY';

const initialState = {
  channelSetup: {
    data: {pushCustomPropertiesString: '{}'},
    errors: {},
    touched: false,
    enabled: true,
    fields: ['product', 'channelId', 'mailFrom', 'mailUser'],
    productPlatforms: [],
    numOfCustomProperties: 0
  },
  lists: {
    data: '',
    errors: {},
    touched: false,
    enabled: true
  },
  setup: {
    data: {},
    errors: {},
    touched: false,
    enabled: false
  },
  content: {
    data: {},
    errors: {},
    touched: false,
    enabled: false,
    contentEMAILPreview: '',
    templateFile: {}
  },
  confirm: {
    data: {},
    errors: {},
    touched: false,
    enabled: false,
    showReceivers: false
  }
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case SAVE_LISTS:
      return {
        ...state,
        lists: {
          data: action.data,
          errors: {},
          touched: true,
          enabled: true
        }
      };
    case SAVE_SETUP:
      return {
        ...state,
        setup: {
          data: action.data.data,
          errors: action.data.errors,
          touched: true,
          enabled: true
        }
      };
    case SAVE_CONTENT:
      return {
        ...state,
        content: {
          data: action.data.data,
          errors: action.data.errors,
          touched: true,
          enabled: true,
          contentEMAILPreview: action.data.data.contentEMAIL
        }
      };
    case SAVE_CHANNEL_SETUP:
      return {
        ...state,
        channelSetup: {
          ...state.channelSetup,
          data: action.data.data,
          errors: action.data.errors,
          touched: true,
          enabled: true,
          fields: action.data.fields
        }
      };
    case ADD_PUSH_CUSTOM_PROPERTY:
      return {
        ...state,
        channelSetup: {
          ...state.channelSetup,
          numOfCustomProperties: state.channelSetup.numOfCustomProperties + 1
        }
      };
    case REMOVE_PUSH_CUSTOM_PROPERTY:
      return {
        ...state,
        channelSetup: {
          ...state.channelSetup,
          numOfCustomProperties: state.channelSetup.numOfCustomProperties - 1,
        }
      };
    case UPDATE_CHANNEL_FIELDS:
      const fields = initialState.channelSetup.fields.slice();
      action.data.fields.forEach((field) => {
        fields.push(field.type);
      });
      return {
        ...state,
        channelSetup: {
          ...state.channelSetup,
          fields: fields,
          productPlatforms: action.data.fields
        }
      };
    case CLEAR_DATA:
      return initialState;
    case SHOW_RECEIVERS_CONFIRM:
      return {
        ...state,
        lists: {
          ...state.lists,
          showLists: true
        }
      };
    case HIDE_RECEIVERS_CONFIRM:
      return {
        ...state,
        lists: {
          ...state.lists,
          showLists: false
        }
      };
    case SHOW_CONTENT_SHORT_PREVIEW:
      return {
        ...state,
        content: {
          ...state.content,
          contentPreviewShort: action.data.contentPreview
        }
      };
    case SHOW_CONTENT_EMAIL_PREVIEW:
      return {
        ...state,
        content: {
          ...state.content,
          contentEMAILPreview: action.data.contentPreview,
          templateFile: action.data.templateFile
        }
      };
    case CLEAR_SHORT_CONTENT_FILE:
      return {
        ...state,
        content: {
          ...state.content,
          contentPreviewShort: ''
        }
      };
    case CLEAR_CONTENT_EMAIL:
      return {
        ...state,
        content: {
          ...state.content,
          contentEMAILPreview: '',
          templateFile: {}
        }
      };
    case SAVE_CONTENT_EMAIL_PREVIEW:
      return {
        ...state,
        content: {
          ...state.content,
          contentEMAILPreview: action.data
        }
      };
    case PREFILL_SENDING:
      let receiverType = 'lists';
      if (action.data.allReceivers) {
        receiverType = 'all';
      } else if (action.data.channels.find((element, index, array) => {
        if (element.anonymous) {return element};
        })) {
        receiverType = 'application'
      };

      let receiverLists = action.data.receiverLists.reduce((prev, next) => {return `${prev}${next.id},`}, '');
      receiverLists = receiverLists.substring(0, receiverLists.length - 1);
      return {
        channelSetup: {
          data: {mailFrom: action.data.mailFrom, mailUser: action.data.mailUser},
          errors: {},
          touched: true,
          enabled: true,
          fields: ['product', 'channelId', 'mailFrom', 'mailUser'],
          productPlatforms: []
        },
        lists: {
          data: {receiverType: receiverType, receiverLists: receiverLists},
          errors: {},
          touched: true,
          enabled: true
        },
        setup: {
          data: {campaign: action.data.campaign.id, deliverAfter: action.data.deliverAfter,
            subject: action.data.subject, ignoreUserPreferences: action.data.ignoreUserPreferences,
            timezone: action.data.timezone ? action.data.timezone.id : null},
          errors: {},
          touched: true,
          enabled: true
        },
        content: {
          data: {contentShort: action.data.contentShort, contentEMAIL: action.data.contentEMAIL, contentShortType: 'new', contentShortType: 'upload'},
          errors: {},
          touched: true,
          enabled: true,
          contentEMAILPreview: action.data.contentEMAIL,
          templateFile: {}
        },
        confirm: {
          data: {},
          errors: {},
          touched: true,
          enabled: true,
          showReceivers: false
        }
      };
    default:
      return state;
  }
}

export function saveReceiversData(data) {
  return {
    type: SAVE_LISTS,
    data:  data
  };
}

export function clearSendingData() {
  return {
    type: CLEAR_DATA,
    data: {}
  };
}

export function saveChannelSetup(data, errors, fields) {
  return {
    type: SAVE_CHANNEL_SETUP,
    data: {
      data: data,
      errors: errors,
      fields: fields
    }
  }
}

export function updateChannelFields(fields) {
  return {
    type: UPDATE_CHANNEL_FIELDS,
    data: {
      fields: fields
    }
  }
}

export function saveSetup(data, errors) {
  return {
    type: SAVE_SETUP,
    data: {
      data: data,
      errors: errors
    }
  };
}

export function saveContent(data, errors) {
  return {
    type: SAVE_CONTENT,
    data: {
      data: data,
      errors: errors
    }
  };
}

export function showReceiversConfirm() {
  return {
    type: SHOW_RECEIVERS_CONFIRM,
    data: {}
  };
}

export function hideReceiversConfirm() {
  return {
    type: HIDE_RECEIVERS_CONFIRM,
    data: {}
  };
}

export function showContentShortPreview(contentPreview) {
  return {
    type: SHOW_CONTENT_SHORT_PREVIEW,
    data: {contentPreview: contentPreview}
  };
}


export function showContentEMAILPreview(contentPreview, templateFile) {
  return {
    type: SHOW_CONTENT_EMAIL_PREVIEW,
    data: {contentPreview: contentPreview, templateFile: templateFile}
  };
}

export function clearContentShortFile() {
  return {
    type: CLEAR_SHORT_CONTENT_FILE,
    data: {}
  }
}


export function clearContentEMAIL() {
  return {
    type: CLEAR_CONTENT_EMAIL,
    data: {}
  }
}

export function prefillSending(newslettter) {
  return {
    type: PREFILL_SENDING,
    data: newslettter
  }
}

export function saveContentEMAILPreview(data) {
  return {
    type: SAVE_CONTENT_EMAIL_PREVIEW,
    data: data
  }
}

export function addPushCustomProperty() {
  return {
    type: ADD_PUSH_CUSTOM_PROPERTY
  }
}

export function removePushCustomProperty() {
  return {
    type: REMOVE_PUSH_CUSTOM_PROPERTY
  }
}
