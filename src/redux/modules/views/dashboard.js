/**
 * Created by rokfortuna on 25/02/16.
 */
import lodash from 'lodash';

import {refactorWebsocketLiveData} from './../../../utils/refactor';

const ON_MESSAGE_SOCKET = 'kumuluzccm/dashboard/ON_MESSAGE_SOCKET';
const CLEAR_STATE = 'kumuluzccm/dashboard/CLEAR_STATE';
const SET_SELECTED_CHANNEL = 'kumuluzccm/dashboard/SET_SELECTED_CHANNEL';
const SET_SELECTED_TIME = 'kumuluzccm/dashboard/SET_SELECTED_TIME';

const initialState = {
  selectedChannel: '',
  selectedTime: 'd',
  liveStatistics: {
    rates: [['x'], ['EMAIL'], ['SMS'], ['PUSH NOTIFICATION']],
    bars: [['PENDING'], ['DELIVERED'], ['BOUNCED'], ['CONFIRMED BY USER'], ['DISABLED BY USER'], ['PROVIDER ERROR'], ['ADDRESS UNKNOWN']]
  }
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case ON_MESSAGE_SOCKET:
      const {rdata, rate} = action.data;
      const {ratesNew, barsNew} = refactorWebsocketLiveData(lodash.cloneDeep(state.liveStatistics.rates), lodash.cloneDeep(state.liveStatistics.bars), rdata, rate);
      return {
        ...state,
        liveStatistics: {
          rates: ratesNew,
          bars: barsNew
        }
      };
    case SET_SELECTED_CHANNEL:
      return {
        ...state,
        selectedChannel: action.data
      };
    case SET_SELECTED_TIME:
      return {
        ...state,
        selectedTime: action.data
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function onMessageSocket(result) {
  return {
    type: ON_MESSAGE_SOCKET,
    data: result
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

export function setSelectedChannel(channel) {
  return {
    type: SET_SELECTED_CHANNEL,
    data: channel
  }
}

export function setSelectedTime(time) {
  return {
    type: SET_SELECTED_TIME,
    data: time
  }
}
