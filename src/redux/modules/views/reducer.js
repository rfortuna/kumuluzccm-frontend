/**
 * Created by rokfortuna on 25/02/16.
 */

import { combineReducers } from 'redux';
import dashboardReducer from './dashboard';
import sendingReducer from './sending';
import campaignsReducer from './campaigns';
import modalsReducer from './modals';

export default combineReducers({
  dashboard: dashboardReducer,
  sending: sendingReducer,
  campaigns: campaignsReducer,
  modals: modalsReducer
});
