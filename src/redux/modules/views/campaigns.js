/**
 * Created by urbanmarovt on 22/05/16.
 */

const SET_SELECTED_CHANNEL = 'kumuluzccm/campaigns/SET_SELECTED_CHANNEL';
const SET_SELECTED_TIME = 'kumuluzccm/campaigns/SET_SELECTED_TIME';

const initialState = {
  selectedChannel: '',
  selectedTime: 'd'
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {

    case SET_SELECTED_CHANNEL:
      return {
        ...state,
        selectedChannel: action.data
      };
    case SET_SELECTED_TIME:
      return {
        ...state,
        selectedTime: action.data
      };
    default:
      return state;
  }
}

export function setSelectedChannel(channel) {
  return {
    type: SET_SELECTED_CHANNEL,
    data: channel
  }
}

export function setSelectedTime(time) {
  return {
    type: SET_SELECTED_TIME,
    data: time
  }
}