/**
 * Created by rokfortuna on 13/07/16.
 */


const SHOW_UPLOAD_IMAGE_MODAL = 'kumuluzccm/modals/SHOW_UPLOAD_IMAGE_MODAL';
const CLOSE_UPLOAD_IMAGE_MODAL = 'kumuluzccm/modals/CLOSE_UPLOAD_IMAGE_MODAL';

const SHOW_ADD_EXISTING_RECEIVER_MODAL = 'kumuluzccm/modals/SHOW_ADD_EXISTING_RECEIVER_MODAL';
const CLOSE_ADD_EXISTING_RECEIVER_MODAL = 'kumuluzccm/modals/CLOSE_ADD_EXISTING_RECEIVER_MODAL';

const initialState = {
  uploadImageModalVisible: false,
  addExistingReceiverModal:{
    visible: false,
    receiver: {firstName: null, lastName: null, email: null, mobilePhone: null}
  }
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {

    case SHOW_UPLOAD_IMAGE_MODAL:
      return {
        ...state,
        uploadImageModalVisible: true
      };
    case CLOSE_UPLOAD_IMAGE_MODAL:
      return {
        ...state,
        uploadImageModalVisible: false
      };
    case SHOW_ADD_EXISTING_RECEIVER_MODAL:
      return {
        ...state,
        addExistingReceiverModal: {
          visible: true,
          receiver: action.data.receiver
        }
      };
    case CLOSE_ADD_EXISTING_RECEIVER_MODAL:
      return {
        ...state,
        addExistingReceiverModal: initialState.addExistingReceiverModal
      };
    default:
      return state;
  }
}

export function showUploadImageModal() {
  return {
    type: SHOW_UPLOAD_IMAGE_MODAL,
  }
}

export function closeUploadImageModal() {
  return {
    type: CLOSE_UPLOAD_IMAGE_MODAL
  }
}

export function showAddExistingReceiverModal(receiver) {
  return {
    type: SHOW_ADD_EXISTING_RECEIVER_MODAL,
    data: {receiver: receiver}
  }
}

export function closeAddExistingReceiverModal() {
  return {
    type: CLOSE_ADD_EXISTING_RECEIVER_MODAL
  }
}
