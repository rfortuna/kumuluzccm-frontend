/**
 * Created by rokfortuna on 08/03/16.
 */

import {setQueryParams} from './../../utils/queryParams.js';

const SET_INITIAL_STATE = 'kumuluzccm/queryParams/SET_INITIAL_STATE';
const CHANGE_PAGE = 'kumuluzccm/queryParams/CHANGE_PAGE';
const CHANGE_ORDER = 'kumuluzccm/queryParams/CHANGE_ORDER';
const CHANGE_SEARCH_STRING = 'kumuluzccm/queryParams/CHANGE_SEARCH_STRING';

const initialState = {
  limit: 10,
  offset: 0,
  order: '',
  searchString: ''
};

export default function reducer(state = initialState, action = {}) {

  let newState = {...state};

  switch (action.type) {
    case SET_INITIAL_STATE:
      //set default query params for the page (not neccessarily equal to initial state)
      newState = {...newState, ...action.data.newQueryParams};
      if (__CLIENT__) {
        setQueryParams(action.data.path, newState);
      }
      return newState;
    case CHANGE_PAGE:
    case CHANGE_ORDER:
    case CHANGE_SEARCH_STRING:
      newState = {...newState, ...action.data.newQueryParams};
      setQueryParams(action.data.path, newState);
      return newState;
    default:
      return state;
  }
}

export function onPageChange(path, newQueryParams) {
  return {
    type: CHANGE_PAGE,
    data: {newQueryParams: newQueryParams, path: path}
  };
}

export function onOrderChange(path, newQueryParams) {
  return {
    type: CHANGE_ORDER,
    data: {newQueryParams: newQueryParams, path: path}
  };
}

export function onSearchStringChange(path, newQueryParams) {
  return {
    type: CHANGE_SEARCH_STRING,
    data: {newQueryParams: newQueryParams, path: path}
  };
}

export function setInitialState(path, newQueryParams) {
  return {
    type: SET_INITIAL_STATE,
    data: {newQueryParams: newQueryParams, path: path}
  };
}
