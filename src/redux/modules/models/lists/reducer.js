/**
 * Created by rokfortuna on 3/14/16.
 */

import { combineReducers } from 'redux';

import lists from './list';
import listNew from './new';
import listDetails from './details';
import properties from './properties/reducer';
import receivers from './receivers/reducer';

export default combineReducers({
  list: lists,
  new: listNew,
  details: listDetails,
  properties: properties,
  receivers: receivers
});
