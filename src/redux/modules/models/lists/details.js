/**
 * Created by rokfortuna on 4/11/16.
 */

const LOAD = 'kumuluzccm/lists/details/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/lists/details/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/lists/details/LOAD_FAIL';

const EDIT = 'kumuluzccm/lists/details/EDIT';
const EDIT_CANCEL = 'kumuluzccm/lists/details/EDIT_CANCEL';

const UPDATE = 'kumuluzccm/lists/details/UPDATE';
const UPDATE_SUCCESS = 'kumuluzccm/lists/details/UPDATE_SUCCESS';
const UPDATE_FAIL = 'kumuluzccm/lists/details/UPDATE_FAIL';


const initialState = {
  name: '',
  editing: false,
  oldState: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        ...action.result.body,
      };
    case LOAD_FAIL:
      return {
        ...state,
      };
    case EDIT:
      return {
        ...state,
        editing: true,
        oldState: state
      };
    case EDIT_CANCEL:
      return {
        ...state.oldState,
        editing: false
      };
    case UPDATE:
      return {
        ...state
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        editing: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        editing: false
      };
    default:
      return state;
  }
}


export function load(listId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) =>
      client.get(`/v1/tenants/${tenantId}/receiverlists/${listId}`, {token: token})
  };
}

export function update(listId, data) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, tenantId) => client.put(`/v1/tenants/${tenantId}/receiverlists/${listId}`, {data: data, token: token}),
  };
}

export function edit() {
  return {
    type: EDIT
  };
}

export function editCancel() {
  return {
    type: EDIT_CANCEL
  };
}
