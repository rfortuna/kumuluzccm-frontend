/**
 * Created by rokfortuna on 3/14/16.
 */

const LOAD = 'kumuluzccm/lists/list/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/lists/list/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/lists/list/LOAD_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      const newCount = action.result.headers['x-total-count'];
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(newCount),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(queryParams) {

  const {limit, offset, searchString, order} = queryParams;

  const params = {
    limit: limit,
    offset: offset,
    where: ''
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} name:likeic:%${searchString}%`;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receiverlists`, {params: params, token: token})
  };
}

export function loadByStatus(queryParams, status) {

  const {limit, offset, searchString} = queryParams;

  const params = {
    limit: limit,
    offset: offset,
    where: ''
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} name:likeic:%${searchString}%`;
  }

  if (status !== undefined && status !== 'undefined' && status !== '') {
    params.where = `${params.where} importStatus:EQ:${status}`;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receiverlists`, {params: params, token: token})
  };
}
