/**
 * Created by urbanmarovt on 19/05/16.
 */


import { combineReducers } from 'redux';

import properties from './list';

export default combineReducers({
  list: properties
});
