/**
 * Created by urbanmarovt on 19/05/16.
 */

const LOAD_STATIC = 'kumuluzccm/lists/properties/LOAD_STATIC';
const LOAD_STATIC_SUCCESS = 'kumuluzccm/lists/properties/LOAD_STATIC_SUCCESS';
const LOAD_STATIC_FAIL = 'kumuluzccm/lists/properties/LOAD_STATIC_FAIL';

const LOAD_CUSTOM = 'kumuluzccm/lists/properties/LOAD_CUSTOM';
const LOAD_CUSTOM_SUCCESS = 'kumuluzccm/lists/properties/LOAD_CUSTOM_SUCCESS';
const LOAD_CUSTOM_FAIL = 'kumuluzccm/lists/properties/LOAD_CUSTOM_FAIL';

const LOAD_PROPERTIES = 'kumuluzccm/lists/properties/LOAD_PROPERTIES';
const LOAD_PROPERTIES_SUCCESS = 'kumuluzccm/lists/properties/LOAD_PROPERTIES_SUCCESS';
const LOAD_PROPERTIES_FAIL = 'kumuluzccm/lists/properties/LOAD_PROPERTIES_FAIL';

const initialState = {
  staticFields: [],
  //hardcoded static fields, because we have camel case and snake case on the backend
  staticFieldsCamelCase: ['firstName', 'lastName', 'email', 'mobilePhone'],
  customFields: [],
  // list of properties with IDs
  detailedCustomFields: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_STATIC:
      return {
        ...state,
        staticFields: [],
      };
    case LOAD_STATIC_SUCCESS:
      return {
        ...state,
        staticFields: action.result.body,
      };
    case LOAD_STATIC_FAIL:
      return {
        ...state,
        staticFields: []
      };
      case LOAD_CUSTOM:
        return {
          ...state,
          customFields: [],
        };
      case LOAD_CUSTOM_SUCCESS:
        return {
          ...state,
          customFields: action.result.body,
        };
      case LOAD_CUSTOM_FAIL:
        return {
          ...state,
          customFields: []
        };
        case LOAD_PROPERTIES:
          return {
            ...state,
            detailedCustomFields: [],
          };
        case LOAD_PROPERTIES_SUCCESS:
          return {
            ...state,
            detailedCustomFields: action.result.body,
          };
        case LOAD_PROPERTIES_FAIL:
          return {
            ...state,
            detailedCustomFields: []
          };

    default:
      return state;
  }
}

// put globalState as parameters
export function loadStatic() {

  return {
    types: [LOAD_STATIC, LOAD_STATIC_SUCCESS, LOAD_STATIC_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receivers/staticFields`, {token: token})
  };

}

export function loadCustom(listIds) {
  return {
    types: [LOAD_CUSTOM, LOAD_CUSTOM_SUCCESS, LOAD_CUSTOM_FAIL],
    promise: (client, token, tenantId) =>
      client.post(`/v1/tenants/${tenantId}/receiverlists/properties`,
        {token: token, data: listIds})
  };
}

export function loadDetailedCustom(listId) {
  return {
    types: [LOAD_PROPERTIES, LOAD_PROPERTIES_SUCCESS, LOAD_PROPERTIES_FAIL],
    promise: (client, token, tenantId) =>
      client.get(`/v1/tenants/${tenantId}/receiverlists/${listId}/properties`,
        {token: token})
  };
}
