/**
 * Created by rokfortuna on 11/07/16.
 */

import { combineReducers } from 'redux';

import actions from './actions';
import list from './list';

export default combineReducers({
  actions: actions,
  list: list
});
