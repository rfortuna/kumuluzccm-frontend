
/**
 * Created by urbanmarovt on 19/05/16.
 */

const LOAD = 'kumuluzccm/lists/receivers/list/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/lists/receivers/list/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/lists/receivers/list/LOAD_FAIL';

const LOAD_NOT_ON_LIST = 'kumuluzccm/lists/receivers/list/LOAD_NOT_ON_LIST';
const LOAD_NOT_ON_LIST_SUCCESS = 'kumuluzccm/lists/receivers/list/LOAD_NOT_ON_LIST_SUCCESS';
const LOAD_NOT_ON_LIST_FAIL = 'kumuluzccm/lists/receivers/list/LOAD_NOT_ON_LIST_FAIL';

const initialState = {
  receivers : {
    count: 0,
    data: []
  },
  receiversNotOnList: {
    count: 0,
    data: []
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        receivers: initialState.receivers
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        receivers: {
          count: action.result.headers['x-total-count'],
          data: action.result.body
        }
      };
    case LOAD_FAIL:
      return {
        ...state,
        receivers: initialState.receivers
      };
      case LOAD_NOT_ON_LIST:
        return {
          ...state,
          receiversNotOnList: initialState.receiversNotOnList
        };
      case LOAD_NOT_ON_LIST_SUCCESS:
        return {
          ...state,
          receiversNotOnList: {
            count: action.result.headers['x-total-count'],
            data: action.result.body
          }
        };
      case LOAD_NOT_ON_LIST_FAIL:
        return {
          ...state,
          receiversNotOnList: initialState.receiversNotOnList
        };
    default:
      return state;
  }
}

export function load(listId, queryParams) {

  const {limit, offset, searchString} = queryParams;

  const params = {
    limit: limit,
    offset: offset
  }

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.search = searchString;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receiverlists/${listId}/table`, {params: params, token: token})
  };
}

export function loadNotOnList(listId, queryParams) {

  const {limit, offset, searchString} = queryParams;

  const params = {
    limit: limit,
    offset: offset
  }

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.search = searchString;
  }

  return {
    types: [LOAD_NOT_ON_LIST, LOAD_NOT_ON_LIST_SUCCESS, LOAD_NOT_ON_LIST_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receivers/noton/${listId}`, {params: params, token: token})
  };
}
