/**
 * Created by rokfortuna on 11/07/16.
 */

const ADD_NEW_RECEIVER = 'kumuluzccm/lists/receivers/actions/ADD_NEW_RECEIVER';
const ADD_NEW_RECEIVER_SUCCESS = 'kumuluzccm/lists/receivers/actions/ADD_NEW_RECEIVER_SUCCESS';
const ADD_NEW_RECEIVER_FAIL = 'kumuluzccm/lists/receivers/actions/ADD_NEW_RECEIVER_FAIL';

const ADD_EXISTING_RECEIVER = 'kumuluzccm/lists/receivers/actions/ADD_EXISTING_RECEIVER';
const ADD_EXISTING_RECEIVER_SUCCESS = 'kumuluzccm/lists/receivers/actions/ADD_EXISTING_RECEIVER_SUCCESS';
const ADD_EXISTING_RECEIVER_FAIL = 'kumuluzccm/lists/receivers/actions/ADD_EXISTING_RECEIVER_FAIL';

const REMOVE_RECEIVER = 'kumuluzccm/lists/receivers/actions/REMOVE_RECEIVER';
const REMOVE_RECEIVER_SUCCESS = 'kumuluzccm/lists/receivers/actions/REMOVE_RECEIVER_SUCCESS';
const REMOVE_RECEIVER_FAIL = 'kumuluzccm/lists/receivers/actions/REMOVE_RECEIVER_FAIL';

const initialState = {

};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_NEW_RECEIVER:
    case ADD_NEW_RECEIVER_SUCCESS:
    case ADD_NEW_RECEIVER_FAIL:
    case ADD_EXISTING_RECEIVER:
    case ADD_EXISTING_RECEIVER_SUCCESS:
    case ADD_EXISTING_RECEIVER_FAIL:
    case REMOVE_RECEIVER:
    case REMOVE_RECEIVER_SUCCESS:
    case REMOVE_RECEIVER_FAIL:
    default:
      return state;
  }
}

export function addNewReceiver(listId, formData, detailedCustomFields) {
  let data = {receiver: {}, values: []};

  detailedCustomFields.forEach((field) => {
    if (formData[field.name]) {
      data.values.push({property: {id: field.id}, value: formData[field.name]});
      delete formData[field.name];
    }
  });

  Object.keys(formData).forEach((key) => {
    data.receiver[key] = formData[key];
  });

  return {
    types: [ADD_NEW_RECEIVER, ADD_NEW_RECEIVER_SUCCESS, ADD_NEW_RECEIVER_FAIL],
    promise: (client, token, tenantId) =>
      client.post(`/v1/tenants/${tenantId}/receiverlists/${listId}/receivers`,
        {token: token, data: data})
  };
}

export function addExistingReceiver(listId, receiverId, formData, detailedCustomFields) {
  let data = {receiver: {id: receiverId}, values: []};

  detailedCustomFields.forEach((field) => {
    if (formData[field.name]) {
      data.values.push({property: {id: field.id}, value: formData[field.name]});
      delete formData[field.name];
    }
  });

  return {
    types: [ADD_EXISTING_RECEIVER, ADD_EXISTING_RECEIVER_SUCCESS, ADD_EXISTING_RECEIVER_FAIL],
    promise: (client, token, tenantId) =>
      client.post(`/v1/tenants/${tenantId}/receiverlists/${listId}/receivers`,
        {token: token, data: data})
  };
}

export function removeReceiver(listId, receiverId) {

  return {
    types: [REMOVE_RECEIVER, REMOVE_RECEIVER_SUCCESS, REMOVE_RECEIVER_FAIL],
    promise: (client, token, tenantId) =>
      client.del(`/v1/tenants/${tenantId}/receiverlists/${listId}/receivers/${receiverId}`,
        {token: token})
  };
}
