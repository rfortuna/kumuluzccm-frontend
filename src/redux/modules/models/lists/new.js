/**
 * Created by rokfortuna on 22/03/16.
 */

const CREATE = 'kumuluzccm/list/new/CREATE';
const CREATE_SUCCESS = 'kumuluzccm/list/new/CREATE_SUCCESS';
const CREATE_FAIL = 'kumuluzccm/list/new/CREATE_FAIL';
const SHOW_CSV_PREVIEW = 'kumuluzccm/list/new/SHOW_CSV_PREVIEW';
const CLEAR_STATE = 'kumuluzccm/list/new/CLEAR_STATE';

const initialState = {
  creating: false,
  created: false,
  error: {},
  csvPreview: [],
  csvFile: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    case SHOW_CSV_PREVIEW:
      return {
        ...state,
        csvPreview: action.data.csvPreview,
        csvFile: action.data.csvFile
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function create(name, file) {
  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/receiverlists`, {
      // additional JSON to send
      multiPart: {
        file: file,
        list: {name: name}
      },
      token: token
    })
  };
}

export function showCSVPreview(csvPreview, csvFile ) {
  return {
    type: SHOW_CSV_PREVIEW,
    data: {csvPreview: csvPreview, csvFile: csvFile }
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  }
}