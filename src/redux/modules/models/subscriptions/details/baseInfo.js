/**
 * Created by rokfortuna on 3/31/16.
 */

const LOAD = 'kumuluzccm/subscription/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/subscription/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/subscription/LOAD_FAIL';
const CLEAR_STATE = 'kumuluzccm/subscription/CLEAR_STATE';

const initialState = {
  loaded: false,
  error: {},
  data: {
    tenant: {}
  }
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function load(tenantId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`/v1/users/${userId}/receivers/tenants/${tenantId}`, { token: token})
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}
