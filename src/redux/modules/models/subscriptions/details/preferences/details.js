/**
 * Created by urbanmarovt on 09/02/16.
 */

import {refactorReceiveDays} from 'utils/refactor';


const LOAD = 'kumuluzccm/subscription/preferences/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/subscription/preferences/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/subscription/preferences/LOAD_FAIL';
const UPDATE = 'kumuluzccm/subscription/preferences/UPDATE';
const UPDATE_SUCCESS = 'kumuluzccm/subscription/preferences/UPDATE_SUCCESS';
const UPDATE_FAIL = 'kumuluzccm/subscription/preferences/UPDATE_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: {}
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          ...action.result.body,
          receiveDays: refactorReceiveDays(action.result.body.receiveDays)
        },
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case UPDATE:
      return {
        ...state,
        editing: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        editing: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        editing: false
      };
    default:
      return state;
  }
}

export function load(subscriptionId,preferenceId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`/v1/users/${userId}/tenants/${subscriptionId}/preferences/${preferenceId}`, {token: token})
  };
}

export function update(subscriptionId, preferenceId, priority, data) {
  let receiveDays = '';
  receiveDays = data.sunday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.monday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.tuesday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.wednesday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.thursday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.friday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.saturday ? `${receiveDays}1` : `${receiveDays}0`;

  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, userId) => client.put(`/v1/users/${userId}/tenants/${subscriptionId}/preferences/${preferenceId}`, {
      token: token,
      data: {
        channelType: data.channelType,
        receiveFrom: data.from,
        receiveTo: data.to,
        receiveDays: receiveDays,
        channelDisabled: data.disabled,
        priority: priority,
        timezone: null
      }
    })
  };
}

export function updatePriority(subscriptionId, preferenceId, priority, data) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, userId) => client.put(`/v1/users/${userId}/tenants/${subscriptionId}/preferences/${preferenceId}`, {
      token: token,
      data: {
        ...data,
        priority: priority
      }
    })
  };
}


