/**
 * Created by urbanmarovt on 09/02/16.
 */

const LOAD = 'kumuluzccm/receivers/preferences/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/receivers/preferences/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/receivers/preferences/LOAD_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(tenantId) {

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`/v1/users/${userId}/tenants/${tenantId}/preferences`, {token: token})
  };
}
