import { combineReducers } from 'redux';

import prefsNew from './new';
import prefsList from './list';
import prefsDetails from './details';

export default combineReducers({
  new: prefsNew,
  list: prefsList,
  details: prefsDetails
});
