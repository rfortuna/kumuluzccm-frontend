/**
 * Created by urbanmarovt on 12/02/16.
 */

const CREATE = 'around/receiver/details/CREATE';
const CREATE_SUCCESS = 'around/receiver/details/CREATE_SUCCESS';
const CREATE_FAIL = 'around/receiver/details/CREATE_FAIL';

const initialState = {
  creating: false,
  created: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function create(subscriptionId, data) {

  let receiveDays = '';
  receiveDays = data.sunday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.monday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.tuesday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.wednesday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.thursday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.friday ? `${receiveDays}1` : `${receiveDays}0`;
  receiveDays = data.saturday ? `${receiveDays}1` : `${receiveDays}0`;

  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, userId) => client.post(`/v1/users/${userId}/tenants/${subscriptionId}/preferences`, {
      token: token,
      data: {
        channelType: data.channelType,
        receiveFrom: data.from,
        receiveTo: data.to,
        receiveDays: receiveDays,
        channelDisabled: data.disabled,
        priority: 1
      }
    })
  };
}
