/**
 * Created by urbanmarovt on 31/03/16.
 */

import { combineReducers } from 'redux';

import baseInfo from './baseInfo';
import preferences from './preferences/reducer';


export default combineReducers({
  baseInfo: baseInfo,
  preferences: preferences
});
