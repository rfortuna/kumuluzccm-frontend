/**
 * Created by urbanmarovt on 3/31/16.
 */

const LOAD = 'kumuluzccm/subscriptions/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/subscriptions/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/subscriptions/LOAD_FAIL';


// TODO change init state
const initialState = {
  loaded: false,
  error: {},
  data: [],
//  data: [{
//    tenant: {
//      name: 'Abanka'
//    },
//    receiver: {
//      id: 123,
//      firstName: 'Urban',
//      lastName: 'Marovt',
//      email: 'urban.marovt@gmail.com',
//      mobilePhone: '+38631317932'
//    }
//  }, {
//    tenant: {
//      name: 'Mercator'
//    },
//    receiver: {
//      id: 124,
//      firstName: 'Urban',
//      lastName: 'Marovt',
//      email: 'urban.marovt@gmail.com',
//      mobilePhone: '+38631317932'
//    }
//  }],
//  count: 2
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(limit, offset) {

  const params = {
    limit: limit,
    offset: offset
  };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`/v1/users/${userId}/receivers`, { params: params, token: token})
  };
}
