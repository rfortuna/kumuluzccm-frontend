/**
 * Created by urbanmarovt on 31/03/16.
 */

import { combineReducers } from 'redux';

import tenantDetails from './details/reducer';
import tenantsList from './list';

export default combineReducers({
  details: tenantDetails,
  list: tenantsList
});
