/**
 * Created by urbanmarovt on 09/02/16.
 */
const LOAD = 'kumuluzccm/tags/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/tags/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/tags/LOAD_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  error: {},
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.tags && globalState.tags.loaded;
}

// put globalState as parameters
export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/tags`, {token: token})
  };
}
