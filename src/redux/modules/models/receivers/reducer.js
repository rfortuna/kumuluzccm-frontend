import { combineReducers } from 'redux';

import receivers from './list';
import details from './details';

export default combineReducers({
  list: receivers,
  details: details
});
