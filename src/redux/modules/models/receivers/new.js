/**
 * Created by urbanmarovt on 12/02/16.
 */

const CREATE = 'around/receiver/CREATE';
const CREATE_SUCCESS = 'around/receiver/CREATE_SUCCESS';
const CREATE_FAIL = 'around/receiver/CREATE_FAIL';

const initialState = {
  creating: false,
  created: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function create(data) {

  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/receivers`, {
      token: token,
      data: {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        mobilePhone: data.mobilePhone
      }
    })
  };
}
