/**
 * Created by urbanmarovt on 09/02/16.
 */

const LOAD = 'kumuluzccm/receiver/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/receiver/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/receiver/LOAD_FAIL';
const EDIT = 'kumuluzccm/receiver/EDIT';
const UPDATE = 'kumuluzccm/receiver/UPDATE';
const UPDATE_SUCCESS = 'kumuluzccm/receiver/UPDATE_SUCCESS';
const UPDATE_FAIL = 'kumuluzccm/receiver/UPDATE_FAIL';
const CANCEL_EDITING = 'kumuluzccm/receiver/CANCEL_EDITING';
const CLEAR_STATE = 'kumuluzccm/receiver/CLEAR_STATE';

const initialState = {
  loaded: false,
  editing: false,
  error: {},
  data: {},
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case EDIT:
      return {
        ...state,
        editing: true
      };
    case UPDATE:
      return {
        ...state,
        editing: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        editing: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        editing: false
      };
    case CANCEL_EDITING:
      return {
        ...state,
        editing: false
      };
    case CLEAR_STATE:
      return {
        loaded: false,
        editing: false,
        error: {},
        data: {}
      };
    default:
      return state;
  }
}

export function load(globalState) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receivers/${globalState.router.params.receiverId}`, {token: token})
  };
}

export function edit() {
  return {
    type: EDIT
  };
}

export function update(id, data) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, tenantId) => client.put(`/v1/tenants/${tenantId}/receivers/${id}`, {
      token: token,
      data: {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        mobilePhone: data.mobilePhone
      }
    })
  };
}

export function cancelEditing() {
  return {
    type: CANCEL_EDITING
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}
