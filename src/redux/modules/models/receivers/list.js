/**
 * Created by urbanmarovt on 09/02/16.
 */

const LOAD = 'kumuluzccm/receivers/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/receivers/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/receivers/LOAD_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(queryParams = {limit: 10, offset: 0}) {

  const {limit, offset, order, searchString} = queryParams;

  const params = {
    limit: limit,
    offset: offset,
    where: '',
    order: order
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} firstName:likeic:%${searchString}%`;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/receivers`, { params: params, token: token })
  };
}


