/**
 * Created by urbanmarovt on 09/02/16.
 */

import _ from 'underscore';
import lodash from 'lodash';

import {refactorWebsocketLiveData} from './../../../../utils/refactor';

const LOAD = 'kumuluzccm/campaign/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/campaign/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/campaign/LOAD_FAIL';
const EDIT = 'kumuluzccm/campaign/EDIT';
const UPDATE = 'kumuluzccm/campaign/UPDATE';
const UPDATE_SUCCESS = 'kumuluzccm/campaign/UPDATE_SUCCESS';
const UPDATE_FAIL = 'kumuluzccm/campaign/UPDATE_FAIL';
const CANCEL_EDITING = 'kumuluzccm/campaign/CANCEL_EDITING';
const CLEAR_STATE = 'kumuluzccm/campaign/CLEAR_STATE';
const ON_MESSAGE_SOCKET = 'kumuluzccm/campaign/ON_MESSAGE_SOCKET';

const initialState = {
  loaded: false,
  error: {},
  data: {},
  editing: false,
  prevData: {},
  liveStatistics: {
    rates: [['x'], ['EMAIL'], ['SMS'], ['PUSH_NOTIFICATION']],
    bars: [['PENDING'], ['DELIVERED'], ['BOUNCED'], ['CONFIRMED BY USER'], ['DISABLED BY USER'], ['PROVIDER ERROR'], ['ADDRESS UNKNOWN']]
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case EDIT:
      return {
        ...state,
        prevData: _.clone(state.data),
        editing: true
      };
    case UPDATE:
      return {
        ...state,
        editing: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        editing: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        data: state.prevData,
        editing: true,
        error: action.error
      };
    case CANCEL_EDITING:
      return {
        ...state,
        data: state.prevData,
        editing: false
      };
    case CLEAR_STATE:
      return initialState;
    case ON_MESSAGE_SOCKET:
      const {rdata, rate} = action.data;
      const {ratesNew, barsNew} = refactorWebsocketLiveData(
        lodash.cloneDeep(state.liveStatistics.rates),
        lodash.cloneDeep(state.liveStatistics.bars),
        rdata, rate);

      return {
        ...state,
        liveStatistics: {
          rates: ratesNew,
          bars: barsNew
        }
      };
    default:
      return state;
  }
}


export function load(globalState) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/campaigns/${globalState.router.params.campaignId}`, {token: token})
  };
}

export function loadById(campaignId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}`, {token: token})
  };
}

export function edit() {
  return {
    type: EDIT
  };
}


export function update(id, data) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, tenantId) => client.put(`/v1/tenants/${tenantId}/campaigns/${id}`, {
      data: {...data},
      token: token
    })
  };
}

export function cancelEditing() {
  return {
    type: CANCEL_EDITING
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

export function onMessageSocket(result) {
  return {
    type: ON_MESSAGE_SOCKET,
    data: result
  };
}
