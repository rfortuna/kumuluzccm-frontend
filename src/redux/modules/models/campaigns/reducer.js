import { combineReducers } from 'redux';

import campaigns from './list';
import campaignNew from './new';
import campaignDetails from './details';

export default combineReducers({
  new: campaignNew,
  list: campaigns,
  details: campaignDetails
});
