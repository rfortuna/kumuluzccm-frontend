/**
 * Created by urbanmarovt on 09/02/16.
 */

const LOAD = 'kumuluzccm/campaigns/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/campaigns/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/campaigns/LOAD_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      const newCount = action.result.headers['x-total-count'];
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        // typeof newCount == 'undefined' ? state.count : parseInt(newCount)
        count: parseInt(newCount),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.models.campaigns.list && globalState.models.campaigns.list.loaded;
}

// put globalState as parameters
export function load(queryParams = {limit: 10, offset: 0}) {
  const {limit, offset, searchString, searchTags} = queryParams;

  const params = {
    ...queryParams,
    limit: limit,
    offset: offset,
    where: ''
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} name:likeic:%${searchString}%`;
  }

  if (searchTags !== undefined && searchTags !== 'undefined' && searchTags !== '') {
    const searchTagsArray = searchTags.split(',');

    searchTagsArray.forEach((tag) => {
      params.where = `${params.where} tags.id:eq:${tag}`;
    });

  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/campaigns`, { params: params, token: token})
  };
}
