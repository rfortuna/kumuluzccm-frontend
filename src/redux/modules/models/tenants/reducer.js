/**
 * Created by rokfortuna on 3/31/16.
 */
import { combineReducers } from 'redux';

import tenantNew from './new';
import tenantDetails from './details';
import tenantsList from './list';
import imagesReducer from './images/reducer';

export default combineReducers({
  new: tenantNew,
  details: tenantDetails,
  list: tenantsList,
  images: imagesReducer
});
