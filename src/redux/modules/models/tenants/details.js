/**
 * Created by rokfortuna on 3/31/16.
 */

const LOAD = 'kumuluzccm/tenant/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/tenant/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/tenant/LOAD_FAIL';
const EDIT = 'kumuluzccm/tenant/EDIT';
const UPDATE = 'kumuluzccm/tenant/UPDATE';
const UPDATE_SUCCESS = 'kumuluzccm/tenant/UPDATE_SUCCESS';
const UPDATE_FAIL = 'kumuluzccm/tenant/UPDATE_FAIL';
const CANCEL_EDITING = 'kumuluzccm/tenant/CANCEL_EDITING';
const CLEAR_STATE = 'kumuluzccm/tenant/CLEAR_STATE';
const SHOW_LOGO_PREVIEW = 'kumuluzccm/tenant/SHOW_LOGO_PREVIEW';

const initialState = {
  loaded: false,
  editing: false,
  error: {},
  data: {},
  logoFile: null
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case EDIT:
      return {
        ...state,
        prevData: _.clone(state.data),
        editing: true
      };
    case UPDATE:
      return {
        ...state,
        editing: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        editing: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        data: state.prevData,
        editing: true,
        error: action.error
      };
    case CANCEL_EDITING:
      return {
        ...state,
        data: state.prevData,
        editing: false
      };
    case SHOW_LOGO_PREVIEW:
      return {
        ...state,
        logoFile: action.data.logoFile
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}`, {token: token})
  };
}

export function isLoaded(globalState) {
  return globalState.models.tenants.details.loaded;
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

export function edit() {
  return {
    type: EDIT
  };
}

export function update(data) {

  //extract logo file
  let file;
  if (data.logoImage) {
    file = data.logoImage[0];
    delete data.logoImage;
  }

  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, tenantId) => client.put(`/v1/tenants/${tenantId}`, {
      multiPart: {
        file: file,
        tenant: data
      },
      token: token
    })
  };
}

export function showLogoPreview(logoFile) {
  return {
    type: SHOW_LOGO_PREVIEW,
    data: {logoFile: logoFile }
  };
}

export function cancelEdit() {
  return {
    type: CANCEL_EDITING
  };
}
