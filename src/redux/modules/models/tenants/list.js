/**
 * Created by rokfortuna on 3/31/16.
 */

const LOAD = 'kumuluzccm/tentants/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/tenants/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/tenants/LOAD_FAIL';

const DELETE_TENANT = 'kumuluzccm/tentants/DELETE_TENANT';
const DELETE_TENANT_SUCCESS = 'kumuluzccm/tentants/DELETE_TENANT_SUCCESS';
const DELETE_TENANT_FAIL = 'kumuluzccm/tentants/DELETE_TENANT_FAIL';

const HANDLE_REQUEST_TENANT_SMS_USER = 'kumuluzccm/tentants/HANDLE_REQUEST_TENANT_SMS_USER';
const HANDLE_REQUEST_TENANT_SMS_USER_SUCCESS = 'kumuluzccm/tentants/HANDLE_REQUEST_TENANT_SMS_USER_SUCCESS';
const HANDLE_REQUEST_TENANT_SMS_USER_FAIL = 'kumuluzccm/tentants/HANDLE_REQUEST_TENANT_SMS_USER_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: [],
  count: 0,
  deletedTenant: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    case DELETE_TENANT:
    case DELETE_TENANT_SUCCESS:
    case DELETE_TENANT_FAIL:
      return {
        ...state
      };
    case HANDLE_REQUEST_TENANT_SMS_USER:
      return {
        ...state
      };
    case HANDLE_REQUEST_TENANT_SMS_USER_SUCCESS:
    case HANDLE_REQUEST_TENANT_SMS_USER_FAIL:
      return {
        ...state,
        count: state.count - 1
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(limit, offset) {
  const params = {
    limit: limit,
    offset: offset
  };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`/v1/users/${userId}/tenants`, { params: params, token: token})
  };
}

export function loadAdminTenants(queryParams, SMSUserStatus, searchString) {
  const params = {
    ...queryParams,
    where: `smsUserPendingStatus:EQ:${SMSUserStatus}`
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} name:likeic:%${searchString}%`;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, userId) => client.get(`/v1/admin/tenants`, { params: params, token: token})
  };
}

export function deleteTenant(id) {
  return {
    types: [DELETE_TENANT, DELETE_TENANT_SUCCESS, DELETE_TENANT_FAIL],
    promise: (client, token, userId) => client.del(`/v1/users/${userId}/tenants/${id}`, {token: token})
  };
}

export function handleRequestTenantSMSUser(tenantId, responseStatus) {
  const params = {
    status: responseStatus
  }
  return {
    types: [HANDLE_REQUEST_TENANT_SMS_USER, HANDLE_REQUEST_TENANT_SMS_USER_SUCCESS, HANDLE_REQUEST_TENANT_SMS_USER_FAIL],
    promise: (client, token, userId) => client.get(`/v1/admin/tenants/${tenantId}/resolve`, { params: params, token: token})
  };
}
