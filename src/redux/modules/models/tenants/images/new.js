/**
 * Created by urbanmarovt on 03/07/16.
 */
/**
 * Created by rokfortuna on 3/31/16.
 */


const CREATE = 'kumuluzccm/tenant/image/new/CREATE';
const CREATE_SUCCESS = 'kumuluzccm/tenant/image/new/CREATE_SUCCESS';
const CREATE_FAIL = 'kumuluzccm/tenant/image/new/CREATE_FAIL';
const SHOW_IMAGE_PREVIEW = 'kumuluzccm/tenant/image/new/SHOW_IMAGE_PREVIEW';
const CLEAR_STATE = 'kumuluzccm/tenant/image/new/CLEAR_STATE';

const initialState = {
  creating: false,
  created: false,
  error: {},
  data: null,
  imageFile: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        data: action.result.body,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    case SHOW_IMAGE_PREVIEW:
      return {
        ...state,
        imageFile: action.data.imageFile
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

// put globalState as parameters
export function upload(file) {

  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/images`, {
      multiPart: {
        file: file
      },
      token: token
    })
  };
}

export function showImagePreview( imageFile) {
  return {
    type: SHOW_IMAGE_PREVIEW,
    data: {imageFile: imageFile }
  };
}

export function clear() {
  return {
    type: CLEAR_STATE
  };
}

