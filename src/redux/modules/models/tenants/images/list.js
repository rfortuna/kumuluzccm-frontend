/**
 * Created by urbanmarovt on 03/07/16.
 */
/**
 * Created by urbanmarovt on 03/07/16.
 */
/**
 * Created by rokfortuna on 3/31/16.
 */


const LOAD = 'kumuluzccm/tenant/image/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/tenant/image/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/tenant/image/LOAD_FAIL';
const ADD_ITEM = 'kumuluzccm/tenant/image/ADD_ITEM';

const initialState = {
  loading: false,
  loaded: false,
  error: {},
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true,
        loaded: false
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        data: action.result.body,
        loading: false,
        loaded: true,
        error: {}
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case ADD_ITEM:
      return {
        ...state,
        data: [action.data.item, ...state.data]
      };
    default:
      return state;
  }
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/images`, {token: token})
  };
}

export function addItem(item) {
  return {
    type: ADD_ITEM,
    data: { item: item }
  }
}
