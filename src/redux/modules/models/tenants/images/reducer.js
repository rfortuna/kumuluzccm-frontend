/**
 * Created by urbanmarovt on 03/07/16.
 */
import { combineReducers } from 'redux';

import tenantImageNew from './new';
import tenantImageDetails from './details';
import tenantImagesList from './list';

export default combineReducers({
  new: tenantImageNew,
  details: tenantImageDetails,
  list: tenantImagesList
});
