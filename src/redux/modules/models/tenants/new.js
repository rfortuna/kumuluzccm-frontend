/**
 * Created by rokfortuna on 3/31/16.
 */


const CREATE = 'kumuluzccm/tenant/new/CREATE';
const CREATE_SUCCESS = 'kumuluzccm/tenant/new/CREATE_SUCCESS';
const CREATE_FAIL = 'kumuluzccm/tenant/new/CREATE_FAIL';
const SHOW_LOGO_PREVIEW = 'kumuluzccm/tenant/new/SHOW_LOGO_PREVIEW';
const CLEAR_STATE = 'kumuluzccm/tenant/new/CLEAR_STATE';

const initialState = {
  creating: false,
  created: false,
  error: {},
  logoFile: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    case SHOW_LOGO_PREVIEW:
      return {
        ...state,
        logoFile: action.data.logoFile
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

// put globalState as parameters
export function create(data) {

  //extract logo file

  let file;
  if (data.logoImage) {
    file = data.logoImage[0];
    delete data.logoImage;
  }

  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, userId) => client.post(`/v1/users/${userId}/tenants`, {
      multiPart: {
        file: file,
        tenant: data
      },
      token: token
    })
  };
}

export function showLogoPreview( logoFile) {
  return {
    type: SHOW_LOGO_PREVIEW,
    data: {logoFile: logoFile }
  };
}

export function clear() {
  return {
    type: CLEAR_STATE
  };
}
