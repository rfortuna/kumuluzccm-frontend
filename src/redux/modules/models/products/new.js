/**
 * Created by urbanmarovt on 5/12/16.
 */

const CREATE = 'kumuluzccm/product/new/CREATE';
const CREATE_SUCCESS = 'kumuluzccm/product/new/CREATE_SUCCESS';
const CREATE_FAIL = 'kumuluzccm/product/new/CREATE_FAIL';
const CLEAR_STATE = 'kumuluzccm/product/new/CLEAR_STATE';

const initialState = {
  creating: false,
  created: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function create(data) {
  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/products`, {
      // additional JSON to send
      data: {
        ...data
      },
      token: token
    })
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  }
}