/**
 * Created by urbanmarovt on 07/04/16.
 */

const LOAD = 'kumuluzccm/product/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/product/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/product/LOAD_FAIL';
const LOAD_PLATFORMS = 'kumuluzccm/product/platforms/LOAD';
const LOAD_PLATFORMS_SUCCESS = 'kumuluzccm/product/platforms/LOAD_SUCCESS';
const LOAD_PLATFORMS_FAIL = 'kumuluzccm/product/platforms/LOAD_FAIL';
const CLEAR_STATE = 'kumuluzccm/product/CLEAR_STATE';
const CLEAR_PLATFORMS = 'kumuluzccm/product/CLEAR_PLATFORMS';

const initialState = {
  loaded: false,
  error: {},
  data: {},
  dataPlatforms: []
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case LOAD_PLATFORMS:
      return {
        ...state,
        dataPlatforms: [],
        loading: true
      };
    case LOAD_PLATFORMS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        dataPlatforms: action.result.body,
        error: null
      };
    case LOAD_PLATFORMS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        dataPlatforms: [],
        error: action.error
      };
    case CLEAR_PLATFORMS:
      return {
        ...state,
        dataPlatforms: []
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function load(productId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/products/${productId}`, {token: token})
  };
}

export function loadPlatforms(productId) {
  return {
    types: [LOAD_PLATFORMS, LOAD_PLATFORMS_SUCCESS, LOAD_PLATFORMS_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/products/${productId}/platforms`, {token: token})
  };
}

export function clearPlatforms() {
  return {
    type: CLEAR_PLATFORMS
  }
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}
