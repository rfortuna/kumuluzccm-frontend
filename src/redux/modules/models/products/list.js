/**
 * Created by urbanmarovt on 07/04/16.
 */

const LOAD = 'kumuluzccm/products/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/products/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/products/LOAD_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(limit = 100, offset = 0) {
  const params = {
    limit: limit,
    offset: offset
  };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/products`, { params: params, token: token})
  };
}

