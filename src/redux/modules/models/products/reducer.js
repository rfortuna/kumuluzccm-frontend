/**
 * Created by urbanmarovt on 07/04/16.
 */

import { combineReducers } from 'redux';

import productList from './list';
import productDetails from './details';

export default combineReducers({
  list: productList,
  details: productDetails
});
