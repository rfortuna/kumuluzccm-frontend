/**
 * Created by urbanmarovt on 15/02/16.
 */

const CREATE = 'around/message/CREATE';
const CREATE_SUCCESS = 'around/message/CREATE_SUCCESS';
const CREATE_FAIL = 'around/message/CREATE_FAIL';

const initialState = {
  creating: false,
  created: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
//export function create(data, channelId, packageId) {
//  const receivers = data.receivers.data.receivers.split(',').map((receiverId) => {
//    return {receiver: { id: receiverId }};
//  });
//
//  const channels = [];
//  if (channelId === '1') {
//    channels.push('EMAIL');
//  } else if (channelId === '2') {
//    channels.push('SMS');
//  } else if (channelId === '3') {
//    channels.push('PUSH_NOTIFICATION');
//  }
//
//  return {
//    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
//    promise: (client) => client.post('/v1/messages/package', {
//      data: {
//        messagePackage: {
//          id: packageId
//        },
//        channels: channels,
//        subject: data.setup.data.subject,
//        contentShort: data.content.data.contentShort,
//        contentLong: data.content.data.contentLong,
//        deliverAfter: data.setup.data.deliverAfter,
//        ignoreUserSchedule: data.setup.data.ignoreReceiverPrefs,
//        messages: receivers
//      }
//    })
//  };
//}

//data.receivers = data.receivers.split(',').map((receiverId) => {
//  return {receiver: { id: receiverId }};
//});
//
//if (!empty(data.channels)) {
//  data.channels = data.channels.split(',');
//} else {
//  data.channels = [];
//}
//
//return {
//  types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
//  promise: (client) => client.post('/v1/messages/package', {
//    data: {
//      messagePackage: {
//        id: packageId
//      },
//      channels: data.channels,
//      subject: data.subject,
//      contentShort: data.contentShort,
//      contentLong: data.contentLong,
//      deliverAfter: data.deliverAfter,
//      ignoreUserSchedule: data.ignoreUserPreferences,
//      messages: data.receivers
//    }
//  })
//};
