import { combineReducers } from 'redux';

import messageNew from './new';
import messagesCount from './count';
import messageDetails from './details';

export default combineReducers({
  new: messageNew,
  count: messagesCount,
  details: messageDetails
});
