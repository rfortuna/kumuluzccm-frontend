/**
 * Created by urbanmarovt on 12/05/16.
 */

const CREATE = 'kumuluzccm/template/CREATE';
const CREATE_SUCCESS = 'kumuluzccm/template/CREATE_SUCCESS';
const CREATE_FAIL = 'kumuluzccm/template/CREATE_FAIL';
const CLEAR_STATE = 'kumuluzccm/template/CLEAR_STATE';
const SHOW_CONTENT_PREVIEW = 'kumuluzccm/template/SHOW_CONTENT_PREVIEW';
const CLEAR_LONG_CONTENT_FILE = 'kumuluzccm/template/CLEAR_LONG_CONTENT_FILE';

const initialState = {
  creating: false,
  created: false,
  error: {},
  templateFile: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    case SHOW_CONTENT_PREVIEW:
      return {
        ...state,
        contentPreview: action.data.contentPreview,
        templateFile: action.data.templateFile
      };
    case CLEAR_LONG_CONTENT_FILE:
      return {
        ...state,
        templateFile: null,
        contentPreview: ''
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

// put globalState as parameters
export function create(data, contentEMAIL) {

  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/templates`, {
      data: {
        name: data.name,
        type: data.contentType,
        html: data.contentType === 'EMAIL' ? contentEMAIL : data.contentShort,
        campaign: {
          id: data.campaign
        }
      },
      token: token
    })
  };
}

export function showContentPreview(contentPreview, templateFile) {
  return {
    type: SHOW_CONTENT_PREVIEW,
    data: {contentPreview: contentPreview, templateFile: templateFile }
  };
}

export function clear() {
  return {
    type: CLEAR_STATE
  };
}

export function clearContentFile() {
  return {
    type: CLEAR_LONG_CONTENT_FILE,
    data: {}
  }
}
