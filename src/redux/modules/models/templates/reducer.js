/**
 * Created by urbanmarovt on 12/05/16.
 */
import { combineReducers } from 'redux';

import templates from './list';
import templateDetails from './details';
import templateNew from './new';

export default combineReducers({
  new: templateNew,
  list: templates,
  details: templateDetails
});
