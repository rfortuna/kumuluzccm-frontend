/**
 * Created by urbanmarovt on 12/05/16.
 */

/**
 * Created by rokfortuna on 3/31/16.
 */

const LOAD = 'kumuluzccm/templates/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/templates/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/templates/LOAD_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: [],
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}

// put globalState as parameters
export function load(limit = 10, offset = 0, campaignId = '') {
  const params = {
    limit: limit,
    offset: offset,
    where: `campaign.id:EQ:${campaignId}`
  };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/templates`, { params: params, token: token})
  };
}




