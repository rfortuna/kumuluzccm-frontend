/**
 * Created by urbanmarovt on 12/05/16.
 */

const LOAD = 'kumuluzccm/template/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/template/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/template/LOAD_FAIL';
const CLEAR_STATE = 'kumuluzccm/template/CLEAR_STATE';
const SET_ITEM = 'kumuluzccm/template/SET_ITEM';
const DELETE = 'kumuluzccm/template/DELETE';
const DELETE_SUCCESS = 'kumuluzccm/template/DELETE_SUCCESS';
const DELETE_FAIL = 'kumuluzccm/template/DELETE_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: null,
  deleting: false
};

export default function reducer(state = initialState, action = {}) {

  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case SET_ITEM:
      return {
        ...state,
        loaded: true,
        data: action.data,
        error: null
      };
    case DELETE:
      return {
        ...state,
        deleting: true,
        error: null
      };
    case DELETE_SUCCESS:
      return {
        ...state,
        deleting: false,
        data: null,
        error: null
      };
    case DELETE_FAIL:
      return {
        ...state,
        deleting: false,
        error: action.error
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}

export function setSelectedTemplate(template) {
  return {
    type: SET_ITEM,
    data: template
  }
}

export function load(templateId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/templates/${templateId}`, {token: token})
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

export function remove(templateId) {
  return {
    types: [DELETE, DELETE_SUCCESS, DELETE_FAIL],
    promise: (client, token, tenantId) => client.del(`/v1/tenants/${tenantId}/templates/${templateId}`, {token: token})
  }
}

