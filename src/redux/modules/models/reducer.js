import { combineReducers } from 'redux';

import tags from './tags';
import campaingsReducer from './campaigns/reducer';
import messagesReducer from './messages/reducer';
import receiversReducer from './receivers/reducer';
import newlettersReducer from './newsletters/reducer';
import listsReducer from './lists/reducer';
import subscriptionsReducer from './subscriptions/reducer';
import tenantsReducer from './tenants/reducer';
import productsReducer from './products/reducer';
import templatesReducer from './templates/reducer';
import constants from './constants';
import platformsReducer from './platforms/reducer';

export default combineReducers({
  campaigns: campaingsReducer,
  tags: tags,
  receivers: receiversReducer,
  messages: messagesReducer,
  newsletters: newlettersReducer,
  lists: listsReducer,
  subscriptions: subscriptionsReducer,
  tenants: tenantsReducer,
  products: productsReducer,
  constants: constants,
  templates: templatesReducer,
  platforms: platformsReducer
});
