/**
 * Created by rokfortuna on 4/8/16.
 */

const LOAD_TIMEZONES = 'kumuluzccm/constants/LOAD_TIMEZONES';
const LOAD_TIMEZONES_SUCCESS = 'kumuluzccm/constants/LOAD_TIMEZONES_SUCCESS';
const LOAD_TIMEZONES_FAIL = 'kumuluzccm/constants/LOAD_TIMEZONES_FAIL';

const initialState = {
  timezones: {
    loaded: false,
    data: []
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_TIMEZONES:
      return state;
    case LOAD_TIMEZONES_SUCCESS:
      return {
        ...state,
        timezones: {
          loaded: true,
          data: action.result.body
        }
      };
    case LOAD_TIMEZONES_FAIL:
      return {
        ...state,
        timezones: {
          loaded: false,
          data: []
        }
      };
    default:
      return state;
  }
}

export function areTimezonesLoaded(globalState) {
  return globalState.models.constants.timezones.loaded;
}

// put globalState as parameters
export function loadTimezones(queryParams) {
  return {
    types: [LOAD_TIMEZONES, LOAD_TIMEZONES_SUCCESS, LOAD_TIMEZONES_FAIL],
    promise: (client, token) => client.get('/v1/timezones', {params: queryParams, token: token})
  };
}
