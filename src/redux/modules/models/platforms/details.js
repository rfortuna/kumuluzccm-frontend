/**
 * Created by rokfortuna on 5/13/16.
 */


const LOAD = 'kumuluzccm/product/platform/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/product/platform/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/product/platform/LOAD_FAIL';

const EDIT = 'kumuluzccm/product/platform/EDIT';
const CANCEL_EDITING = 'kumuluzccm/product/platform/CANCEL_EDITING';

const UPDATE = 'kumuluzccm/product/platform/UPDATE';
const UPDATE_SUCCESS = 'kumuluzccm/product/platform/UPDATE_SUCCESS';
const UPDATE_FAIL = 'kumuluzccm/product/platform/UPDATE_FAIL';

const CLEAR_STATE = 'kumuluzccm/product/platform/CLEAR_STATE';

const initialState = {
  loaded: false,
  error: {},
  data: {},
  editing: false,
  prevData: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: {},
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: {},
        error: action.error
      };
    case EDIT:
      return {
        ...state,
        prevData: _.clone(state.data),
        editing: true
      };
    case UPDATE:
      return {
        ...state,
        editing: true
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        editing: false
      };
    case UPDATE_FAIL:
      return {
        ...state,
        data: state.prevData,
        editing: true,
        error: action.error
      };
    case CANCEL_EDITING:
      return {
        ...state,
        data: state.prevData,
        editing: false
      };
    case CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
}


export function load(globalState) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) =>
      client.get(`/v1/tenants/${tenantId}/products/${globalState.router.params.productId}/platforms/${globalState.router.params.platformId}`, {token: token})
  };
}


export function edit() {
  return {
    type: EDIT
  };
}


export function update(productId, platformId, data) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAIL],
    promise: (client, token, tenantId) =>
      client.put(`/v1/tenants/${tenantId}/products/${productId}/platforms/${platformId}`, {
      data: {...data},
      token: token
    })
  };
}

export function cancelEditing() {
  return {
    type: CANCEL_EDITING
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

