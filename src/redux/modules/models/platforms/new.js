/**
 * Created by rokfortuna on 5/12/16.
 */

const CREATE_IOS = 'kumuluzccm/product/platform/new/ios/CREATE_IOS';
const CREATE_IOS_SUCCESS = 'kumuluzccm/product/platform/new/ios/CREATE_IOS_SUCCESS';
const CREATE_IOS_FAIL = 'kumuluzccm/product/platform/new/ios/CREATE_IOS_FAIL';

const CREATE_ANDROID = 'kumuluzccm/product/platform/new/android/CREATE_ANDROID';
const CREATE_ANDROID_SUCCESS = 'kumuluzccm/product/platform/new/android/CREATE_ANDROID_SUCCESS';
const CREATE_ANDROID_FAIL = 'kumuluzccm/product/platform/new/android/CREATE_ANDROID_FAIL';

const CLEAR_STATE = 'kumuluzccm/product/platform/new/CLEAR_STATE';

const SET_CERT_FILE_DATA = 'kumuluzccm/product/platform/new/SET_CERT_FILE_DATA';


const initialState = {
  data: {
    certFileData: null
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE_ANDROID:
    case CREATE_ANDROID_SUCCESS:
    case CREATE_ANDROID_FAIL:
    case CREATE_IOS:
    case CREATE_IOS_SUCCESS:
    case CREATE_IOS_FAIL:
      return state;
    case CLEAR_STATE:
      return initialState;
    case SET_CERT_FILE_DATA:
      return {...state,
        data: {
          ...state.data,
          certFileData: action.data
        }
      };
    default:
      return state;
  }
}

export function createAndroid(productId, apiKey) {
  return {
    types: [CREATE_ANDROID, CREATE_ANDROID_SUCCESS, CREATE_ANDROID_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/products/${productId}/platforms/android`, {
      // additional JSON to send
      data: {
        type: 'ANDROID',
        apiKey: apiKey
      },
      token: token
    })
  };
}

export function createIOS(productId, file, password) {
  return {
    types: [CREATE_IOS, CREATE_IOS_SUCCESS, CREATE_IOS_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/products/${productId}/platforms/ios`, {
      // additional JSON to send
      multiPart: {
        file: file,
        platform: {
          type: 'IOS',
          password: password
        }
      },
      token: token
    })
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  }
}

export function setCertFileData(data) {
  return {
    type: SET_CERT_FILE_DATA,
    data: data
  }
}