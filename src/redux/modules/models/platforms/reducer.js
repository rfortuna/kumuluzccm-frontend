/**
 * Created by rokfortuna on 5/13/16.
 */


import { combineReducers } from 'redux';

import platformNew from './new';
import platformList from './list';
import platformDetails from './details';

export default combineReducers({
  new: platformNew,
  list: platformList,
  details: platformDetails
});
