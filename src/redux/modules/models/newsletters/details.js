/**
 * Created by rokfortuna on 03/03/16.
 */

const LOAD = 'kumuluzccm/newsletter/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/newsletter/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/newsletter/LOAD_FAIL';
const LOAD_MESSAGES = 'kumuluzccm/newsletter/LOAD_MESSAGES';
const LOAD_MESSAGES_SUCCESS = 'kumuluzccm/newsletter/LOAD_MESSAGES_SUCCESS';
const LOAD_MESSAGES_FAIL = 'kumuluzccm/newsletter/LOAD_MESSAGES_FAIL';

const SEND_NEWSLETTERS = 'kumuluzccm/newsletter/SEND_NEWSLETTERS';
const SEND_NEWSLETTERS_SUCCESS = 'kumuluzccm/newsletter/SEND_NEWSLETTERS_SUCCESS';
const SEND_NEWSLETTERS_FAIL = 'kumuluzccm/newsletter/SEND_NEWSLETTERS_FAIL';

const CANCEL_NEWSLETTERS = 'kumuluzccm/newsletter/CANCEL_NEWSLETTERS';
const CANCEL_NEWSLETTERS_SUCCESS = 'kumuluzccm/newsletter/CANCEL_NEWSLETTERS_SUCCESS';
const CANCEL_NEWSLETTERS_FAIL = 'kumuluzccm/newsletter/CANCEL_NEWSLETTERS_FAIL';

const CLEAR_STATE = 'kumuluzccm/newsletter/CLEAR_STATE';

const initialState = {
  loaded: false,
  sending: false,
  canceling: false,
  error: {},
  newsletter: {
    user: {},
    campaign: {},
    totalCount: 0,
    openRate: {},
    clickRate: {}
  },
  messages: {
    data: [],
    totalCount: 0
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        newsletter: initialState.newsletter,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        newsletter: {
          ...action.result[0].body,
          totalCount: action.result[1].body,
          openRate: action.result[2].body,
          clickRate: action.result[3].body
        },
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        newsletter: initialState.newsletter,
        error: action.error
      };
    case LOAD_MESSAGES:
      return {
        ...state
      };
    case LOAD_MESSAGES_SUCCESS:
      return {
        ...state,
        messages: {
          data: action.result.body,
          totalCount: action.result.headers['x-total-count']
        }
      };
    case LOAD_MESSAGES_FAIL:
      return {
        ...state,
        messages: {data: [], totalCount: 0},
        error: action.error
      };
    case SEND_NEWSLETTERS:
      return {
        ...state,
        sending: true
      };
    case SEND_NEWSLETTERS_SUCCESS:
      return {
        ...state,
        sending: false
      };
    case SEND_NEWSLETTERS_FAIL:
      return {
        ...state,
        sending: false,
        error: action.error
      };
    case CANCEL_NEWSLETTERS:
      return {
        ...state,
        canceling: true
      };
    case CANCEL_NEWSLETTERS_SUCCESS:
      return {
        ...state,
        canceling: false
      };
    case CANCEL_NEWSLETTERS_FAIL:
      return {
        ...state,
        canceling: false,
        error: action.error
      };
    case CLEAR_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
}


export function load(campaignId, newsletterId) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => Promise.all(
      [client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/${newsletterId}`, {token: token}),
        client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/${newsletterId}/messageCount`, {token: token}),
        client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/${newsletterId}/openRate`, {token: token}),
        client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/${newsletterId}/clickRate`, {token: token}) ])
  };
}

export function loadMessages(newsletterId, queryParams) {

  const {limit, offset, searchString, order} = queryParams;

  const params = {
    limit: limit,
    offset: offset,
    where: `newsletters.id:EQ:${newsletterId}`,
    order: order
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    params.where = `${params.where} receiver.email:likeic:%${searchString}%`;
  }

  return {
    types: [LOAD_MESSAGES, LOAD_MESSAGES_SUCCESS, LOAD_MESSAGES_FAIL],
    promise: (client, token, tenantId) =>
        client.get(`/v1/tenants/${tenantId}/messages/`,
          {
            token: token,
            params: params
          })
  };
}

export function clearState() {
  return {
    type: CLEAR_STATE
  };
}

export function send(campaignId, newsletterId) {

  return {
    types: [SEND_NEWSLETTERS, SEND_NEWSLETTERS_SUCCESS, SEND_NEWSLETTERS_FAIL],
    promise: (client, token, tenantId) =>
      client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/${newsletterId}/send`,
        {
          token: token
        })
  }

}

export function cancel(campaignId, newsletterId) {

  return {
    types: [CANCEL_NEWSLETTERS, CANCEL_NEWSLETTERS_SUCCESS, CANCEL_NEWSLETTERS_FAIL],
    promise: (client, token, tenantId) =>
      client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/${newsletterId}/cancel`,
        {
          token: token
        })
  }

}

