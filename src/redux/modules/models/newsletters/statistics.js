/**
 * Created by rokfortuna on 4/22/16.
 */

const LOAD_STATISTICS = 'kumuluzccm/newsletters/LOAD_STATISTICS';
const LOAD_STATISTICS_SUCCESS = 'kumuluzccm/newsletters/LOAD_STATISTICS_SUCCESS';
const LOAD_STATISTICS_FAIL = 'kumuluzccm/newsletters/LOAD_STATISTICS_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: {
    newsletters: [],
    openRate: [],
    clickRate: [],
    openRateAvg: 0,
    clickRateAvg: 0
  },
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_STATISTICS:
      return {
        ...state,
        data: initialState.data,
        loading: true
      };
    case LOAD_STATISTICS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          newsletters: action.result[0].body,
          openRate: action.result[1].body,
          clickRate: action.result[2].body,
          openRateAvg: action.result[3].body,
          clickRateAvg: action.result[4].body
        },
        count: parseInt(action.result[0].headers['x-total-count']),
        error: null
      };
    case LOAD_STATISTICS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: initialState.data,
        count: 0,
        error: action.error
      };


    default:
      return state;
  }
}

export function loadStatistics(channel= '', limit = 10, offset = 0) {

  const queryParams = {
    limit: limit,
    offset: offset,
    channel: channel,
    where: `channels.type:EQ:${channel}`,
    order: 'createdAt DESC'

  };

  return {
    types: [LOAD_STATISTICS, LOAD_STATISTICS_SUCCESS, LOAD_STATISTICS_FAIL],
    promise: (client, token, tenantId) => Promise.all(
      [
        client.get(`/v1/tenants/${tenantId}/newsletters`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/newsletters/openRate`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/newsletters/clickRate`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/newsletters/openRateAverage`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/newsletters/clickRateAverage`, { params: queryParams, token: token})
      ])
  };
}

export function loadStatisticsByCampaignId(campaignId, channel= '', params = {limit: 10, offset: 0}) {



  const queryParams = {
    limit: params.limit,
    offset: params.offset,
    channel: channel,
    where: `channels.type:EQ:${channel}`,
    order: 'createdAt DESC'
  };

  return {
    types: [LOAD_STATISTICS, LOAD_STATISTICS_SUCCESS, LOAD_STATISTICS_FAIL],
    promise: (client, token, tenantId) => Promise.all(
      [
        client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/openRate`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/campaigns/${campaignId}/newsletters/clickRate`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/newsletters/openRateAverage`, { params: queryParams, token: token}),
        client.get(`/v1/tenants/${tenantId}/newsletters/clickRateAverage`, { params: queryParams, token: token})
      ])
  };
}