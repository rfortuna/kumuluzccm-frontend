import { combineReducers } from 'redux';

import newsletterNew from './new';
import newslettersList from './list';
import newsletterDetails from './details';
import newsletterStatistics from './statistics';

export default combineReducers({
  new: newsletterNew,
  list: newslettersList,
  details: newsletterDetails,
  statistics: newsletterStatistics
});
