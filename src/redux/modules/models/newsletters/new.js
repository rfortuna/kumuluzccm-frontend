/**
 * Created by urbanmarovt on 15/02/16.
 */

const CREATE = 'around/newsletter/CREATE';
const CREATE_SUCCESS = 'around/newsletter/CREATE_SUCCESS';
const CREATE_FAIL = 'around/newsletter/CREATE_FAIL';

import {messageTypes} from 'utils/constants';

const initialState = {
  creating: false,
  created: false,
  error: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        creating: true,
        created: false
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        created: true,
        error: {}
      };
    case CREATE_FAIL:
      return {
        ...state,
        creating: false,
        created: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function create(data, channelId) {
  const date = new Date();
  // if lists is selected reformat them
  let lists = null;
  if (data.lists.data.receiverType === 'lists') {
    lists = data.lists.data.receiverLists.split(',').map((listId) => {
      return {id: listId};
    })
  }

  const messageType = messageTypes.find((type) => {
    return type.id === channelId
  });

  const channels = [];
  if (messageType.email) {
    channels.push({type: 'EMAIL'});
  }

  if (messageType.sms) {
    channels.push({type: 'SMS'});
  }

  if (messageType.push) {
    data.channelSetup.productPlatforms.forEach((item) => {
      // TODO import these info

      if (data.channelSetup.data[item.type]) {
        channels.push({
          type: 'PUSH_NOTIFICATION',
          productPlatform: {
            id: item.id
          },
          anonymous: data.lists.data.receiverType === 'application'
        });
      }
    });
  }


  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_FAIL],
    promise: (client, token, tenantId) => client.post(`/v1/tenants/${tenantId}/campaigns/${data.setup.data.campaign}/newsletters`, {
      token: token,
      data: {
        preferred: channelId === '4',
        channels: channels,
        mailFrom: data.channelSetup.data.mailFrom,
        mailUser: data.channelSetup.data.mailUser,
        smsUser: data.channelSetup.data.smsUser,
        customProperties: data.channelSetup.data.pushCustomPropertiesString,
        subject: data.setup.data.subject,
        contentShort: data.content.data.contentShort,
        contentLong: data.content.data.contentEMAIL,
        deliverAfter:  data.setup.data.timezone ? Number(data.setup.data.deliverAfter) + (-1) * date.getTimezoneOffset() * 60 * 1000 : data.setup.data.deliverAfter,
        ignoreUserPreferences: data.setup.data.ignoreReceiverPrefs,
        timezone: data.setup.data.timezone ? { id: data.setup.data.timezone } : null,
        receiverLists: lists,
        allReceivers: data.lists.data.receiverType === 'all'
      }
    })
  };
}
