/**
 * Created by urbanmarovt on 09/02/16.
 */


const LOAD = 'kumuluzccm/newsletters/LOAD';
const LOAD_SUCCESS = 'kumuluzccm/newsletters/LOAD_SUCCESS';
const LOAD_FAIL = 'kumuluzccm/newsletters/LOAD_FAIL';

const initialState = {
  loaded: false,
  error: {},
  data: [],
  count: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        data: initialState.data,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.body,
        count: parseInt(action.result.headers['x-total-count']),
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: initialState.data,
        count: 0,
        error: action.error
      };
    default:
      return state;
  }
}


export function load(params) {
  const {limit, offset, searchString, order, status} = params;
  const queryParams = {
    limit: limit,
    offset: offset,
    where: '',
    order: order
  };

  if (searchString !== undefined && searchString !== 'undefined' && searchString !== '') {
    queryParams.where = `${queryParams.where} subject:likeic:%${searchString}%`;
  }

  if (status !== undefined && status !== 'undefined' && status !== '' && status !== 'ALL') {
    queryParams.where = `${queryParams.where} status:EQ:${status}`;
  }

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client, token, tenantId) => client.get(`/v1/tenants/${tenantId}/newsletters`, { params: queryParams, token: token})
  };
}