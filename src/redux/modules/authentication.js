/**
 * Created by rokfortuna on 3/24/16.
 */
import cookie from 'react-cookie';
import config from 'config';

const LOGIN = 'kumuluzccm/authentication/LOGIN';
const LOGIN_SUCCESS = 'kumuluzccm/authentication/LOGIN_SUCCESS';
const LOGIN_FAIL = 'kumuluzccm/authentication/LOGIN_FAIL';
const LOGOUT = 'kumuluzccm/authentication/LOGOUT';
const FILL_USER_DATA_COOKIE = 'kumuluzccm/authentication/FILL_USER_DATA_COOKIE';

const REFRESH_TOKEN = 'kumuluzccm/authentication/REFRESH_TOKEN';
const REFRESH_TOKEN_SUCCESS = 'kumuluzccm/authentication/REFRESH_TOKEN_SUCCESS';
const REFRESH_TOKEN_FAIL = 'kumuluzccm/authentication/REFRESH_TOKEN_FAIL';

const REFRESH_TOKEN_BEFORE = 30;

const initialState = {
  error: {},
  loggingIn: false,
  token: null,
  refreshingToken: false
};

export default function reducer(state = initialState, action = {}) {
  const curTime = new Date().getTime() / 1000;

  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loggingIn: true,
      };
    case LOGIN_SUCCESS:
      cookie.save('auth_token', action.result.body.access_token, { path: '/' });
      cookie.save('refresh_token', action.result.body.refresh_token, { path: '/' });
      cookie.save('token_expires_at', action.result.body.expires_in + curTime - REFRESH_TOKEN_BEFORE, { path: '/' });

      return {
        ...state,
        loggingIn: false,
        token: action.result.body.access_token,
        refreshToken: action.result.body.refresh_token,
        tokenExpiresAt: action.result.body.expires_in + curTime - REFRESH_TOKEN_BEFORE
      };
    case LOGIN_FAIL:
      return {
        ...initialState,
        error: action.error,
      };
    case LOGOUT:
      return {
        ...initialState,
        token: null,
        refreshToken: null,
        tokenExpiresAt: 0

      };
    case FILL_USER_DATA_COOKIE:
      return {
        ...state,
        token: action.data.token,
        refreshToken: action.data.refreshToken,
        tokenExpiresAt: action.data.tokenExpiresAt
      };
    case REFRESH_TOKEN:
      return {
        ...state,
        refreshingToken: true
      };
    case REFRESH_TOKEN_SUCCESS:
      cookie.save('auth_token', action.result.body.access_token, { path: '/' });
      cookie.save('refresh_token', action.result.body.refresh_token, { path: '/' });
      cookie.save('token_expires_at', action.result.body.expires_in + curTime - REFRESH_TOKEN_BEFORE, { path: '/' });

      return {
        ...state,
        refreshingToken: false,
        token: action.result.body.access_token,
        refreshToken: action.result.body.refresh_token,
        tokenExpiresAt: action.result.body.expires_in + new Date().getTime() / 1000 - 30

      };
    case REFRESH_TOKEN_FAIL:

      cookie.remove('auth_token', {path: '/'});
      cookie.remove('refresh_token', {path: '/'});
      cookie.remove('token_expires_at', {path: '/'});
      cookie.remove('profile_type', {path: '/'});
      cookie.remove('profile_id', {path: '/'});

      return {
        ...initialState,
        error: action.error,
        token: null,
        refreshToken: null,
        tokenExpiresAt: 0
      };
    default:
      return state;
  }
}

export function login(username, password) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: (client, token) => client.post(`/auth/realms/kumuluzccm/protocol/openid-connect/token`, {
      formData: {
        username: username,
        password: password,
        client_id: config.keycloakClientId,
        grant_type: 'password'
      },
      token: token
    })
  };
}

export function refreshToken(refreshToken) {
  return {
    types: [REFRESH_TOKEN, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_FAIL],
    promise: (client) => client.post(`/auth/realms/kumuluzccm/protocol/openid-connect/token`, {
      formData: {
        refresh_token: refreshToken,
        client_id: config.keycloakClientId,
        grant_type: 'refresh_token'
      }
    })
  };
}

export function logout() {
  return {
    type: LOGOUT
  };
}

export function fillTokenCookie(token, refreshToken, tokenExpiresAt) {
  return {
    type: FILL_USER_DATA_COOKIE,
    data: {token: token, refreshToken: refreshToken, tokenExpiresAt: tokenExpiresAt}
  };
}
