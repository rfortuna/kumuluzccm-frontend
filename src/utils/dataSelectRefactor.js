export const refactorTags = (tags) => {
  return tags.map(
    (element) => {
      return {label: element.value, value: element.id, color: element.color };
    }
  );
};

export const refactorReceivers = (receivers) => {
  return receivers.map(
    (element) => {
      return {label: `${element.firstName} ${element.lastName}, ${element.email}`, value: element.id
      };
    }
  );
};

export const refactorCampaigns = (campaigns) => {
  return campaigns.map(
    (element) => {
      return {label: element.name, value: element.id };
    }
  );
};

export const refactorProducts = (products) => {
  return products.map(
    (element) => {
      return {label: element.name, value: element.id };
    }
  );
};


export const refactorProfileOptions = (data) => {

  const options = [{
    id: data.id,
    name: `${data.firstName} ${data.lastName}`,
    type: 'user',
    logo: null
  }];

  data.tenants.forEach((tenant) => {
    options.push({
      id: tenant.id,
      name: tenant.name,
      type: 'tenant',
      logo: tenant.logo
    })
  });

  return options;

};

export const refactorLists = (lists) => {
  return lists.map(
    (list) => {
      return {label: list.name, value: list.id };
    }
  );
};

export const refactorTimezones = (timezones) => {
  return timezones.map(
    (timezone) => {
      return {label: timezone.display, value: timezone.id };
    }
  );
};

export const refactorTemplates = (templates) => {
  return templates.map(
    (template) => {
      return {label: template.name, value: template.html, type: template.type, id: template.id}
    }
  )
};