/**
 * Created by urbanmarovt on 25/04/16.
 */

export const messageTypes = [
  {id: '4', label: 'PREFERRED CHANNEL', description: 'Send a message via user-preferred channels.', icons: ['fa-star'], icon: 'fa-star', email: true, sms: true, push: true},
  {id: '1', label: 'EMAIL', description: 'Send an email to your customers with your preferred address.', icons: ['fa-envelope'], icon: 'fa-envelope',email: true, sms: false, push: false},
  {id: '3', label: 'PUSH NOTIFICATION', description: 'Send a push notification to your customers through your KumuluzCCM integrated mobile app.', icons: ['fa-mobile-phone'], icon: 'fa-mobile-phone', email: false, sms: false, push: true},
  {id: '2', label: 'SMS', description: 'Send an SMS to your customers.', icons: ['fa-comments-o'], icon: 'fa-comments-o', email: false, sms: true, push: false},
  {id: '5', label: 'EMAIL + PUSH NOTIFICATION', description: 'Send a push notification and an email to your customers.', icons: ['fa-envelope', 'fa-mobile-phone'], icon: 'fa-envelope', email: true, sms: false, push: true},
  {id: '6', label: 'EMAIL + SMS', description: 'Send an email and a sms to your customers.', icons: ['fa-envelope', 'fa-comments-o'], icon: 'fa-envelope', email: true, sms: true, push: false},
  {id: '7', label: 'SMS + PUSH NOTIFICATION', description: 'Send a sms and a push notification to your customers.', icons: ['fa-comments-o', 'fa-mobile-phone'], icon: 'fa-envelope', email: false, sms: true, push: true},
  {id: '8', label: 'EMAIL + SMS + PUSH NOTIFICATION', description: 'Send a message to your customers through all available channels.', icons: ['fa-envelope', 'fa-comments-o', 'fa-mobile-phone'], icon: 'fa-envelope', email: true, sms: true, push: true}
];

export const templateTypes = [
  {id: '1', value: 'EMAIL', label: 'EMAIL'},
  {id: '2', value: 'SMS', label: 'SMS'},
  {id: '3', value: 'PUSH_NOTIFICATION', label: 'PUSH NOTIFICATION'}
];

