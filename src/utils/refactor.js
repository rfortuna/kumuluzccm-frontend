import _ from 'underscore';

const ChannelType = ['EMAIL', 'SMS', 'PUSH_NOTIFICATION'];
const DeliveryStatus = ['PENDING', 'DELIVERED', 'BOUNCED', 'CONFIRMED_BY_USER', 'DISABLED_BY_USER', 'PROVIDER_ERROR', 'ADDRESS_UNKNOWN'];


export const refactorTags = (tags) => {
  return tags.split(',')
    .filter((tagId)=> {return tagId !== '';})
    .map((tagId) => {return { id: tagId };});
};

export const refactorWebsocketLiveData = (ratesCpy, barsCpy, rdata, rate) => {

  // remove spaces
  for (let i = 0; i < ratesCpy.length; i++) {
    ratesCpy[i][0] = ratesCpy[i][0].replace(/ /g, '_');
  }
  for (let i = 0; i < barsCpy.length; i++) {
    barsCpy[i][0] = barsCpy[i][0].replace(/ /g, '_');
  }

  // bars
  for (let i = 0; i < ChannelType.length; i++) {
    for (let j = 0; j < DeliveryStatus.length; j++) {
      let value = rdata[ChannelType[i]][DeliveryStatus[j]];
      if (typeof value === 'undefined') {
        value = 0;
      }
      barsCpy[j][i + 1] = value;
    }
  }

  // rate
  if (typeof rate !== 'undefined') {
    for (let i = 0; i < ChannelType.length; i++) {
      const channel = ChannelType[i];
      let value = rate[channel];
      if (typeof value === 'undefined') {
        value = 0;
      }

      for (let j = 1; j < ratesCpy.length; j++) {
        if (ratesCpy[j][0] === channel) {
          ratesCpy[j].push(value);
          break;
        }
      }
    }
    ratesCpy[0].push(new Date());
  }

  for (let i = 0; i < ratesCpy.length; i++) {
    // live rates only for past 30 seconds
    if (ratesCpy[i].length > 31) {
      ratesCpy[i] = [ratesCpy[i][0], ...ratesCpy[i].slice(-30)];
    }
  }

  // add spaces back
  for (let i = 0; i < ratesCpy.length; i++) {
    ratesCpy[i][0] = ratesCpy[i][0].replace(/_/g, ' ');
  }
  for (let i = 0; i < barsCpy.length; i++) {
    barsCpy[i][0] = barsCpy[i][0].replace(/_/g, ' ');
  }

  return {ratesNew: ratesCpy, barsNew: barsCpy};
};

export const refactorSubscriptionPrefs = (subscriptionPrefs) => {
  const channels = ['SMS', 'EMAIL', 'PUSH_NOTIFICATION'];

  channels.forEach((channel) => {

    const channelPrefs = subscriptionPrefs.find((preference) => {return preference.channelType === channel;});

    if (channelPrefs === undefined) {
      subscriptionPrefs.push({channelType: channel, notDefined: true, priority: 0});
    }

  });

  return subscriptionPrefs;

};

export const refactorReceiveDays = (days) => {
  const refactoredDays = {};

  refactoredDays.sunday = days.charAt(0) === '1';
  refactoredDays.monday = days.charAt(1) === '1';
  refactoredDays.tuesday = days.charAt(2) === '1';
  refactoredDays.wednesday = days.charAt(3) === '1';
  refactoredDays.thursday = days.charAt(4) === '1';
  refactoredDays.friday = days.charAt(5) === '1';
  refactoredDays.saturday = days.charAt(6) === '1';

  return refactoredDays;
};

export const refactorCSVListData = (data, upperLimit) => {
  const headers = data[0];
  const result = [];
  const limit = data.length < upperLimit ? data.length : upperLimit;

  let i;
  for (i = 1; i < limit; i++) {
    let newObj = {};
    let j;
    for (j = 0; j < data[i].length; j++) {
      newObj[headers[j]] = data[i][j];
    }
    result.push(newObj);
  }
  return result;
};

export const refactorListData = (data) => {
  let result = [[]];
  // const ignoredKeys = ['notifications', 'id'];
  if (data && !_.isEmpty(data)) {
    Object.values(data)[0].forEach(
      (prop) => {
          result[0].push(prop.prop);
      }
    );
    let i;
    Object.keys(data).forEach((receiverId) => {
      let props = {};
      props.id = receiverId;
      data[receiverId].forEach(
        (prop) => {
          props[prop.prop] = prop.val;
        }
      )
      result.push(props);
    });
  }

  return result;
};


export const refactorTemplatesToShort = (data) => {
  return data.filter((item) => item.type !== 'EMAIL');
};

export const refactorTemplatesToLong = (data) => {
  return data.filter((item) => item.type === 'EMAIL');
};

export const refactorProfileOptions = (data) => {
  return data.map((option)=> { return {value: option.id, label: option.name}});
};

export const refactorTenantImagesForGallery = (data) => {
  return data.map((item) => { return {src: item.url, caption: item.url}});
}
