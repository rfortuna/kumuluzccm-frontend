import Express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import config from './config';
import favicon from 'serve-favicon';
import compression from 'compression';
import httpProxy from 'http-proxy';
import path from 'path';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import Html from './helpers/Html';
import PrettyError from 'pretty-error';
import http from 'http';
import Cookies from 'cookies';
import $ from 'jquery';
import atob from 'atob';

import {ReduxRouter} from 'redux-router';
import createHistory from 'history/lib/createMemoryHistory';
import {reduxReactRouter, match} from 'redux-router/server';
import {Provider} from 'react-redux';
import qs from 'query-string';
import getRoutes from './routes';
import getStatusFromRoutes from './helpers/getStatusFromRoutes';
import {fillTokenCookie} from './redux/modules/authentication';
import {setProfileValues} from './redux/modules/global/profile';
import {setConfig as setConfigClient} from './redux/modules/global/config';
import {setSelectedTenantId} from './redux/modules/global/tenants';
import {tokenDecode} from './utils/token';

const pretty = new PrettyError();
const app = new Express();
const server = new http.Server(app);

const targetUrl = config.apiHost + ':' + config.apiPort;
const keycloakUrl = config.keycloakHost + ':' + config.keycloakPort + '/auth';
const websocketUrl = config.socketHost + ':' + config.socketPort;

const proxy = httpProxy.createProxyServer({
  target: targetUrl,
  ws: true
});

const authProxy = httpProxy.createProxyServer({
  target: keycloakUrl
});


app.use(compression());
app.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

app.use(Express.static(path.join(__dirname, '..', 'static')));

// Proxy auth requests to keycloak server
app.use('/api/auth', (req, res) => {
  authProxy.web(req, res);
});

// Proxy to API server
app.use('/api', (req, res) => {
  proxy.web(req, res);
});


// added the error handling to avoid https://github.com/nodejitsu/node-http-proxy/issues/527
proxy.on('error', (error, req, res) => {
  let json;
  if (error.code !== 'ECONNRESET') {
    console.error('proxy error', error);
  }
  if (!res.headersSent) {
    res.writeHead(500, {'content-type': 'application/json'});
  }

  json = {error: 'proxy_error', reason: error.message};
  res.end(JSON.stringify(json));
});

app.use((req, res) => {
  if (__DEVELOPMENT__) {
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    webpackIsomorphicTools.refresh();
  }
  const client = new ApiClient(req);

  const store = createStore(reduxReactRouter, getRoutes, createHistory, client);

  function hydrateOnClient() {
    res.send('<!doctype html>\n' +
      ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()} store={store}/>));
  }

  if (__DISABLE_SSR__) {
    hydrateOnClient();
    return;
  }

  const cookies = new Cookies(req, res);
  const cookieToken = cookies.get('auth_token');
  const cookieRefreshToken = cookies.get('refresh_token');

  const now = new Date();

  if (cookieToken && cookieRefreshToken && tokenDecode(cookieToken).exp * 1000 < now.getTime() && tokenDecode(cookieRefreshToken).exp * 1000 < now.getTime()) {
    cookies.set('auth_token', null);
    cookies.set('refresh_token', null);
    cookies.set('token_expires_at', null);
    cookies.set('profile_type', null);
    cookies.set('profile_id', null);
  } else {

    const cookieTokenExpiresAt = cookies.get('token_expires_at');
    const cookieProfileType = cookies.get('profile_type');
    const cookieProfileId = cookies.get('profile_id');

    if (cookieToken) {
      store.dispatch(fillTokenCookie(cookieToken, cookieRefreshToken, cookieTokenExpiresAt));
    }

    if(cookieProfileType && cookieProfileId) {
      store.dispatch(setProfileValues(cookieProfileType, cookieProfileId));
    }
  }

  //set NODE env variables used by the client
  store.dispatch(setConfigClient({websocketUrl: websocketUrl}));

  store.dispatch(match(req.originalUrl, (error, redirectLocation, routerState) => {
    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      console.error('ROUTER ERROR:', pretty.render(error));
      res.status(500);
      hydrateOnClient();
    } else if (!routerState) {
      res.status(500);
      hydrateOnClient();
    } else {
      // Workaround redux-router query string issue:
      // https://github.com/rackt/redux-router/issues/106
      if (routerState.location.search && !routerState.location.query) {
        routerState.location.query = qs.parse(routerState.location.search);
      }

      store.getState().router.then(() => {
        const component = (
          <Provider store={store} key="provider">
            <ReduxRouter/>
          </Provider>
        );

        const status = getStatusFromRoutes(routerState.routes);
        if (status) {
          res.status(status);
        }
        res.send('<!doctype html>\n' +
          ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()} component={component} store={store}/>));
      }).catch((err) => {
        console.error('DATA FETCHING ERROR:', pretty.render(err));
        res.status(500);
        hydrateOnClient();
      });
    }
  }));
});

if (config.port) {

  server.listen(config.port, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> ✅  %s is running, talking to API server on %s.', config.app.title, config.apiPort);
      console.info('==> 💻  Open http://%s:%s in a browser to view the app.', config.host, config.port);
  });
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
