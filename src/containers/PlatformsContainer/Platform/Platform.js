/**
 * Created by rokfortuna on 5/13/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import { FormControls, Button, Glyphicon, ButtonToolbar, Label, Input} from 'react-bootstrap';

import {clearState as clearPlatformState, load as loadPlatform} from 'redux/modules/models/platforms/details';
import styles from './Platform.scss';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadPlatform(getState())));
  return Promise.all(promises);
}


@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    platform: state.models.platforms.details
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      clearPlatformState
    }, dispatch)
)
export default class Platform extends Component {

  static propTypes = {
    platform: PropTypes.object,
    clearPlatformState: PropTypes.func
  };

  componentWillUnmount() {
    const {clearPlatformState} = this.props;
    clearPlatformState();
  }

  handleSubmit = (data) => {
//    data.tags = refactorTagsAPI(data.tags);
//    this.props.updateCampaign(this.props.campaign.data.id, data)
//      .then(res => {if (! _.has(res, 'error') ) window.location.reload(); });
  };

  render() {
    const {platform} = this.props;

    return (
      <div>
        <div className="panel-body">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Platform info</h5>
                </div>
                <div className="ibox-content">
                  <form className="form-horizontal">
                    <FormControls.Static label="Product" labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                                         value={platform.data.product && platform.data.product.name} />
                    <FormControls.Static label="Platform type" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={platform.data.type} />
                    <FormControls.Static label="Application ID" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      <input className={styles.inputKey} type="text" value={platform.data.id} static ></input>&nbsp;
                    </FormControls.Static>
                    <FormControls.Static label="API key" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      <input className={styles.inputKey} type="text" value={platform.data.authToken} static></input>&nbsp;
                      <small>(Use this token in Authorization HTTP header when calling CCM REST API.)</small>
                    </FormControls.Static>
                    {platform.data.type === 'IOS' &&
                    <FormControls.Static label="Production" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      {!platform.data.production && <i className="fa fa-remove"/> || <i className="fa fa-check"/>}
                    </FormControls.Static>
                    }
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
