/**
 * Created by rokfortuna on 5/12/16.
 */


import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';
import {reset as resetForm} from 'redux-form';

import {
  createIOS as createPlatformIOS,
  createAndroid as createPlatformAndroid,
  clearState, setCertFileData} from 'redux/modules/models/platforms/new';
import {load as loadProducts} from 'redux/modules/models/products/list';
import {PlatformNewForm} from 'components';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadProducts(100, 0)));
  return Promise.all(promises);
}


@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    certFileData: state.models.platforms.new.data.certFileData,
    products: state.models.products.list.data
  }),
  dispatch => bindActionCreators({
    ...routerActions, clearState, resetForm, setCertFileData,
    createPlatformIOS, createPlatformAndroid}, dispatch)
)
export default class PlatformNew extends Component {

  static propTypes = {
    pushState: PropTypes.func,
    createProduct: PropTypes.func,
    clearState: PropTypes.func,
    setCertFileData: PropTypes.func,
    certFileData: PropTypes.object,
    products: PropTypes.array,
    createPlatformAndroid: PropTypes.func,
    createPlatformIOS: PropTypes.func
  };

  componentWillUnmount() {
    this.props.resetForm('ProductNewForm');
    this.props.clearState();
  }

  handleSubmit = (data) => {
    const {createPlatformIOS, createPlatformAndroid, pushState} = this.props;
    let promise;
    if (data.platform === 'ANDROID') {
      promise = createPlatformAndroid(data.product, data.apiKey);
    } else {
      promise = createPlatformIOS(data.product, data.certFile[0], data.certPass);
    }

    promise.then((res) => {
      // success
      if (res.error) {
        // handle error
        toastr.error('There was a problem creating a new platform.');
      } else {
        pushState(null, `/app/tenant/products/${data.product}/platforms/${res.result.body.id}`);
      }
    });
  }

  handleCancel = () => {
    this.props.pushState(null, '/app/tenant');
  }

  render() {
    const {setCertFileData, certFileData, products} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New product</h2>
            <small>Register a new app in order to send push notifications using KumuluzCCM.</small>
          </div>
          <div className="col-lg-2">
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">

                <div className="ibox-content p-md">
                  <PlatformNewForm
                    setCertFileData={setCertFileData}
                    certFileData={certFileData}
                    onSubmit={this.handleSubmit}
                    handleCancel={this.handleCancel}
                    products={products}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

