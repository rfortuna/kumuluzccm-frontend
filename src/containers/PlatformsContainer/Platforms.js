/**
 * Created by urbanmarovt on 13/05/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';

import {PlatformsGrid} from 'components';
import { load as loadPlatforms} from 'redux/modules/models/platforms/list';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadPlatforms(getState().router.params.productId)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    platforms: state.models.platforms.list.data,
    productId: state.router.params.productId
  }),
  dispatch => bindActionCreators(
    {...routerActions}, dispatch)
)
export default class Campaigns extends Component {

  static propTypes = {
    platforms: PropTypes.array,
    productId: PropTypes.string
  };

  onPlatformClick = (id) => {
    const {pushState, productId} = this.props;
    pushState(null, `/app/tenant/products/${productId}/platforms/${id}`, '');
  };

  render() {
    const { platforms } = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Platforms</h2>
          </div>
        </div>

        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <PlatformsGrid
                    platforms={platforms}
                    onPlatformClick={this.onPlatformClick}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
