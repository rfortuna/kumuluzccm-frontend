/**
 * Created by urbanmarovt on 09/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {refactorTags} from 'utils/dataSelectRefactor';

import {CampaignForm} from 'components';

import {isLoaded as isTagsLoaded, load as loadTags} from 'redux/modules/models/tags';
import * as campaignNewActions from 'redux/modules/models/campaigns/new';


function fetchDataDeferred(getState, dispatch) {
  if (!isTagsLoaded(getState())) {
    return dispatch(loadTags());
  }
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    allTags: state.models.tags.data
  }),
  dispatch => bindActionCreators({...routerActions, ...campaignNewActions}, dispatch)
)
export default class CampaignNew extends Component {

  static propTypes = {
    tags: PropTypes.array,
    create: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    allTags: PropTypes.array
  };

  handleSubmit = (data) => {
    const {create} = this.props;
    create(data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/app/tenant/campaigns/${res.result.body.id}`);
      }
    });
  };

  handleCancel = () => {
    this.props.pushState(null, '/app/tenant/campaigns');
  };

  render() {
    const {allTags} = this.props;

    return (
    <div>
      <div className="row wrapper border-bottom white-bg page-heading">
        <div className="col-lg-10">
          <h2>New campaign</h2>
        </div>
        <div className="col-lg-2">

        </div>
      </div>
      <div className="wrapper wrapper-content animated fadeInRight">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
                <CampaignForm allTags={refactorTags(allTags)} onSubmit={this.handleSubmit} handleCancel={this.handleCancel}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }

}
