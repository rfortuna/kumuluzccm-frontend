/**
 * Created by urbanmarovt on 12/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import _ from 'underscore';

import {onMessageSocket} from 'redux/modules/models/campaigns/details';
import {LineChart, BarChart} from 'components';
import config from 'config';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  return Promise.all(promises);
}

let webSocket;

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    campaignId: state.router.params.campaignId,
    rates: state.models.campaigns.details.liveStatistics.rates,
    bars: state.models.campaigns.details.liveStatistics.bars,
    websocketUrl: state.global.config.websocketUrl
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      onMessageSocket}, dispatch)
)
export default class Campaign extends Component {

  static propTypes = {
    history: PropTypes.object,
    rates: PropTypes.array,
    bars: PropTypes.array,
    onMessageSocket: PropTypes.func,
    campaignId: PropTypes.string,
    websocketUrl: PropTypes.string
  };

  componentDidMount() {
    // live graphs
    const {onMessageSocket, campaignId, websocketUrl} = this.props;
    webSocket = new WebSocket(`${websocketUrl}/v1/campaigns/${campaignId}`);

    webSocket.onmessage = function(event) {
      const rec = JSON.parse(event.data);
      const rdata = rec.data;
      const rate = rec.rate;
      onMessageSocket({rdata: rdata, rate: rate});
    };
  }

  componentWillUnmount() {
    webSocket.close();
  }

  render() {
    const { rates, bars } = this.props;

    const rateChartData = {
      data: {
        x: 'x',
        columns: rates
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H:%M:%S'
          },
          label: 'time'
        },
        y: {
          label: 'messages'
        }
      },
      bindto: '#rateChartCampaign'
    };

    const barChartData = {
      data: {
        columns: bars,
        type: 'bar'
      },
      bar: {
        width: {
          ratio: 0.5 // this makes bar width 50% of length between ticks
        }
      },
      axis: {
        x: {
          type: 'category',
          categories: ['EMAIL', 'SMS', 'PUSH NOTIFICATIONS'],
          label: 'channel'
        },
        y: {
          label: 'messages'
        }

      },
      bindto: '#barChartCampaign'
    };

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Live rate</h5>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    <LineChart data={rateChartData} id="rateChartCampaign" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Live distribution</h5>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    <BarChart data={barChartData} id="barChartCampaign" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
