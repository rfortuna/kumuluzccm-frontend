/**
 * Created by rokfortuna on 03/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import _ from 'underscore';

import {
  load as loadCampaign,
  edit as editCampaign,
  update as updateCampaign,
  cancelEditing as cancelEditingCampaign,
  clearState as clearCampaignDetails
} from 'redux/modules/models/campaigns/details';
import {isLoaded as isTagsLoaded, load as loadTags} from 'redux/modules/models/tags';
import {TagDisplayForm, NewslettersGrid} from 'components';
import {refactorTags} from 'utils/dataSelectRefactor';
import {DateLabel} from 'components';
import {CampaignForm} from 'components';
import {refactorTags as refactorTagsAPI} from 'utils/refactor';
import config from 'config';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadCampaign(getState())));
  if (!isTagsLoaded(getState())) {
    promises.push(dispatch(loadTags()));
  }
  return Promise.all(promises);
}


@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    campaign: state.models.campaigns.details,
    allTags: state.models.tags.data,
    campaignId: state.router.params.campaignId
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      editCampaign,
      updateCampaign,
      cancelEditingCampaign,
      loadCampaign,
      clearCampaignDetails
    }, dispatch)
)
export default class Campaign extends Component {

  static propTypes = {
    campaign: PropTypes.object,
    history: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    editCampaign: PropTypes.func,
    saveCampaign: PropTypes.func,
    cancelEditingCampaign: PropTypes.func,
    clearCampaignDetails: PropTypes.func,
    campaignId: PropTypes.string,
    updateCampaign: PropTypes.func,
    allTags: PropTypes.array
  };

  componentWillUnmount() {
    const {clearCampaignDetails} = this.props;
    clearCampaignDetails();
  }

  handleSubmit = (data) => {
    data.tags = refactorTagsAPI(data.tags);
    this.props.updateCampaign(this.props.campaign.data.id, data)
      .then(res => {if (! _.has(res, 'error') ) window.location.reload(); });
  };

  render() {
    const { campaign, editCampaign, cancelEditingCampaign, allTags, pushState, newsletters} = this.props;

    return (
      <div>
        <div className="panel-body">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Basic info</h5>
                </div>
                <div className="ibox-content p-md">
                  <div>
                    {!campaign.editing &&
                    <form className="form-horizontal">
                      <FormControls.Static label="Name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={campaign.data.name}/>
                      <FormControls.Static label="Start date" labelClassName="col-xs-2" wrapperClassName="col-xs-10"> <DateLabel date={campaign.data.startDate}/> </FormControls.Static>
                      <FormControls.Static label="End date" labelClassName="col-xs-2" wrapperClassName="col-xs-10"><DateLabel date={campaign.data.endDate}/></FormControls.Static>
                      <TagDisplayForm labelClassName="col-xs-2" wrapperClassName="col-xs-10" tags={campaign.data.tags} />
                      <div className="hr-line-dashed"></div>
                      <FormControls.Static label="Sending email" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={campaign.data.mailFrom}/>
                      <FormControls.Static label="Sending email name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={campaign.data.mailUser}/>
                      {
//                        <FormControls.Static label="Sending sms name" labelClassName="col-xs-2"
//                                             wrapperClassName="col-xs-10" value={campaign.data.smsUser}/>
                      }
                      <div className="hr-line-dashed"></div>
                      <div className="row">
                        <div className="col-xs-offset-2 col-xs-10">
                          <ButtonToolbar>
                            <Button onClick={editCampaign}><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                          </ButtonToolbar>
                        </div>
                      </div>
                    </form>
                    }
                    {campaign.editing &&
                      <CampaignForm allTags={refactorTags(allTags)} onSubmit={this.handleSubmit} handleCancel={cancelEditingCampaign}/>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  }
