/**
 * Created by urbanmarovt on 15/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import { FormControls, Glyphicon, ButtonToolbar, ButtonGroup, Button} from 'react-bootstrap';
import _ from 'underscore';

import {LineChart, NewslettersCrudGrid, ChartPlaceholder} from 'components';
import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import {getQueryParams} from 'utils/queryParams';

import {setSelectedChannel} from 'redux/modules/views/campaigns';
import {loadStatisticsByCampaignId} from 'redux/modules/models/newsletters/statistics';
import {
  onPageChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';

import styles from './Campaign.scss';

const defaultQueryParams = {
  limit: 10,
  offset: 0
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  const queryParams = getQueryParams(getState().router, defaultQueryParams);
  promises.push(dispatch(setInitialStateQueryParams(`/app/tenant/campaigns/${getState().router.params.campaignId}/newsletters`, queryParams)));
  promises.push(dispatch(loadStatisticsByCampaignId(getState().router.params.campaignId),'', queryParams));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    selectedChannel: state.views.campaigns.selectedChannel,
    newsletters: state.models.newsletters.statistics.data.newsletters,
    newslettersOpenRate: state.models.newsletters.statistics.data.openRate,
    newslettersClickRate: state.models.newsletters.statistics.data.clickRate,
    newslettersOpenRateAvg: state.models.newsletters.statistics.data.openRateAvg,
    newslettersClickRateAvg: state.models.newsletters.statistics.data.clickRateAvg,
    queryParams: state.queryParams,
    newslettersTotalCount: state.models.newsletters.statistics.count
  }),
  dispatch => bindActionCreators(
    {...routerActions, loadStatisticsByCampaignId, onPageChange, setSelectedChannel, loadStatisticsByCampaignId}, dispatch)
)
export default class CampaignNewsletters extends Component {

  static propTypes = {
    selectedChannel: PropTypes.string,
    newsletters: PropTypes.array,
    newslettersOpenRate: PropTypes.array,
    newslettersClickRate: PropTypes.array,
    newslettersOpenRateAvg: PropTypes.number,
    newslettersClickRateAvg: PropTypes.number,
    loadStatisticsByCampaignId: PropTypes.func,
    queryParams: PropTypes.object,
    newslettersTotalCount: PropTypes.number
  };

  onNewsletterClick = (campaignId, newsletterId)=> {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/campaigns/${campaignId}/newsletters/${newsletterId}`, '');
  };

  onPageChangeClick = (e, selectedPage) => {

    const { loadStatisticsByCampaignId, queryParams, onPageChange, params: {campaignId}, selectedChannel } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit)};

    onPageChange(`/app/tenant/campaigns/${campaignId}/newsletters`, newQueryParams);
    loadStatisticsByCampaignId(campaignId, selectedChannel, newQueryParams);
  };

  onChannelTabClick = (channel) => {
    const {loadStatisticsByCampaignId, setSelectedChannel, params: {campaignId}} = this.props;
    onPageChange(`/app/tenant/campaigns/${campaignId}/newsletters`, defaultQueryParams);

    setSelectedChannel(channel);
    loadStatisticsByCampaignId(campaignId, channel, defaultQueryParams);
  };

  render() {
    const {newsletters, newslettersOpenRate, newslettersClickRate, newslettersOpenRateAvg, newslettersClickRateAvg,
      queryParams: {limit, offset}, newslettersTotalCount, selectedChannel} = this.props;

    const lineChartData = {
      data: {
        columns: [
          ['opened'].concat(newslettersOpenRate.map((item) => {
            if(Number(item.allMsgs) === 0) {
              return 0;
            } else {
              return Number(item.openMsgs) / Number(item.allMsgs);
            }
          }))
        ],
        types: {
          Opened: 'bar' // ADD
        }
      },
      grid: {
        y: {
          lines: [
            {value: newslettersOpenRateAvg, text: 'Average open rate'}
          ]
        }
      },
      axis: {
        x: {
          label: 'newsletters',
          type: 'category',
          categories: newsletters.map((item) => item.subject)
        },
        y: {
          label: 'open rate',
          min: 0,
          max: 1,
          padding: {top: 0, bottom: 0}
        }
      }
    };

//    if (selectedChannel == 'EMAIL') {
//      console.log("EMAIL");
//      lineChartData.data.columns.push(['clicked'].concat(newslettersClickRate.map((item) => {
//        if (Number(item.allLinks) == 0) {
//          return 0;
//        } else {
//          return Number(item.clickedLinks) / Number(item.allLinks);
//        }
//      })));
//
//      lineChartData.grid.y.lines.push({value: newslettersClickRateAvg, text: 'Average click rate'});
//    }

    return (
      <div className="panel-body">

        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Last newletters</h5>
                <div className="pull-right">
                  <ButtonToolbar>
                    <ButtonGroup bsSize="xsmall">
                      <Button onClick={() => this.onChannelTabClick('')}
                              className={selectedChannel === '' && styles.active}>All</Button>
                      <Button onClick={() => this.onChannelTabClick('EMAIL')}
                              className={selectedChannel === 'EMAIL' && styles.active}>Email</Button>
                      <Button onClick={() => this.onChannelTabClick('SMS')}
                              className={selectedChannel === 'SMS' && styles.active}>Sms</Button>
                      <Button onClick={() => this.onChannelTabClick('PUSH_NOTIFICATION')}
                              className={selectedChannel === 'PUSH_NOTIFICATION' && styles.active}>Notification</Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </div>
              </div>
              <div className="ibox-content">
                {selectedChannel !== 'SMS' &&
                <LineChart data={lineChartData} id="lineChartChannels" /> ||
                <ChartPlaceholder placeholderMessage="We are currently not tracking sms open rate."/>}
                <NewslettersCrudGrid
                  pagination={true}
                  newsletters={newsletters}
                  paginationOnPageChange={this.onPageChangeClick}
                  paginationPages={getPagesCount(newslettersTotalCount, limit)}
                  paginationActivePage={getActivePage(limit, offset)}
                  onNewsletterClick={this.onNewsletterClick} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
