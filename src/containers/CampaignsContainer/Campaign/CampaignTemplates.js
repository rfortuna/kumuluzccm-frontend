/**
 * Created by urbanmarovt on 13/05/16.
 */

import React, {Component, PropTypes} from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import {reduxForm} from 'redux-form';
import {toastr} from 'react-redux-toastr';
import {Input, Button, ButtonToolbar, Glyphicon, FormControls} from 'react-bootstrap';

import {setSelectedTemplate, remove as removeTemplate, clearState as clearSelectedTemplate} from 'redux/modules/models/templates/details';
import {load as loadTemplates} from 'redux/modules/models/templates/list';
import {refactorTemplates} from 'utils/dataSelectRefactor';

import { SimpleSelect, LabelsContainer, Iframe } from 'components';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(loadTemplates(100, 0, getState().router.params.campaignId)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    campaignTemplates: state.models.templates.list.data,
    selectedTemplate: state.models.templates.details.data,
    campaignId: state.router.params.campaignId
  }),
  dispatch => bindActionCreators(
    {...routerActions, setSelectedTemplate, removeTemplate, loadTemplates, clearSelectedTemplate}, dispatch)
)
@reduxForm({
  form: 'Template',
  fields: ['content']
}, state => ({}))
export default class CampaignTemplates extends Component {

  static propTypes = {
    fields: PropTypes.object,
    campaignTemplates: PropTypes.array,
    setSelectedTemplate: PropTypes.func,
    contentPreview: PropTypes.string
  };

  componentWillUnmount() {
    this.props.clearSelectedTemplate();
  }

  handleTemplateSelectChange = (item) => {
    const {setSelectedTemplate} = this.props;
    if (item) {
      setSelectedTemplate(item);
    } else {
      setSelectedTemplate(null);
    }
  };

  handleRemove = (templateId) => {

    const {removeTemplate, campaignId, loadTemplates} = this.props;

    toastr.confirm('Are you sure you want to delete the tenant?', {
      onOk: () => {
        removeTemplate(templateId).then((res) => {

          if (res && !res.error) {
            loadTemplates(100, 0, campaignId)
          }

        });
      }
    });

  };

  render() {
    const {fields: {content},selectedTemplate, campaignTemplates} = this.props;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Templates</h5>
              </div>
              <div className="ibox-content">
                <form className="form-horizontal">

                  <SimpleSelect label="Template" placeholder="Select template ..." labelClassName="col-xs-2" wrapperClassName="col-xs-10" {...content}
                                data={refactorTemplates(campaignTemplates)} handleOnChange={this.handleTemplateSelectChange}/>

                  {selectedTemplate && selectedTemplate.type === 'EMAIL' &&
                      <LabelsContainer label="Preview" labelClassName="col-xs-2"
                                           wrapperClassName="col-xs-10">
                        <Iframe content={selectedTemplate.value} />
                      </LabelsContainer>
                  }

                  {selectedTemplate && selectedTemplate.type !== 'EMAIL' &&
                  <FormControls.Static label="Preview" labelClassName="col-xs-2"
                                       wrapperClassName="col-xs-10">
                    {selectedTemplate.value}
                  </FormControls.Static>
                  }
                  {selectedTemplate &&
                  <div className="row">
                    <div className="col-xs-offset-2 col-xs-10">
                      <ButtonToolbar>
                        <Button onClick={()=> this.handleRemove(selectedTemplate.id)}>
                          <Glyphicon glyph="remove" />&nbsp;Delete
                        </Button>
                      </ButtonToolbar>
                    </div>
                  </div> }
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
