/**
 * Created by urbanmarovt on 10/02/16.
 */


import React, { Component, PropTypes } from 'react';

import {Link} from 'react-router';

export default class Campaign extends Component {

  static propTypes = {
    children: PropTypes.object.isRequired,
    location: PropTypes.object,
    params: PropTypes.object
  };

  render() {
    const {params: {campaignId}, location: {pathname}} = this.props;

    return (
      <div className="wrapper wrapper-content">
        <div className="tabs-container">
          <ul role="tablist" className="nav nav-tabs">
            <li role="presentation" className={pathname === `/app/tenant/campaigns/${campaignId}` && 'active'}>
              <Link to={`/app/tenant/campaigns/${campaignId}`}>Basic</Link>
            </li>
            <li role="presentation" className={pathname === `/app/tenant/campaigns/${campaignId}/newsletters` && 'active'}>
              <Link to={`/app/tenant/campaigns/${campaignId}/newsletters`}>Newsletters</Link>
            </li>
            <li role="presentation" className={pathname === `/app/tenant/campaigns/${campaignId}/statistics` && 'active'}>
              <Link to={`/app/tenant/campaigns/${campaignId}/statistics`}>Channel statistics</Link>
            </li>
            <li role="presentation" className={pathname === `/app/tenant/campaigns/${campaignId}/templates` && 'active'}>
              <Link to={`/app/tenant/campaigns/${campaignId}/templates`}>Channel templates</Link>
            </li>
            <li role="presentation" className={pathname === `/app/tenant/campaigns/${campaignId}/live` && 'active'}>
              <Link to={`/app/tenant/campaigns/${campaignId}/live`}>Live</Link>
            </li>
          </ul>
          <div className="tab-content">
            <div role="tabpanel" className="tab-pane active">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

/*

 <div className="row">
 <div className="col-lg-4">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Message distribution</h5>

 </div>
 <div className="ibox-content">
 <div>
 <PieChart
 data={pieData}
 width={300}
 height={300}
 radius={70}
 innerRadius={5}
 sectorBorderColor="white"
 />
 </div>
 </div>
 </div>
 </div>
 <div className="col-lg-8">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Messages per day</h5>
 </div>
 <div className="ibox-content">
 <LineChart data={lineChartData} id="lineChart"/>
 </div>
 </div>
 </div>
 </div>

 <div className="row">
 <div className="col-lg-12">
 <div className="ibox float-e-margins">
 <div className="ibox-title">
 <h5>Last messages</h5>
 <div className="pull-right">
 <div className="btn-group">
 <button type="button" className="btn btn-xs btn-white active">All</button>
 <button type="button" className="btn btn-xs btn-white">Completed</button>
 <button type="button" className="btn btn-xs btn-white">Pending</button>
 <button type="button" className="btn btn-xs btn-white">Undelivered</button>
 </div>
 </div>
 </div>
 <div className="ibox-content">
 <table className="table table-hover no-margins">
 <thead>
 <tr>
 <th>Status</th>
 <th>Date</th>
 <th>Referent</th>
 <th>Subject</th>
 <th>Delivered</th>
 </tr>
 </thead>
 <tbody>
 <tr>
 <td><small>Pending...</small></td>
 <td><i className="fa fa-clock-o"></i> 11:20pm</td>
 <td>Mojca</td>
 <td>Dostava kreditne kartice</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 24% </td>
 </tr>
 <tr>
 <td><span className="label label-danger">Undelivered</span> </td>
 <td><i className="fa fa-clock-o"></i> 10:40am</td>
 <td>Mojca</td>
 <td>Posodobitev uporabniškega računa</td>
 <td className="text-danger"> <i className="fa fa-level-up"></i> 0% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 01:30pm</td>
 <td>Ana</td>
 <td>Novoletno voščilo</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 54% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 02:20pm</td>
 <td>Petra</td>
 <td>Novoletno voščilo</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 12% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 09:40pm</td>
 <td>Nejc</td>
 <td>Posodobitev uporabniškega računa</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 22% </td>
 </tr>
 <tr>
 <td><span className="label label-primary">Completed</span> </td>
 <td><i className="fa fa-clock-o"></i> 04:10am</td>
 <td>Miha</td>
 <td>Dostava kreditne kartice</td>
 <td className="text-navy"> <i className="fa fa-level-up"></i> 100% </td>
 </tr>
 <tr>
 <td><small>Pending...</small> </td>
 <td><i className="fa fa-clock-o"></i> 12:08am</td>
 <td>Miha</td>
 <td>Nov uporabnik</td>
 <td className="text-warning"> <i className="fa fa-level-up"></i> 23% </td>
 </tr>
 </tbody>
 </table>
 </div>
 </div>
 </div>
 </div>*/
