/**
 * Created by urbanmarovt on 01/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import { ChannelStatistics, NewslettersGrid} from 'components';
import {LineChart} from 'components';
import {ButtonToolbar, ButtonGroup, Button} from 'react-bootstrap';

import {loadCountByChannels, loadCampaignHistoryByChannels} from 'redux/modules/models/messages/count';
import {setSelectedTime} from 'redux/modules/views/campaigns';

function fetchDataDeferred(getState, dispatch) {
  const campaignId = getState().router.params.campaignId;

  const promises = [];
  promises.push(dispatch(loadCountByChannels('d', campaignId)));
  promises.push(dispatch(loadCampaignHistoryByChannels('d', campaignId)));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    countByChannels: state.models.messages.count.byChannels,
    historyByChannels: state.models.messages.count.history,
    selectedTime: state.views.campaigns.selectedTime
  }),
  dispatch => bindActionCreators({...routerActions, loadCountByChannels, loadCampaignHistoryByChannels, setSelectedTime}, dispatch)
)
export default class CampaignStatistics extends Component {

  static propTypes = {
    countByChannels: PropTypes.object,
    historyByChannels: PropTypes.object,
    loadCountByChannels: PropTypes.func,
    loadHistoryByChannels: PropTypes.func,
    params: PropTypes.object,
    pushState: PropTypes.func,
    loadCampaignHistoryByChannels: PropTypes.func,
    selectedTime: PropTypes.string,
    setSelectedTime: PropTypes.func
  };

  onHistoryTabClick = (timePeriod) => {
    const {loadCountByChannels, loadCampaignHistoryByChannels, params: {campaignId}, setSelectedTime} = this.props;
    loadCountByChannels(timePeriod, campaignId);
    loadCampaignHistoryByChannels(timePeriod, campaignId);
    setSelectedTime(timePeriod);
  };

  onNewsletterClick = (id)=> {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/newsletters/${id}`, '');
  };

  render() {

    const {countByChannels, historyByChannels, selectedTime} = this.props;

    let lineChartData2 = {
      data: {
        x: 'x',
        columns: [
          [...historyByChannels.x],
          [...historyByChannels.sms],
          [...historyByChannels.email],
          [...historyByChannels.notification]
        ]
      },
      axis: {
        x: {
          type: 'timeseries',
          label: 'time',
          tick: {}
        },
        y: {
          label: 'messages',
          min: 0,
          padding: {top: 0, bottom: 0}
        }
      }
    };

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Channels</h5>
                <div className="pull-right">
                  <ButtonToolbar>
                    <ButtonGroup bsSize="xsmall">
                      <Button onClick={() => this.onHistoryTabClick('d')} className={selectedTime == 'd' ? 'active' : ''}>Today</Button>
                      <Button onClick={() => this.onHistoryTabClick('M')} className={selectedTime == 'M' ? 'active' : ''}>Monthly</Button>
                      <Button onClick={() => this.onHistoryTabClick('y')} className={selectedTime == 'y' ? 'active' : ''}>Annual</Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </div>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-3">
                    <h1>
                      {countByChannels.all}
                    </h1>
                    <h5>Total messages in period</h5>
                  </div>
                  <div className="col-lg-9">
                    <ul className="stat-list">
                      <li>
                        <ChannelStatistics label="Total SMS in period" channelCount={countByChannels.sms} channelPercent={countByChannels.smsPercent} />
                      </li>
                      <li>
                        <ChannelStatistics label="Total Email in period" channelCount={countByChannels.email} channelPercent={countByChannels.emailPercent} />
                      </li>
                      <li>
                        <ChannelStatistics label="Total Notifications in period" channelCount={countByChannels.notification} channelPercent={countByChannels.notificationPercent} />
                      </li>
                    </ul>
                  </div>
                </div>
                <br/>
                <br/>
                <div className="row">
                  <LineChart data={lineChartData2} id="lineChartHistory" selectedTime={selectedTime} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


