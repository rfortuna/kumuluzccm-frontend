/**
 * Created by urbanmarovt on 09/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';
import {Input} from 'react-bootstrap';

import {CampaignsGrid, MultiSelect} from 'components';
import { load as loadCampaigns} from 'redux/modules/models/campaigns/list';
import {isLoaded as areTagsLoaded, load as loadTags} from 'redux/modules/models/tags';
import {refactorTags} from 'utils/dataSelectRefactor';
import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import {getQueryParams, toggleOrder, getSearchTags} from 'utils/queryParams';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  order: 'name ASC',
  searchString: '',
  searchTags: ''
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  let queryParams = getQueryParams(getState().router, defaultQueryParams);

  queryParams = {...queryParams, searchTags: getSearchTags(getState().router) };

  promises.push(dispatch(setInitialStateQueryParams('/app/tenant/campaigns', queryParams)));
  promises.push(dispatch(loadCampaigns(queryParams)));

  if (!areTagsLoaded(getState())) {
    promises.push(dispatch(loadTags()));
  }
  return Promise.all(promises);
}

const readQueryParams = (router) => {
  const {searchString} = getQueryParams(router, defaultQueryParams);
  const searchTags = getSearchTags(router);

  return {searchString: searchString, searchTags: searchTags};
};

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    campaigns: state.models.campaigns.list.data,
    allTags: state.models.tags.data,
    campaignsTotalCount: state.models.campaigns.list.count,
    router: state.router,
    queryParams: state.queryParams
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      loadCampaigns,
      onPageChange,
      onSearchStringChange,
      onOrderChange
    }, dispatch)
)
@reduxForm({
  form: 'CampaignsSearchForm',
  fields: ['searchTags', 'searchString']
},
state => ({
  initialValues: readQueryParams(state.router)
}))
export default class Campaigns extends Component {

  static propTypes = {
    allTags: PropTypes.array,
    campaigns: PropTypes.array,
    history: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    campaignsTotalCount: PropTypes.number,
    router: PropTypes.object,
    fields: PropTypes.object,
    loadCampaigns: PropTypes.func,
    queryParams: PropTypes.object,
    onPageChange: PropTypes.func,
    onSearchStringChange: PropTypes.func,
    onOrderChange: PropTypes.func
  }


  onCampaignClick = (id) => {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/campaigns/${id}`, '');
  };

  onPageChangeClick = (error, selectedPage) => {
    const { loadCampaigns, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange('/app/tenant/campaigns', newQueryParams);
    loadCampaigns(newQueryParams);
  };

  onFormStateChange = (field, fieldValue) => {

    const { loadCampaigns, queryParams, onSearchStringChange } = this.props;

    let newQueryParams = {...queryParams};
    if (field.name === 'searchTags') {
      newQueryParams = {
        ...newQueryParams,
        searchTags: fieldValue,
        offset: 0
      };
      loadCampaigns(newQueryParams);

    } else if (field.name === 'searchString') {
      newQueryParams = {
        ...newQueryParams,
        searchString: fieldValue,
        offset: 0
      };
      loadCampaigns(newQueryParams);
    }
    onSearchStringChange('/app/tenant/campaigns', newQueryParams);

  };

  onSortClick = (field)=> {
    const { loadCampaigns, queryParams, onOrderChange } = this.props;
    const newQueryParams = {
      ...queryParams,
      order: toggleOrder(field, queryParams.order),
      limit: defaultQueryParams.limit,
      offset: 0
    };
    onOrderChange('/app/tenant/campaigns', newQueryParams);
    loadCampaigns(newQueryParams);
  };

  makeOnChangeMultiSelect = (field) => {
    return (value) => {
      field.onChange(value);
      this.onFormStateChange(field, value);
    };
  };

  makeOnChangeInput = (field) => {
    return (event) => {
      field.onChange(event);
      this.onFormStateChange(field, event.target.value);
    };
  };

  render() {
    const { allTags, campaigns, campaignsTotalCount, fields: {searchTags, searchString} } = this.props;

    const {limit, offset} = this.props.queryParams;

    return (
    <div>
      <div className="row wrapper border-bottom white-bg page-heading">
        <div className="col-lg-10">
          <h2>Campaigns</h2>
        </div>
        <div className="col-lg-2">

        </div>
      </div>
      <div className="wrapper wrapper-content animated fadeInRight p-b-none">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
                <form className="form-horizontal">
                  <Input type="text" label="Search by name" {...searchString}
                         labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                         onChange={this.makeOnChangeInput(searchString)}/> {/* onChange must be on last place*/}
                  <MultiSelect labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                    {...searchTags} data={refactorTags(allTags)} label="Choose tags"
                               onChange={this.makeOnChangeMultiSelect(searchTags)}/> {/* onChange must be on last place*/}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="wrapper wrapper-content animated fadeInRight p-t-none">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
                <CampaignsGrid
                  campaigns={campaigns}
                  onCampaignClick={this.onCampaignClick}
                  paginationOnPageChange={this.onPageChangeClick}
                  paginationPages={getPagesCount(campaignsTotalCount, limit)}
                  paginationActivePage={getActivePage( limit, offset)}
                  onSortClick={this.onSortClick}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}
