/**
 * Created by urbanmarovt on 11/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {clearSendingData} from 'redux/modules/views/sending';

import {MessageTypeCard} from 'components';
import {messageTypes} from 'utils/constants';

import {load as loadTenant, isLoaded as isTenantLoaded} from 'redux/modules/models/tenants/details';


function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  if (!isTenantLoaded(getState())) {
    promises.push(dispatch(loadTenant()));
  }

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    tenant: state.models.tenants.details.data
  }),
  dispatch => bindActionCreators({...routerActions, clearSendingData}, dispatch)
)
export default class NewsletterNew extends Component {

  static propTypes = {
    pushState: PropTypes.func,
    clearSendingData: PropTypes.func
  };

  handleCancel = () => {
    this.props.pushState(null, '/app');
  };

  redirectToSending = (channelId) => {
    this.props.clearSendingData();
    this.props.pushState(null, '/app/tenant/sending/setup', {channelId: channelId});
  };

  render() {

    const {tenant} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New message</h2>
          </div>
          <div className="col-lg-2"></div>
        </div>

        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            {messageTypes.map((messageType) =>
              <div key={messageType.id} className="col-lg-3 col-md-4 col-sm-6">
                <MessageTypeCard  messageType={messageType} click={this.redirectToSending}
                                  disabled={!tenant.smsUser && messageType.sms && messageType.id !== '4'
                                  /*Disabled if smsUser not accepted and messageType has sms and it is not preferedChannel*/}/>
              </div>)
            }
          </div>
        </div>
      </div>
    );
  }

}

