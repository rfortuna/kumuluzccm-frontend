/**
 * Created by rokfortuna on 4/22/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm, change as changeFieldValue} from 'redux-form';
import {Input} from 'react-bootstrap';

import {load as loadNewsletters} from 'redux/modules/models/newsletters/list';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';
import {getQueryParams} from 'utils/queryParams';
import {NewslettersCrudGrid, ButtonGroupSelect} from 'components';
import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import styles from './Newsletters.scss';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  searchString: '',
  order: 'createdAt DESC'
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  const queryParams = getQueryParams(getState().router, defaultQueryParams);

  promises.push(dispatch(setInitialStateQueryParams('/app/tenant/newsletters', queryParams)));
  promises.push(dispatch(loadNewsletters(queryParams)));

//  if (!areTagsLoaded(getState())) {
//    promises.push(dispatch(loadTags()));
//  }
  return Promise.all(promises);
}

const readQueryParams = (router) => {
  const {searchString} = getQueryParams(router, defaultQueryParams);
  return {searchString: searchString};
};

const newslettersStatusOptions = [
  {label: 'All', value: 'ALL'},
  {label: 'Sent', value: 'SENT'},
  {label: 'Draft', value: 'DRAFT'},
  {label: 'Pending', value: 'PENDING'}
];


@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    newsletters: state.models.newsletters.list.data,
    queryParams: state.queryParams,
    newslettersTotalCount: state.models.newsletters.list.count,
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      loadNewsletters,
      onPageChange,
      onSearchStringChange,
      changeFieldValue
    }, dispatch)
)
@reduxForm({
    form: 'NewslettersSearchForm',
    fields: ['searchString', 'status']
  },
  state => ({
    initialValues: {
      ...readQueryParams(state.router),
      status: 'ALL'
    }
  }))
export default class Newsletters extends Component {

  static propTypes = {
    newsletters: PropTypes.array,
    queryParams: PropTypes.object,
    onPageChange: PropTypes.func,
    onSearchStringChange: PropTypes.func,
    newslettersTotalCount: PropTypes.number,
    pushState: PropTypes.func.isRequired,
    loadNewsletters: PropTypes.func,
    changeFieldValue: PropTypes.func
  };


  onNewsletterClick = (campaignId, newsletterId) => {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/campaigns/${campaignId}/newsletters/${newsletterId}`, '');
  };

  onPageChangeClick = (error, selectedPage) => {
    const { loadNewsletters, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange('/app/tenant/newsletters', newQueryParams);
    loadNewsletters(newQueryParams);
  };

  onFormStateChange = (field, fieldValue) => {

    const { loadNewsletters, queryParams, onSearchStringChange, fields: {status} } = this.props;

    let newQueryParams = {...queryParams};
    if (field.name === 'searchString') {
      newQueryParams = {
        ...newQueryParams,
        searchString: fieldValue,
        offset: 0
      };
      loadNewsletters(newQueryParams);
    }

    if (field.name === 'status') {
      newQueryParams = {
        ...newQueryParams,
        offset: 0,
        status: fieldValue
      };
      loadNewsletters(newQueryParams);
    }
    onSearchStringChange('/app/tenant/newsletters', newQueryParams);

  };

  makeOnChangeInput = (field) => {
    return (event) => {
      field.onChange(event);
      this.onFormStateChange(field, event.target.value);
    };
  };

  render() {
    const {newsletters, fields: {searchString, status},
      queryParams: {limit, offset}, newslettersTotalCount, changeFieldValue} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Newsletters</h2>
          </div>
          <div className="col-lg-2">

          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight p-b-none">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <form className="form-horizontal">
                    <Input type="text" label="Search by subject" {...searchString}
                           labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                           onChange={this.makeOnChangeInput(searchString)}/> {/* onChange must be on last place*/}
                    <div className="row">
                      <label className="col-sm-2 control-label">Status</label>
                      <div className="col-sm-10">
                      <ButtonGroupSelect formName="NewslettersSearchForm" options={newslettersStatusOptions} field={status}
                                         changeFieldValue={changeFieldValue} onFormStateChange={this.onFormStateChange}/>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight p-t-none">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <NewslettersCrudGrid
                  pagination={true}
                  newsletters={newsletters}
                  paginationOnPageChange={this.onPageChangeClick}
                  paginationPages={getPagesCount(newslettersTotalCount, limit)}
                  paginationActivePage={getActivePage(limit, offset)}
                  onNewsletterClick={this.onNewsletterClick} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
