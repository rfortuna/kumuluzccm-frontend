/**
 * Created by urbanmarovt on 03/03/16.
*/

import React, { Component, PropTypes} from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {
  load as loadNewsletter,
  clearState as clearNewsletterDetails,
  send as sendNewsletter,
  cancel as cancelNewsletter} from 'redux/modules/models/newsletters/details';
import { prefillSending } from 'redux/modules/views/sending';
import { FormControls, Button, ButtonToolbar, Glyphicon } from 'react-bootstrap';
import { messageTypes } from 'utils/constants';
import { DateTimeLabel, MessageStatusLabel, ChannelDisplayGrid, ListDisplayGrid, RateProgress, LabelsContainer, Iframe} from 'components';


import _ from 'lodash';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadNewsletter(getState().router.params.campaignId, getState().router.params.newsletterId));
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    newsletter: state.models.newsletters.details.newsletter
  }),
  dispatch => bindActionCreators(
    {...routerActions, loadNewsletter, clearNewsletterDetails, prefillSending, sendNewsletter, cancelNewsletter}, dispatch)
)
export default class Newsletter extends Component {

  static propTypes = {
    newsletter: PropTypes.object,
    clearNewsletterDetails: PropTypes.func,
    sendNewsletter: PropTypes.func,
    cancelNewsletter: PropTypes.func,
    loadNewsletter: PropTypes.func
  };

  componentWillUnmount() {
    const {clearNewsletterDetails} = this.props;

    clearNewsletterDetails();
  }

  handleResend = () => {
    const {prefillSending, newsletter, pushState} = this.props;

    prefillSending(newsletter);

    if (newsletter.preferred) {
      pushState(null, `/app/tenant/sending/setup?channelId=4`);
    } else {
      const channelEmail = newsletter.channels.find((element) => {
        return element.type === 'EMAIL';
      });

      const channelSms = newsletter.channels.find((element) => {
        return element.type === 'SMS';
      });

      const channelPush = newsletter.channels.find((element) => {
        return element.type === 'PUSH_NOTIFICATION';
      });

      const messageType = messageTypes.find((type) => {
        // use ! to convert object to boolean
        return !type.email === !channelEmail && !type.sms === !channelSms && !type.push === !channelPush && type.id !== '4'
      });

      pushState(null, `/app/tenant/sending/setup?channelId=${messageType.id}`);

    }

  };

  handleSend = () => {
    const {loadNewsletter, sendNewsletter, newsletter: {id: newsletterId, campaign: {id: campaignId}}} = this.props;

    sendNewsletter(campaignId, newsletterId).then((res) => {
      if(res.error) {
        //handle error
      } else {
        loadNewsletter(campaignId, newsletterId);
      }
    });

  };

  handleCancel = () => {
    const {cancelNewsletter, loadNewsletter, newsletter: {id: newsletterId, campaign: {id: campaignId}}} = this.props;

    cancelNewsletter(campaignId, newsletterId).then((res) => {
      if(res.error) {
        //handle error
      } else {
        loadNewsletter(campaignId, newsletterId);

      }
    });

  };

  render() {
    const {newsletter} = this.props;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Newsletter info</h5>
              </div>
              <div className="ibox-content p-md">
                <div>
                  <form className="form-horizontal">
                    <FormControls.Static label="Status" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      <MessageStatusLabel type={newsletter.status} />
                    </FormControls.Static>
                    {newsletter.status == 'PENDING' &&
                      <FormControls.Static label="Delivery schedule" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                        <DateTimeLabel date={newsletter.deliverAfter} />
                      </FormControls.Static>
                    }
                    <FormControls.Static label="Referent" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      {`${newsletter.user.firstName} ${newsletter.user.lastName}`}
                    </FormControls.Static>
                    {newsletter.status == 'SENT' &&
                    <div>
                      <div className="hr-line-dashed"></div>
                      <FormControls.Static label="Number of messages" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                        {newsletter.totalCount} &nbsp;
                      </FormControls.Static>
                      <div className="row">
                        <div className="col-xs-offset-2 col-sm-5">
                          <RateProgress rate={newsletter.openRate.rate} label="Open rate"/>
                        </div>
                        <div className="col-sm-5">
                          <RateProgress rate={newsletter.clickRate.rate} label="Click rate"/>
                        </div>
                      </div>
                    </div>}
                    <div className="hr-line-dashed"></div>
                    <FormControls.Static label="Channel" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      {!newsletter.preferred &&
                      <ChannelDisplayGrid channels={newsletter.channels} /> ||
                      <ChannelDisplayGrid channels={[{id: '1', type: 'PREFERRED_CHANNEL'}]}/>}
                    </FormControls.Static>
                    <div className="hr-line-dashed"></div>
                    <FormControls.Static label="Campaign" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      {newsletter.campaign.name}
                    </FormControls.Static>
                    { !newsletter.allReceivers &&
                    <LabelsContainer label="Receiver lists" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      <ListDisplayGrid lists={newsletter.receiverLists} />
                    </LabelsContainer>}
                    { newsletter.allReceivers &&
                    <FormControls.Static label="Receivers" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      All
                    </FormControls.Static>}
                    <FormControls.Static label="Ignore receivers preferences" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      {!newsletter.ignoreUserPreferences && <i className="fa fa-remove"/>}
                      {newsletter.ignoreUserPreferences && <i className="fa fa-check"/>}
                    </FormControls.Static>
                    <div className="hr-line-dashed"></div>
                    <FormControls.Static label="Subject" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{newsletter.subject}</FormControls.Static>
                    { _.find(newsletter.channels, ['type', 'SMS', 'PUSH_NOTIFICATION']) &&
                    <FormControls.Static label="Short content" labelClassName="col-xs-2"
                                         wrapperClassName="col-xs-10">{newsletter.contentShort}</FormControls.Static>
                    }
                    { _.find(newsletter.channels, {type: 'EMAIL'}) &&
                        <LabelsContainer label="Email content" labelClassName="col-xs-2"
                                             wrapperClassName="col-xs-10">
                          <Iframe content={newsletter.contentLong} />
                        </LabelsContainer>
                    }
                    <div className="hr-line-dashed"></div>

                    <div className="row">
                      <div className="col-xs-offset-2 col-xs-10">
                        <ButtonToolbar>
                          {newsletter.status == 'DRAFT' &&
                            <Button onClick={this.handleSend}><Glyphicon glyph="envelope" /> &nbsp;Send</Button>
                          }
                          <Button onClick={this.handleResend}><Glyphicon glyph="forward" /> &nbsp;Reuse content</Button>
                          {newsletter.status == 'PENDING' &&
                          <Button onClick={this.handleCancel}><Glyphicon glyph="remove" /> &nbsp;Cancel</Button>}
                        </ButtonToolbar>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
