/**
 * Created by urbanmarovt on 01/07/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import {reset as resetForm} from 'redux-form';

import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import {TenantForm, TenantsGrid} from 'components';
import {load as loadTenant, isLoaded as isTenantLoaded, edit as editTenant,
  update as updateTenant, cancelEdit as cancelEditTenant, showLogoPreview} from 'redux/modules/models/tenants/details';
import {toastr} from 'react-redux-toastr';

import styles from './Settings.scss';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  if (!isTenantLoaded(getState())) {
    promises.push(dispatch(loadTenant()));
  }

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    tenant: state.models.tenants.details,
    logoFile: state.models.tenants.details.logoFile
  }),
  dispatch => bindActionCreators(
    {...routerActions, editTenant, showLogoPreview, updateTenant, cancelEditTenant}, dispatch)
)
export default class UserTenants extends Component {

  static propTypes = {
    tenant: PropTypes.object,
    editTenant: PropTypes.func,
    showLogoPreview: PropTypes.func,
    updateTenant: PropTypes.func,
    cancelEditTenant: PropTypes.func,
    logoFile: PropTypes.object
  };

  handleSubmit = (data) => {
    this.props.updateTenant(data).then((res) => {
      // success
      if (res.error) {
        // handle error
        toastr.error('There was an error creating new tenant.');
      } else {
        // handle success
        window.location.reload();
      }
    });
  };

  handleCancel = () => {
    this.props.cancelEditTenant();
  };

  render() {
    const {tenant, editTenant, showLogoPreview, logoFile} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Tenant settings</h2>
          </div>
        </div>
        <div className="panel-body">
          <div className="row">
            <div className="col-sm-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <div>
                    {!tenant.editing &&
                    <form className="form-horizontal">
                      <FormControls.Static label="Name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.data.name}/>
                      <FormControls.Static label="Logo" labelClassName="col-xs-2" wrapperClassName="col-xs-10"><img src={tenant.data.logo} className={styles.logo} /></FormControls.Static>
                      <div className="hr-line-dashed"></div>
                      <FormControls.Static label="Sending email" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.data.mailFrom}/>
                      <FormControls.Static label="Sending email name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.data.mailUser}/>
                      <FormControls.Static label="Sending sms name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.data.smsUser}/>
                      <div className="hr-line-dashed"></div>
                      <FormControls.Static label="&ensp;" labelClassName="col-xs-2" wrapperClassName="col-xs-10">Requested sms name value and status</FormControls.Static>
                      <FormControls.Static label="Sending sms pending name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.data.smsUserPending}/>
                      <FormControls.Static label="Sending sms pending name status" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.data.smsUserPendingStatus}/>
                      <div className="hr-line-dashed"></div>
                      <div className="row">
                        <div className="col-xs-offset-2 col-xs-10">
                          <ButtonToolbar>
                            <Button onClick={editTenant}><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                          </ButtonToolbar>
                        </div>
                      </div>
                    </form>
                    }
                    {tenant.editing &&
                      <TenantForm initialValues={tenant.data} showLogoPreview={showLogoPreview} onSubmit={this.handleSubmit} handleCancel={this.handleCancel} logoFile={logoFile} />
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
