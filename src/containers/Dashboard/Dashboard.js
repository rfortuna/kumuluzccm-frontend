import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class Dashboard extends Component {

  static propTypes = {
    children: PropTypes.object.isRequired,
    location: PropTypes.object
  };

  render() {

    const {location: {pathname}} = this.props;

    return (
      <div className="wrapper wrapper-content">
        <div className="tabs-container">
          <ul role="tablist" className="nav nav-tabs">
            <li role="presentation" className={pathname === '/app/tenant' && 'active'}>
              <Link to={`/app/tenant`}>Basic Info</Link>
            </li>
            <li role="presentation" className={pathname === '/app/tenant/history' && 'active'}>
              <Link to={`/app/tenant/history`}>Statistics</Link>
            </li>
            <li role="presentation" className={pathname === '/app/tenant/live' && 'active'}>
              <Link to={`/app/tenant/live`}>Live</Link>
            </li>
          </ul>
          <div className="tab-content">
            <div role="tabpanel" className="tab-pane active">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}