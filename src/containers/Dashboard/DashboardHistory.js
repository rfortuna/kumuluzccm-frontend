/**
 * Created by rokfortuna on 29/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import {ChannelStatistics} from 'components';
import {LineChart} from 'components';
import {ButtonToolbar, ButtonGroup, Button} from 'react-bootstrap';

import {loadCountByChannels, loadHistoryByChannels} from 'redux/modules/models/messages/count';
import {setSelectedTime} from 'redux/modules/views/dashboard';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadCountByChannels('d')));
  promises.push(dispatch(loadHistoryByChannels('d')));
  return Promise.all(promises);
}

import styles from './Dashboard.scss';

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    countByChannels: state.models.messages.count.byChannels,
    historyByChannels: state.models.messages.count.history,
    selectedTime: state.views.dashboard.selectedTime
  }),
  dispatch => bindActionCreators({...routerActions, loadCountByChannels, loadHistoryByChannels, setSelectedTime}, dispatch)
)
export default class DashboardHistory extends Component {

  static propTypes = {
    countByChannels: PropTypes.object,
    historyByChannels: PropTypes.object,
    loadCountByChannels: PropTypes.func,
    loadHistoryByChannels: PropTypes.func,
    pushState: PropTypes.func,
    selectedTime: PropTypes.string,
    setSelectedTime: PropTypes.func
  };

  onHistoryTabClick = (timePeriod) => {
    const {loadCountByChannels, loadHistoryByChannels, setSelectedTime} = this.props;
    loadCountByChannels(timePeriod);
    loadHistoryByChannels(timePeriod);
    setSelectedTime(timePeriod);
  };

  render() {
    const {countByChannels, historyByChannels, selectedTime} = this.props;

    const lineChartData2 = {
      data: {
        x: 'x',
        columns: [
          [...historyByChannels.x],
          [...historyByChannels.sms],
          [...historyByChannels.email],
          [...historyByChannels.notification]
        ]
      },
      axis: {
        x: {
          type: 'timeseries',
          label: 'time',
          tick: {},
        },
        y: {
          label: 'messages',
          min: 0,
          padding: {top: 0, bottom: 0}
        }
      }
    };

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Channels</h5>
                <div className="pull-right">
                  <ButtonToolbar>
                    <ButtonGroup bsSize="xsmall">
                      <Button onClick={() => this.onHistoryTabClick('d')} className={selectedTime == 'd' ? 'active' : ''}>Today</Button>
                      <Button onClick={() => this.onHistoryTabClick('M')} className={selectedTime == 'M' ? 'active' : ''}>Monthly</Button>
                      <Button onClick={() => this.onHistoryTabClick('y')} className={selectedTime == 'y' ? 'active' : ''}>Annual</Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </div>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-3">
                    <h1>
                      {countByChannels.all}
                    </h1>
                    <h5>Total messages in period</h5>
                  </div>
                  <div className="col-lg-9">
                    <ul className="stat-list">
                      <li>
                        <ChannelStatistics label="Total SMS in period" channelCount={countByChannels.sms} channelPercent={countByChannels.smsPercent} />
                      </li>
                      <li>
                        <ChannelStatistics label="Total Email in period" channelCount={countByChannels.email} channelPercent={countByChannels.emailPercent} />
                      </li>
                      <li>
                        <ChannelStatistics label="Total Notifications in period" channelCount={countByChannels.notification} channelPercent={countByChannels.notificationPercent} />
                      </li>
                    </ul>
                  </div>
                </div>
                <br/>
                <br/>
                <div className="row">
                  <LineChart data={lineChartData2} id="lineChartHistory" selectedTime={selectedTime}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}


