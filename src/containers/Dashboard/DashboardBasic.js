/**
 * Created by urbanmarovt on 12/04/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import {MessageStatistics, NewslettersCrudGrid, LineChart, BarChart, ChartPlaceholder} from 'components';
import { ButtonToolbar, ButtonGroup, Button} from 'react-bootstrap';
import {loadCountByTime} from 'redux/modules/models/messages/count';
import config from 'config';
import {loadStatistics as loadNewsletterStatistics} from 'redux/modules/models/newsletters/statistics';
import {setSelectedChannel} from 'redux/modules/views/dashboard';

import styles from './Dashboard.scss';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadCountByTime()));
  promises.push(dispatch(loadNewsletterStatistics()));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    selectedChannel: state.views.dashboard.selectedChannel,
    countByTime: state.models.messages.count.byTime,
    newsletters: state.models.newsletters.statistics.data.newsletters,
    newslettersOpenRate: state.models.newsletters.statistics.data.openRate,
    newslettersClickRate: state.models.newsletters.statistics.data.clickRate,
    newslettersOpenRateAvg: state.models.newsletters.statistics.data.openRateAvg,
    newslettersClickRateAvg: state.models.newsletters.statistics.data.clickRateAvg
  }),
  dispatch => bindActionCreators({...routerActions, loadNewsletterStatistics, setSelectedChannel}, dispatch)
)
export default class DashboardBasic extends Component {

  static propTypes = {
    selectedChannel: PropTypes.string,
    countByTime: PropTypes.object,
    newsletters: PropTypes.array,
    newslettersOpenRate: PropTypes.array,
    newslettersClickRate: PropTypes.array,
    newslettersOpenRateAvg: PropTypes.number,
    newslettersClickRateAvg: PropTypes.number,
    loadNewsletterStatistics: PropTypes.func,
    setSelectedChannel: PropTypes.func
  };

  onNewsletterClick = (campaignId, newsletterId)=> {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/campaigns/${campaignId}/newsletters/${newsletterId}`, '');
  };

  onChannelTabClick = (channel) => {
    const {loadNewsletterStatistics, setSelectedChannel} = this.props;
    setSelectedChannel(channel);
    loadNewsletterStatistics(channel);
  };

  render() {
    const {countByTime, newsletters, newslettersOpenRate, newslettersClickRate,
      newslettersOpenRateAvg, newslettersClickRateAvg, selectedChannel} = this.props;

    const lineChartData = {
      data: {
        columns: [
          ['opened'].concat(newslettersOpenRate.map((item) => {
            if(Number(item.allMsgs) === 0) {
              return 0;
            } else {
              return Number(item.openMsgs) / Number(item.allMsgs);
            }
          }))
        ],
        types: {
          Opened: 'bar' // ADD
        }
      },
      grid: {
        y: {
          lines: [
            {value: newslettersOpenRateAvg, text: 'Average open rate'}
          ]
        }
      },
      axis: {
        x: {
          label: 'newsletters',
          type: 'category',
          categories: newsletters.map((item) => item.subject)
        },
        y: {
          label: 'open rate',
          min: 0,
          max: 1,
          padding: {top: 0, bottom: 0}
        }
      }
    };

//    if (selectedChannel == 'EMAIL') {
//      console.log("EMAIL");
//      lineChartData.data.columns.push(['clicked'].concat(newslettersClickRate.map((item) => {
//        if (Number(item.allLinks) == 0) {
//          return 0;
//        } else {
//          return Number(item.clickedLinks) / Number(item.allLinks);
//        }
//      })));
//
//      lineChartData.grid.y.lines.push({value: newslettersClickRateAvg, text: 'Average click rate'});
//    }

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-3">
            <MessageStatistics title="All time" totalCount={countByTime.allTime.count} clickedCount={countByTime.allTime.delivered} openedCount={countByTime.allTime.opened} />
          </div>
          <div className="col-lg-3">
            <MessageStatistics title="Annual" totalCount={countByTime.annual.count} clickedCount={countByTime.annual.delivered} openedCount={countByTime.annual.opened} />
          </div>
          <div className="col-lg-3">
            <MessageStatistics title="Monthly" totalCount={countByTime.monthly.count} clickedCount={countByTime.monthly.delivered} openedCount={countByTime.monthly.opened} />
          </div>
          <div className="col-lg-3">
            <MessageStatistics title="Today" totalCount={countByTime.today.count} clickedCount={countByTime.today.delivered} openedCount={countByTime.today.opened} />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Last newletters</h5>
                <div className="pull-right">
                  <ButtonToolbar>
                    <ButtonGroup bsSize="xsmall">
                      <Button onClick={() => this.onChannelTabClick('')}
                              className={selectedChannel === '' && styles.active}>All</Button>
                      <Button onClick={() => this.onChannelTabClick('EMAIL')}
                              className={selectedChannel === 'EMAIL' && styles.active}>Email</Button>
                      <Button onClick={() => this.onChannelTabClick('SMS')}
                              className={selectedChannel === 'SMS' && styles.active}>Sms</Button>
                      <Button onClick={() => this.onChannelTabClick('PUSH_NOTIFICATION')}
                              className={selectedChannel === 'PUSH_NOTIFICATION' && styles.active}>Notification</Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </div>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    {selectedChannel !== 'SMS' &&
                    <LineChart data={lineChartData} id="lineChartChannels" /> ||
                    <ChartPlaceholder placeholderMessage="We are currently not tracking sms open rate."/>}
                    <NewslettersCrudGrid newsletters={newsletters} onNewsletterClick={this.onNewsletterClick}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }

}
