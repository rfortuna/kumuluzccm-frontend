/**
 * Created by rokfortuna on 29/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {MessageStatistics} from 'components';
import {LineChart, BarChart} from 'components';
import {onMessageSocket, clearState} from 'redux/modules/views/dashboard';
import config from 'config';


function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  return Promise.all(promises);
}

let webSocket;

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    rates: state.views.dashboard.liveStatistics.rates,
    bars: state.views.dashboard.liveStatistics.bars,
    websocketUrl: state.global.config.websocketUrl
  }),
  dispatch => bindActionCreators({onMessageSocket, clearState}, dispatch)
)
export default class DashboardLive extends Component {

  static propTypes = {
    countByTime: PropTypes.object,
    rates: PropTypes.array,
    bars: PropTypes.array,
    onMessageSocket: PropTypes.func,
    clearState: PropTypes.func,
    websocketUrl: PropTypes.string
  };

  componentDidMount() {
    // live graphs
    const {onMessageSocket, websocketUrl} = this.props;
    webSocket = new WebSocket(`${websocketUrl}/v1/global`);
    webSocket.onmessage = function(event) {
      const rec = JSON.parse(event.data);
      const rdata = rec.data;
      const rate = rec.rate;

      onMessageSocket({rdata: rdata, rate: rate});

    };
  }

  componentWillUnmount() {
    webSocket.close();
    this.props.clearState();
  }

  render() {
    const {rates, bars} = this.props;

    const rateChartData = {
      data: {
        x: 'x',
        columns: rates
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%H:%M:%S'
          },
          label: 'channel'
        },
        y: {
          label: 'messages'
        }
      },
      bindto: '#rateChart'
    };

    const barChartData = {
      data: {
        columns: bars,
        type: 'bar'
      },
      bar: {
        width: {
          ratio: 0.5
        }
      },
      axis: {
        x: {
          type: 'category',
          categories: ['EMAIL', 'SMS', 'PUSH NOTIFICATIONS'],
          label: 'channel'
        },
        y: {
          label: 'messages'
        }
      },
      bindto: '#barChart'
    };

    return (
      <div className="panel-body">


        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Sending rate</h5>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    <LineChart data={rateChartData} id="rateChart" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Message distribution</h5>
              </div>
              <div className="ibox-content">
                <div className="row">
                  <div className="col-lg-12">
                    <BarChart data={barChartData} id="barChart" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
