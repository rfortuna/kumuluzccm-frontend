/**
 * Created by urbanmarovt on 12/05/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {change as changeFormField} from 'redux-form';

import {TemplateNewForm} from 'components';

import {load as loadCampaigns} from 'redux/modules/models/campaigns/list';
import {create as createTemplate, showContentPreview, clearContentFile} from 'redux/modules/models/templates/new';

import {refactorCampaigns} from 'utils/dataSelectRefactor';
import {templateTypes} from 'utils/constants';

function fetchDataDeferred(getState, dispatch) {
  return dispatch(loadCampaigns({limit: 1000, offset: 0}));
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    allCampaigns: state.models.campaigns.list.data,
    templateFile: state.models.templates.new.templateFile,
    contentPreview: state.models.templates.new.contentPreview
  }),
  dispatch => bindActionCreators({...routerActions, createTemplate, showContentPreview, clearContentFile}, dispatch)
)
export default class CampaignNew extends Component {

  static propTypes = {
    allCampaigns: PropTypes.array,
    createTemplate: PropTypes.func,
    pushState: PropTypes.func,
    allTags: PropTypes.array,
    templateFile: PropTypes.object,
    contentPreview: PropTypes.string,
    showContentPreview: PropTypes.func
  };

  handleSubmit = (data) => {
    const {createTemplate, contentPreview} = this.props;
    createTemplate(data, contentPreview).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/app/tenant`);
      }
    } );
  };

  handleCancel = () => {
    this.props.pushState(null, '/app/tenant');
  };

  handleFileClear = () => {
    const {clearContentFile, changeFormField} = this.props;

    clearContentFile();
    changeFormField('TemplateForm', 'content', '');
  };

  render() {
    const {allCampaigns, showContentPreview, contentPreview, templateFile, clearContentFile} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New template</h2>
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <TemplateNewForm allCampaigns={refactorCampaigns(allCampaigns)} onSubmit={this.handleSubmit}
                                   handleCancel={this.handleCancel} showContentPreview={showContentPreview}
                                   contentPreview={contentPreview} templateFile={templateFile} handleFileClear={this.handleFileClear}
                                   templateTypes={templateTypes}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
