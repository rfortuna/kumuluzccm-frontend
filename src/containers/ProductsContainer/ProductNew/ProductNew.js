/**
 * Created by urbanmarovt on 13/05/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import {create as createProduct, clearState} from 'redux/modules/models/products/new';
import {ProductNewForm} from 'components';


@connect(
  state => ({
  }),
  dispatch => bindActionCreators({...routerActions, createProduct, clearState}, dispatch)
)
export default class ProductNew extends Component {

  static propTypes = {
    pushState: PropTypes.func,
    createProduct: PropTypes.func,
    clearState: PropTypes.func
  };

  componentWillUnmount() {
    this.props.clearState();
  }

  handleSubmit = (data) => {
    const {createProduct, pushState} = this.props;
    createProduct(data).then((res) => {
      // success
      if (res.error) {
        // handle error
        toastr.error('There was a problem uploading the CSV document.');
      } else {
        pushState(null, '/app/tenant/products');
      }
    } );
  };

  handleCancel = () => {
    this.props.pushState(null, '/app/tenant/products');
  };

  render() {

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New product</h2>
            <small>Create new product in order to send push notifications using KumuluzCCM.</small>
          </div>
          <div className="col-lg-2">
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ProductNewForm onSubmit={this.handleSubmit} handleCancel={this.handleCancel} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

