/**
 * Created by urbanmarovt on 13/05/16.
 */


import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';

import {ProductsGrid} from 'components';
import { load as loadProducts} from 'redux/modules/models/products/list';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(loadProducts()));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    products: state.models.products.list.data
  }),
  dispatch => bindActionCreators(
    {...routerActions}, dispatch)
)
export default class Campaigns extends Component {

  static propTypes = {
    products: PropTypes.array
  };

  onProductClick = (id) => {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/products/${id}/platforms`, '');
  };

  render() {
    const { products } = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Product platforms</h2>
          </div>
        </div>

        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ProductsGrid
                    products={products}
                    onProductClick={this.onProductClick}
                   />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
