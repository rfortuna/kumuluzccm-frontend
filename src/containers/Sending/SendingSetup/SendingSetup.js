/**
 * Created by urbanmarovt on 15/03/16.
 */

import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import connectData from 'helpers/connectData';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import {SendingSetupForm} from 'components';
import {load as loadCampaigns} from 'redux/modules/models/campaigns/list';
import {loadTimezones, areTimezonesLoaded} from 'redux/modules/models/constants';
import {saveSetup} from 'redux/modules/views/sending';
import {refactorCampaigns, refactorTimezones} from 'utils/dataSelectRefactor';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(loadCampaigns({limit: 100, offset: 0})));
  if (!areTimezonesLoaded(getState())) {
    promises.push(dispatch(loadTimezones({limit: 100, offset: 0})));
  }

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    setup: state.views.sending.setup,
    allCampaigns: state.models.campaigns.list.data,
    channelId: state.router.location.query.channelId,
    allTimezones: state.models.constants.timezones.data
  }),
  dispatch => bindActionCreators({...routerActions, saveSetup}, dispatch)
)
export default class SendingSetup extends Component {

  static propTypes = {
    setup: PropTypes.object,
    allCampaigns: PropTypes.array,
    saveSetup: PropTypes.func,
    pushState: PropTypes.func,
    channelId: PropTypes.string,
    allTimezones: PropTypes.array
  };

  handleForm = (data, errors) => {
    const {saveSetup} = this.props;

    saveSetup(data, errors);

  };

  handleNext = () => {

    const {channelId} = this.props;

    this.props.pushState(null, '/app/tenant/sending/channelSetup', {channelId: channelId});
  };

  render() {
    const {allCampaigns, allTimezones} = this.props;
    return (
      <div className="ibox float-e-margins">
        <div className="ibox-content p-md">
          <SendingSetupForm
            handleForm={this.handleForm}
            handleNext={this.handleNext}
            allCampaigns={refactorCampaigns(allCampaigns)}
            allTimezones={refactorTimezones(allTimezones)}
          />
        </div>
      </div>
    );
  }
}