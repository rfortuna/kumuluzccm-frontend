/**
 * Created by urbanmarovt on 15/03/16.
 */
import React, {Component, PropTypes} from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {FormControls, ButtonToolbar, Button, Glyphicon} from 'react-bootstrap';

import {DateTimeLabel, PlatformLabel, LabelsContainer, Iframe} from 'components';
import {loadById as loadCampaign} from 'redux/modules/models/campaigns/details';
import {load as loadProduct} from 'redux/modules/models/products/details';

import {create as createMessages} from 'redux/modules/models/messages/new';
import {create as createNewsletter} from 'redux/modules/models/newsletters/new';
import {showReceiversConfirm as showReceivers, hideReceiversConfirm as hideReceivers} from 'redux/modules/views/sending';
import {send as sendNewsletter} from 'redux/modules/models/newsletters/details';
import {load as loadTenant, isLoaded as isTenantLoaded} from 'redux/modules/models/tenants/details';

import {messageTypes} from 'utils/constants';


function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadCampaign(getState().views.sending.setup.data.campaign)));

  const messageType = messageTypes.find((type) => {
    return type.id === getState().router.location.query.channelId
  });

  if (messageType.push) {
    promises.push(dispatch(loadProduct(getState().views.sending.channelSetup.data.product)));
  }

  if (!isTenantLoaded(getState())) {
    promises.push(dispatch(loadTenant()));
  }

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    sending: state.views.sending,
    campaign: state.models.campaigns.details.data,
    channelId: state.router.location.query.channelId,
    product: state.models.products.details.data,
    tenant: state.models.tenants.details.data
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      createMessages,
      createNewsletter,
      showReceivers,
      hideReceivers,
      sendNewsletter
    }, dispatch)

)
export default class SendingConfirm extends Component {

  static propTypes = {
    sending: PropTypes.object,
    campaign: PropTypes.object,
    channelId: PropTypes.string,
    showReceivers: PropTypes.func,
    hideReceivers: PropTypes.func,
    createNewsletter: PropTypes.func,
    createMessages: PropTypes.func,
    pushState: PropTypes.func,
    sendNewsletter: PropTypes.func,
    tenant: PropTypes.object
  };

  handleSave = () => {
    const {createNewsletter, sending, channelId} = this.props;
    createNewsletter(sending, channelId).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {

        const {result: {body: {id: newsletterId, campaign: {id: campaignId}}}} = res;
        // handle success
        this.props.pushState(null, `/app/tenant/campaigns/${campaignId}/newsletters/${newsletterId}`);


      }
    }, (error) => {
      console.log(error);
    });
  };

  handleSend = () => {
    const {createNewsletter, sendNewsletter, sending, channelId} = this.props;
    createNewsletter(sending, channelId).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        const {result: {body: {id: newsletterId, campaign: {id: campaignId}}}} = res;

        // handle success
        sendNewsletter(campaignId, newsletterId).then(res => {
          if (res.error) {
            // handle error
          } else {
            this.props.pushState(null, `/app/tenant/campaigns/${campaignId}/newsletters/${newsletterId}`);
          }
        });

      }
    }, (error) => {
      console.log(error);
    });
  };

  handleCancel = () => {
    this.props.pushState(null, '/app/tenant');
  };

  render() {
    const {sending, campaign, channelId, product, tenant} = this.props;

    const messageType = messageTypes.find((type) => {
      return type.id === channelId
    });

    const contentShortField = (messageType.sms || messageType.push);
    const contentEMAILField = messageType.email;

    return (
      <div className="ibox float-e-margins">
        <div className="ibox-content p-md">
          <form className="form-horizontal">
            <FormControls.Static label="Channel type" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={messageTypes[channelId - 1].label} />
            {messageType.email &&
            <div>
              <FormControls.Static label="Sending email" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={sending.channelSetup.data.mailFrom} />
              <FormControls.Static label="Sending email name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={sending.channelSetup.data.mailUser} />
            </div>
            }
            {messageType.sms &&
            <div>
              <FormControls.Static label="Sending sms name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={tenant.smsUser} />
            </div>
            }
            {messageType.push &&
            <div>
              <FormControls.Static label="Product" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={product.name} />
              <LabelsContainer label="Platforms" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                {sending.channelSetup.productPlatforms.map((item) => {
                  // TODO import these info
                  if (sending.channelSetup.data[item.type]) {
                    return <PlatformLabel key={`platform_${item.type}`} platform={item}/>;
                  }
                })}
              </LabelsContainer>
            </div>
            }
            <div className="hr-line-dashed"></div>
            <FormControls.Static label="Campaign" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={campaign.name}/>
            <FormControls.Static label="Subject" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={sending.setup.data.subject}/>
            <FormControls.Static label="Delivery schedule" labelClassName="col-xs-2" wrapperClassName="col-xs-10"> <DateTimeLabel date={sending.setup.data.deliverAfter}/> </FormControls.Static>
            <div className="hr-line-dashed"></div>
            {sending.lists.data.receiverType === 'lists' &&
            <FormControls.Static label="Number of lists" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                <span>{`${sending.lists.data.receiverLists.split(',').length}`}</span>
            </FormControls.Static>
            }
            {sending.lists.data.receiverType !== 'lists' &&
            <FormControls.Static label="Receivers" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
              <span>{sending.lists.data.receiverType}</span>
            </FormControls.Static>
            }
            <FormControls.Static label="Ignore receivers preferences" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
              {!sending.setup.data.ignoreReceiversPrefs && <i className="fa fa-remove"/>}
              {sending.setup.data.ignoreReceiversPrefs && <i className="fa fa-check"/>}
            </FormControls.Static>
            <div className="hr-line-dashed"></div>
            {contentShortField &&
              <FormControls.Static label="Short content" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={sending.content.data.contentShort}/>
            }
            {contentEMAILField &&
              <div>
                <LabelsContainer label="Email content" labelClassName="col-xs-2"
                                 wrapperClassName="col-xs-10">
                  <Iframe content={sending.content.data.contentEMAIL} />
                </LabelsContainer>

              </div>
            }
            <div className="hr-line-dashed"></div>
            <div className="row">
              <div className="col-xs-offset-2 col-xs-10">
                <ButtonToolbar>
                  <Button onClick={() => this.handleSend()}><Glyphicon glyph="envelope" /> &nbsp;Send</Button>
                  <Button onClick={() => this.handleSave()}><Glyphicon glyph="save" /> &nbsp;Save</Button>
                  <Button onClick={() => this.handleCancel()}><Glyphicon glyph="remove" /> &nbsp;Cancel</Button>
                </ButtonToolbar>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
