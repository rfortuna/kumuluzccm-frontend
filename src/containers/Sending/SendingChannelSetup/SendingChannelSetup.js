/**
 * Created by urbanmarovt on 08/04/16.
 */

import React, {Component, PropTypes} from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';
import _ from 'underscore';

import { saveChannelSetup, updateChannelFields, addPushCustomProperty, removePushCustomProperty} from 'redux/modules/views/sending';
import {load as loadProducts} from 'redux/modules/models/products/list';
import {loadById as loadCampaign} from 'redux/modules/models/campaigns/details';
import {load as loadTenant, isLoaded as isTenantLoaded} from 'redux/modules/models/tenants/details';
import {loadPlatforms, clearPlatforms} from 'redux/modules/models/products/details';
import { SendingChannelSetupForm } from 'components';
import {refactorProducts} from 'utils/dataSelectRefactor';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(loadProducts()));

  const campaignId = getState().views.sending.setup.data.campaign;
  if (campaignId) {
    promises.push(dispatch(loadCampaign(campaignId)));
  }

  if (!isTenantLoaded(getState())) {
    promises.push(dispatch(loadTenant()));
  }

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    allProducts: state.models.products.list.data,
    channelId: state.router.location.query.channelId,
    campaign: state.models.campaigns.details.data,
    allPlatforms: state.models.products.details.dataPlatforms,
    fields: state.views.sending.channelSetup.fields,
    tenant: state.models.tenants.details.data,
    numOfCustomProperties: state.views.sending.channelSetup.numOfCustomProperties,
    pushCustomPropertiesString: state.views.sending.channelSetup.data.pushCustomPropertiesString,
  }),
  dispatch => bindActionCreators({
    ...routerActions, saveChannelSetup, loadPlatforms,
    updateChannelFields, clearPlatforms,
    addPushCustomProperty, removePushCustomProperty
  }, dispatch)
)
export default class SendingChannelSetup extends Component {

  static propTypes = {
    saveChannelSetup: PropTypes.func,
    pushState: PropTypes.func,
    channelId: PropTypes.string,
    allProducts: PropTypes.array,
    loadPlatforms: PropTypes.func,
    allPlatforms: PropTypes.array,
    updateChannelFields: PropTypes.func,
    campaign: PropTypes.object,
    addPushCustomProperty: PropTypes.func,
    removePushCustomProperty: PropTypes.func,
    numOfCustomProperties: PropTypes.number,
    pushCustomPropertiesString: PropTypes.string
  };

  componentWillUnmount() {
    const {fields: { }, saveReceivers} = this.props;
//    saveReceivers(receivers.value, receiversType.value );
  }

  handleNext = () => {
    this.props.pushState(null, '/app/tenant/sending/receivers', {channelId: this.props.channelId});
  };

  handleForm = (data, errors, fields) => {
    const {saveChannelSetup} = this.props;

    saveChannelSetup(data, errors, fields);
  };

  handleNotificationProductSelected = (value) => {

    const {loadPlatforms, updateChannelFields, clearPlatforms} = this.props;

    // clear if additional fields if value is null (select was cleared)
    if (!value || value.length == 0) {
      updateChannelFields([]);
      clearPlatforms();
      return;
    }


    loadPlatforms(value.value).then(res => {
      // update dynamic fields for notification product platform
      updateChannelFields(res.result.body);
    });

  };

  render() {
    const {allProducts, channelId, allPlatforms, campaign, fields,
      tenant, addPushCustomProperty, removePushCustomProperty,
      numOfCustomProperties, pushCustomPropertiesString} = this.props;
    return (
      <div className="ibox float-e-margins">
        <div className="ibox-content p-md">
          <SendingChannelSetupForm fields= {fields}
                                   handleNext={this.handleNext}
                                   handleForm={this.handleForm}
                                   allProducts={refactorProducts(allProducts)}
                                   channelId={channelId}
                                   campaign={campaign}
                                   handleNotificationProductSelected={this.handleNotificationProductSelected}
                                   tenant={tenant}
                                   addPushCustomProperty={addPushCustomProperty}
                                   removePushCustomProperty={removePushCustomProperty}
                                   numOfCustomProperties={numOfCustomProperties}
                                   pushCustomPropertiesString={pushCustomPropertiesString}
                                   />
        </div>
      </div>
    );
  }
}
