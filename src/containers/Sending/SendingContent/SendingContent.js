/**
 * Created by urbanmarovt on 15/03/16.
 */

import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import connectData from 'helpers/connectData';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import {change as changeFormField} from 'redux-form';

import {refactorTemplates} from 'utils/dataSelectRefactor';
import {refactorTemplatesToShort, refactorTemplatesToLong} from 'utils/refactor';
import {load as loadTemplates} from 'redux/modules/models/templates/list';
import {SendingContentForm} from 'components';

import {loadStatic as loadStaticFields, loadCustom as loadCustomFields} from 'redux/modules/models/lists/properties/list';
import {saveContent, showContentEMAILPreview, clearContentEMAIL, saveContentEMAILPreview} from 'redux/modules/views/sending';
import {showUploadImageModal, closeUploadImageModal} from 'redux/modules/views/modals';
import {upload as uploadNewImage, showImagePreview, clear as clearUploadImage} from 'redux/modules/models/tenants/images/new';
import {load as loadImages, addItem as addImageToTenantImages} from 'redux/modules/models/tenants/images/list';
import {messageTypes} from 'utils/constants';

const queryParams = {
  limit: 1000,
  offset: 0
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  const campaignId = getState().views.sending.setup.data.campaign;
  if (campaignId) {
    promises.push(dispatch(loadTemplates(queryParams.limit, queryParams.offset, campaignId)));
  }

  let receivers = getState().views.sending.lists.data;
  promises.push(dispatch(loadStaticFields()));
  if (receivers.receiverLists) {
      promises.push(dispatch(loadCustomFields(receivers.receiverLists.split(','))));
  }

  promises.push(dispatch(loadImages()));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    setup: state.views.sending.setup,
    channelId: state.router.location.query.channelId,
    contentEMAILPreview: state.views.sending.content.contentEMAILPreview,
    templateFile: state.views.sending.content.templateFile,
    campaignTemplates: state.models.templates.list.data,
    staticFields: state.models.lists.properties.list.staticFields,
    customFields: state.models.lists.properties.list.customFields,
    receivers: state.views.sending.lists.data,
    tenantImages: state.models.tenants.images.list.data
  }),
  dispatch => bindActionCreators({...routerActions, saveContent, showContentEMAILPreview,
    clearContentEMAIL, changeFormField, saveContentEMAILPreview, addImageToTenantImages,
    showUploadImageModal, closeUploadImageModal, uploadNewImage, showImagePreview, clearUploadImage}, dispatch)
)
export default class SendingContent extends Component {

  static propTypes = {
    setup: PropTypes.object,
    allCampaigns: PropTypes.array,
    saveSetup: PropTypes.func,
    pushState: PropTypes.func,
    channelId: PropTypes.string,
    saveContent: PropTypes.func,
    showContentEMAILPreview: PropTypes.func,
    contentEMAILPreview: PropTypes.string,
    templateFile: PropTypes.object,
    clearContentEMAIL: PropTypes.func,
    changeFormField: PropTypes.func,
    campaignTemplates: PropTypes.array,
    allowedProperties: PropTypes.array,
    showUploadImageModal: PropTypes.func,
    closeUploadImageModal: PropTypes.func,
    uploadNewImage: PropTypes.func,
    showImagePreview: PropTypes.func,
    clearUploadImage: PropTypes.func,
    staticFields: PropTypes.array,
    customFields: PropTypes.array,
    saveContentEMAILPreview: PropTypes.func,
    receivers: PropTypes.object,
    tenantImages: PropTypes.array,
    addImageToTenantImages: PropTypes.func
  };

  handleForm = (data, errors) => {

    const {saveContent} = this.props;
    saveContent(data, errors);
  };

  handleNext = () => {
    const {channelId} = this.props;

    this.props.pushState(null, '/app/tenant/sending/confirm', {channelId: channelId});
  };

  handleFileClear = () => {
    const {clearContentEMAIL, changeFormField} = this.props;

    clearContentEMAIL();
    changeFormField('ContentForm', 'contentEMAIL', null);
    changeFormField('ContentForm', 'contentEMAILPreview', null);
  };

  saveContentEMAIL = (content) => {
    const {saveContentEMAILPreview, changeFormField} = this.props;
    saveContentEMAILPreview(content);
    changeFormField('ContentForm', 'contentEMAILPreview', content);
  }

  render() {

    const {channelId, showContentEMAILPreview, templateFile, contentEMAILPreview,
      campaignTemplates, staticFields, customFields, togglePreview, changeFormField,
      receivers, showUploadImageModal, closeUploadImageModal,
      showImagePreview, uploadNewImage, clearUploadImage, tenantImages, addImageToTenantImages} = this.props;

    let allowedProperties = staticFields;
    if (receivers && receivers.receiverType  === 'lists') {
      allowedProperties = [...allowedProperties, ...customFields];
    }

    const messageType = messageTypes.find((type) => {
      return type.id === channelId
    });

    return (
      <div className="ibox float-e-margins">
        <div className="ibox-content p-md">
          <SendingContentForm handleNext={this.handleNext} handleForm={this.handleForm}
                              isSMS={messageType.sms}
                              isPUSH={messageType.push}
                              isEMAIL={messageType.email}
                              showContentEMAILPreview={showContentEMAILPreview}
                              contentEMAILPreview={contentEMAILPreview}
                              templateFile={templateFile}
                              handleFileClear={this.handleFileClear}
                              campaignTemplatesShort={refactorTemplates(refactorTemplatesToShort(campaignTemplates))}
                              campaignTemplatesLong={refactorTemplates(refactorTemplatesToLong(campaignTemplates))}
                              allowedProperties={allowedProperties}
                              changeFieldValue={changeFormField}
                              saveContentEMAILPreview={this.saveContentEMAIL}
                              showUploadImageModal={showUploadImageModal}
                              closeUploadImageModal={closeUploadImageModal}
                              showImagePreview={showImagePreview}
                              uploadNewImage={uploadNewImage}
                              clearUploadImage={clearUploadImage}
                              tenantImages={tenantImages}
                              addImageToTenantImages={addImageToTenantImages}/>
        </div>
      </div>
    );
  }
}
