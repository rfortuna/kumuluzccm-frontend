/**
 * Created by rokfortuna on 15/03/16.
 */

import React, {Component, PropTypes} from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm} from 'redux-form';
import _ from 'underscore';

import {saveReceiversData, showContentPreview } from 'redux/modules/views/sending';
import { SendingReceiverForm } from 'components';

import {loadByStatus as loadListsByStatus} from 'redux/modules/models/lists/list';
import styles from './SendingReceivers.scss';
import { MultiSelect, SimpleSelect} from 'components';
import {refactorLists} from 'utils/dataSelectRefactor';

const queryParams = {
  limit: 100,
  offset: 0
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(loadListsByStatus(queryParams, 'IMPORTED')));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    allLists: state.models.lists.list.data,
    channelId: state.router.location.query.channelId
}),
  dispatch => bindActionCreators({...routerActions, saveReceiversData}, dispatch)
)
export default class SendingReceivers extends Component {

  static propTypes = {
    saveLists: PropTypes.func,
    pushState: PropTypes.func,
    channelId: PropTypes.string,
    errors: PropTypes.object,
    allLists: PropTypes.array,
  };


  handleNext = () => {
    this.props.pushState(null, '/app/tenant/sending/content', {channelId: this.props.channelId});
  };

  handleForm = (formData) => {
    const {saveReceiversData} = this.props;
    saveReceiversData(formData);
  };

  render() {

    const {allLists, channelId} = this.props;
    return (
      <div className="ibox float-e-margins">
        <div className="ibox-content p-md">
          <SendingReceiverForm handleNext={this.handleNext} handleForm={this.handleForm}
                               allLists={refactorLists(allLists)} channelId={channelId} />
        </div>
      </div>
    );
  }
}