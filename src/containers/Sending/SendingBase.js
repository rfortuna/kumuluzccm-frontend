import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';

import {SendingFlowTag} from 'components';
import _ from 'underscore';
import {messageTypes} from 'utils/constants';


@connect(
  state => ({
    sending: state.views.sending,
    channelId: state.router.location.query.channelId
  })
)
export default class SendingBase extends Component {

  static propTypes = {
    sending: PropTypes.object,
    channelId: PropTypes.string,
    location: PropTypes.object,
    children: PropTypes.object
  };

  render() {

    const {sending, channelId, location: {pathname}} = this.props;

    const messageType = _.findWhere(messageTypes, {id: channelId});

    return (
      <div>
        <div className="row border-bottom white-bg dashboard-header">
          <div className="row">
            { messageType &&
              <SendingFlowTag stepInfo={{touched: true, errors: {}, icon: messageType.icon, label: messageType.label}}/>
            }
            <Link to="/app/tenant/sending/setup" query={{channelId: channelId}}>
              <SendingFlowTag stepInfo={{...sending.setup, icon: 'fa-book', label: 'Setup', active: pathname === '/app/tenant/sending/setup'}} />
            </Link>
            <Link to="/app/tenant/sending/channelSetup" query={{channelId: channelId}}>
              <SendingFlowTag stepInfo={{...sending.channelSetup, icon: 'fa-wrench', label: 'Channel setup', active: pathname === '/app/tenant/sending/channelSetup'}} />
            </Link>
            <Link to="/app/tenant/sending/receivers" query={{channelId: channelId}}>
              <SendingFlowTag stepInfo={{...sending.lists, icon: 'fa-users', label: 'Recipients', active: pathname === '/app/tenant/sending/receivers'}} />
            </Link>
            <Link to="/app/tenant/sending/content" query={{channelId: channelId}}>
              <SendingFlowTag stepInfo={{...sending.content, icon: 'fa-file-text-o', label: 'Content', active: pathname === '/app/tenant/sending/content'}} />
            </Link>
            <Link to="/app/tenant/sending/confirm" query={{channelId: channelId}}>
              <SendingFlowTag stepInfo={{...sending.confirm, icon: 'fa-send-o', label: 'Confirm', active: pathname === '/app/tenant/sending/confirm'}} />
            </Link>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="wrapper wrapper-content">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
