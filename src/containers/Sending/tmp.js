/**
 * Created by urbanmarovt on 11/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {load as loadReceivers} from 'redux/modules/models/receivers/list';
import {load as loadCampaigns} from 'redux/modules/models/campaigns/list';
import {refactorReceivers, refactorCampaigns} from 'utils/dataSelectRefactor';

import {create as createMessages} from 'redux/modules/models/messages/new';
import {create as createNewsletter} from 'redux/modules/models/newsletters/new';


import {MessageNewForm} from 'components';

const allChannels = [{label: 'SMS', value: 'SMS'}, {label: 'EMAIL', value: 'EMAIL'}, {label: 'PUSH_NOTIFICATION', value: 'PUSH_NOTIFICATION'}];

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  promises.push(dispatch(loadReceivers()));

  promises.push(dispatch(loadCampaigns(1000, 0)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    allReceivers: state.models.receivers.list.data,
    allCampaigns: state.models.campaigns.list.data
  }),
  dispatch => bindActionCreators({...routerActions, createMessages, createNewsletter}, dispatch)
)
export default class MessageNew extends Component {

  static propTypes = {
    allReceivers: PropTypes.array,
    allCampaigns: PropTypes.array,
    createNewsletter: PropTypes.func,
    createMessages: PropTypes.func,
    pushState: PropTypes.func,
    initialize: PropTypes.func
  };

  handleSubmit = (data) => {
    const {createPackage, createMessages} = this.props;
    createPackage(data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // post messages on created package id
        createMessages(data, res.result.body.id).then((res) => {
          if (res.error) {
            // handle error
          } else {
            // handle success
            this.props.pushState(null, `/`);
          }
        });
      }
    }, (error) => {
      console.log(error);
    });
  };

  handleCancel = () => {
    this.props.pushState(null, '/');
  };

  render() {
    const {allReceivers, allCampaigns} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New message</h2>
          </div>
          <div className="col-lg-2">

          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                <MessageNewForm onSubmit={this.handleSubmit} handleCancel={this.handleCancel}
                                allReceivers={refactorReceivers(allReceivers)} allCampaigns={refactorCampaigns(allCampaigns)}
                                allChannels={allChannels}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}