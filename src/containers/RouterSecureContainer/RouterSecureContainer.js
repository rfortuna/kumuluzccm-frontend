/**
 * Created by urbanmarovt on 07/04/16.
 */
import React, {Component, PropTypes} from 'react';

export default class RouterSecureContainer extends Component {

  static propTypes = {
    children: PropTypes.object
  };

  render() {
    return this.props.children;
  }
}
