import React, { Component, PropTypes } from 'react';
import DocumentMeta from 'react-document-meta';
import config from '../../config';
import ReduxToastr from 'react-redux-toastr';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./App.scss');

    return (
      <div className={styles.app}>
        <DocumentMeta {...config.app}/>
        {this.props.children}
        <ReduxToastr
          timeOut={1000}
          newestOnTop={false}
          position="top-right"/>
      </div>
    );
  }
}
