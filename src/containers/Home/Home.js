import React, { Component } from 'react';
import config from '../../config';

export default class Home extends Component {
  render() {
    const styles = require('./Home.scss');
    // require the logo image both from client and server
//    const logoImage = require('./logo.png');
    return (
      <div className={styles.home}>
        <div className={styles.masthead}>
          <div className="container">
            <div className={styles.logo}>
            </div>
            <h1>{config.app.title}</h1>

            <h2>{config.app.description}</h2>
            <p className={styles.humility}>
              Created by Sunesis.
            </p>
          </div>
        </div>

        <div className="container">

          Boilerplate.
        </div>
      </div>
    );
  }
}
