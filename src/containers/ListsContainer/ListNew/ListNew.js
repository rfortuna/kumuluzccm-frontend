/**
 * Created by rokfortuna on 22/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {toastr} from 'react-redux-toastr';

import {create as createList, showCSVPreview, clearState} from 'redux/modules/models/lists/new';
import {ListNewForm} from 'components';


@connect(
  state => ({
    csvPreview: state.models.lists.new.csvPreview,
    csvFile: state.models.lists.new.csvFile
  }),
  dispatch => bindActionCreators({...routerActions, createList, showCSVPreview, clearState }, dispatch)
)
export default class ListNew extends Component {

  static propTypes = {
    pushState: PropTypes.func,
    createList: PropTypes.func,
    showCSVPreview: PropTypes.func,
    csvPreview: PropTypes.array,
    csvFile: PropTypes.object
  };

  componentWillUnmount() {
    this.props.clearState();
  }

  handleSubmit = (data) => {
    const {createList, pushState} = this.props;
    createList(data.name, data.csvData[0]).then((res) => {
      console.log(res);
      // success
      if (res.error) {
        // handle error
        toastr.error('There was a problem uploading the CSV document.');
      } else {
        pushState(null, `/app/tenant/lists/${res.result.body.id}`);
      }
    } );
  }

  handleCancel = () => {
    this.props.pushState(null, '/app/tenant');
  }

  render() {

    const {csvFile, csvPreview, showCSVPreview} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New list</h2>
            <small>Segment your receivers using lists.</small>
          </div>
          <div className="col-lg-2">
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ListNewForm
                    csvFile={csvFile}
                    csvPreview={csvPreview}
                    onSubmit={this.handleSubmit}
                    handleCancel={this.handleCancel}
                    showCSVPreview={showCSVPreview}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
