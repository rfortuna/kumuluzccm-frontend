/**
 * Created by rokfortuna on 3/10/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {reduxForm, change as changeFieldValue} from 'redux-form';
import {Input} from 'react-bootstrap';

import { getPagesCount, getActivePage, getOffset} from 'utils/pagination';
import {getQueryParams, toggleOrder} from 'utils/queryParams';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';
import {loadByStatus as loadListByStatus} from 'redux/modules/models/lists/list';
import {ListsGrid, ButtonGroupSelect} from 'components';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  order: '',
  searchString: ''
}

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  let queryParams = getQueryParams(getState().router, defaultQueryParams);
  promises.push(dispatch(loadListByStatus(queryParams, 'IMPORTED')));
  promises.push(dispatch(setInitialStateQueryParams('/lists', queryParams)));
  return Promise.all(promises);
}

const importStatusOptions = [
  {label: 'Imported', value: 'IMPORTED'},
  {label: 'Import in progress', value: 'IN_PROGRESS'}];

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    lists: state.models.lists.list,
    queryParams: state.queryParams
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      loadListByStatus,
      onPageChange,
      onSearchStringChange,
      onOrderChange,
      setInitialStateQueryParams,
      changeFieldValue
    }, dispatch)
)
@reduxForm({
  form: 'ListSearchForm',
  fields: ['searchString', 'importStatus']
},
  state => ({
    initialValues: {
      importStatus: 'IMPORTED'
    }
  }))
export default class Lists extends Component {

  static propTypes = {
    lists: PropTypes.object,
    queryParams: PropTypes.object
  };

  onListClick = (id) => {
    const {pushState} = this.props;
    pushState(null, `/app/tenant/lists/${id}`, '');
  };

  onPageChangeClick = (error, selectedPage) => {
    const { loadListByStatus, queryParams, onPageChange, fields: {importStatus} } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange("/app/tenant/lists", newQueryParams);
    loadListByStatus(newQueryParams, importStatus.value);
  };

  onFormStateChange = (field, fieldValue) => {
    const {  loadListByStatus, queryParams, onSearchStringChange, fields: {importStatus}  } = this.props;
    let newQueryParams = {...queryParams};
    newQueryParams = {
      ...newQueryParams,
      offset: 0
    };
    if (field.name === 'searchString') {
      newQueryParams.searchString = fieldValue;
      loadListByStatus(newQueryParams, importStatus.value);
      onSearchStringChange("/app/tenant/lists", newQueryParams);
    }
    if (field.name === 'importStatus') {
      loadListByStatus(newQueryParams, fieldValue);
    }
  }

  makeOnChangeInput = (field) => {
    return (event) => {
      field.onChange(event);
      this.onFormStateChange(field, event.target.value);
    };
  };

  onSortClick = (field)=> {
//    const { loadLists, queryParams, onOrderChange } = this.props;
//    const newQueryParams = {
//      ...queryParams,
//      order: toggleOrder(field, queryParams.order),
//      limit: defaultQueryParams.limit,
//      offset:0
//    };
//    onOrderChange("/lists", newQueryParams);
//    loadLists(newQueryParams);
  }

  render() {
    const { fields: {searchString, importStatus}, lists, queryParams, changeFieldValue } = this.props;

    const {limit, offset} = queryParams;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Lists</h2>
          </div>
          <div className="col-lg-2"></div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight p-b-none">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <form className="form-horizontal">
                    <Input type="text" label="Search by name" {...searchString}
                           labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                           onChange={this.makeOnChangeInput(searchString)}/>
                    <div className="row">
                      <label className="col-sm-2 control-label">Status</label>
                      <div className="col-sm-10">
                        <ButtonGroupSelect formName="ListSearchForm" options={importStatusOptions} field={importStatus}
                                           changeFieldValue={changeFieldValue} onFormStateChange={this.onFormStateChange}/>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight p-b-none">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ListsGrid
                    lists={lists.data}
                    onListClick={this.onListClick}
                    paginationOnPageChange={this.onPageChangeClick}
                    paginationPages={getPagesCount(lists.count, limit )}
                    paginationActivePage={getActivePage(limit, offset)}
                    onSortClick={this.onSortClick}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
