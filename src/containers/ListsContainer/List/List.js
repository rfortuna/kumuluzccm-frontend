/**
 * Created by rokfortuna on 3/10/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import {FormControls, Button, ButtonToolbar, Glyphicon, Input} from 'react-bootstrap';
import {reduxForm, change as changeFormField, reset as resetForm} from 'redux-form';
import {toastr} from 'react-redux-toastr';

import {
  load as loadList,
  update as updateList,
  edit as editList,
  editCancel as editCancelList,
} from 'redux/modules/models/lists/details';
import {load as loadListReceivers, loadNotOnList as loadListReceiversNotOnList} from 'redux/modules/models/lists/receivers/list';
import {
  ListForm, ListGrid, ListImportStatusLabel, LabelsContainer,
  ListAddNewReceiverForm, ButtonGroupSelect, Tabs, ReceiversGrid,
  AddExistingReceiverModal
} from 'components';
import {refactorListData} from 'utils/refactor';
import {
  onPageChange,
  onSearchStringChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';
import {getQueryParams} from 'utils/queryParams';
import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import { loadCustom as loadCustomFields, loadDetailedCustom} from 'redux/modules/models/lists/properties/list';
import {addNewReceiver, addExistingReceiver, removeReceiver} from 'redux/modules/models/lists/receivers/actions';
import {showAddExistingReceiverModal, closeAddExistingReceiverModal} from 'redux/modules/views/modals';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  searchString: ''
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  let queryParams = getQueryParams(getState().router, defaultQueryParams);
  promises.push(dispatch(setInitialStateQueryParams('', queryParams)));
  promises.push(dispatch(loadList(getState().router.params.listId)));
  promises.push(dispatch(loadListReceivers(getState().router.params.listId, defaultQueryParams)));
  promises.push(dispatch(loadListReceiversNotOnList(getState().router.params.listId, queryParams)));
  promises.push(dispatch(loadCustomFields([getState().router.params.listId])));
  promises.push(dispatch(loadDetailedCustom(getState().router.params.listId)));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    listId: state.router.params.listId,
    editing: state.models.lists.details.editing,
    queryParams: state.queryParams,
    list: state.models.lists.details,
    listReceivers: state.models.lists.receivers.list.receivers,
    receiversNotOnList: state.models.lists.receivers.list.receiversNotOnList,
    staticFieldsCamelCase: state.models.lists.properties.list.staticFieldsCamelCase,
    customFields: state.models.lists.properties.list.customFields,
    detailedCustomFields: state.models.lists.properties.list.detailedCustomFields,
    addExistingReceiverModal: state.views.modals.addExistingReceiverModal
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      resetForm,
      editList,
      editCancelList,
      updateList,
      onPageChange,
      onSearchStringChange,
      loadList,
      loadListReceivers,
      loadListReceiversNotOnList,
      changeFormField,
      addNewReceiver,
      addExistingReceiver,
      setInitialStateQueryParams,
      removeReceiver,
      showAddExistingReceiverModal,
      closeAddExistingReceiverModal
    }, dispatch)
)
@reduxForm({
    form: 'ListGridForm',
    fields: ['searchString', 'searchOrAdd', 'addType', 'serachStringNotOnList']
  },
  state => ({
    initialValues: {
      searchOrAdd: 'search',
      addType: 'new'
    }
  }))
export default class List extends Component {

  static propTypes = {
    name: PropTypes.string,
    editing: PropTypes.bool,
    editList: PropTypes.func,
    editCancelList: PropTypes.func,
    updateList: PropTypes.func,
    listId: PropTypes.string,
    queryParams: PropTypes.object,
    loadList: PropTypes.func,
    loadListReceivers: PropTypes.func,
    loadListReceiversNotOnList: PropTypes.func,
    list: PropTypes.object,
    searchOrAdd: PropTypes.string,
    addType: PropTypes.string,
    staticFieldsCamelCase: PropTypes.array,
    customFields: PropTypes.array,
    detailedCustomFields: PropTypes.array,
    addNewReceiver: PropTypes.func,
    listReceivers: PropTypes.object,
    receiversNotOnList: PropTypes.object,
    setInitialStateQueryParams: PropTypes.func,
    showAddExistingReceiverModal: PropTypes.func,
    closeAddExistingReceiverModal: PropTypes.func
  };


  handleSubmit = (data) => {
    const {listId, updateList} = this.props;
    updateList(listId, data)
      .then(res => {if (! _.has(res, 'error') ) window.location.reload(); });
  };

  onPageChangeClick = (error, selectedPage) => {
    const { listId, loadListReceivers, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange(`/app/tenant/lists/${listId}`, newQueryParams);
    loadListReceivers(listId, newQueryParams);
  };

  onPageChangeClickNotOnList = (error, selectedPage) => {
    const { listId, loadListReceiversNotOnList, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange(`/app/tenant/lists/${listId}`, newQueryParams);
    loadListReceiversNotOnList(listId, newQueryParams);
  };

  onFormStateChange = (field, fieldValue) => {

    const { listId, loadListReceivers, loadListReceiversNotOnList, queryParams, onSearchStringChange } = this.props;


    const newQueryParams = {
      ...queryParams,
      searchString: fieldValue,
      offset: 0
    };
    if (field.name === 'searchString') {
      loadListReceivers(listId, newQueryParams);
    }

    if (field.name === 'serachStringNotOnList') {
      loadListReceiversNotOnList(listId, newQueryParams);
    }
    onSearchStringChange(`/app/tenant/lists/${listId}`, newQueryParams);

  }

  makeOnChangeInput = (field) => {
    return (event) => {
      field.onChange(event);
      this.onFormStateChange(field, event.target.value);
    };
  };

  handleOnReceiverClick = (receiverId) => {
    this.props.pushState(null, `/app/tenant/receivers/${receiverId}`);
  };

  handleAddNewReceiver = (data) => {
    const {detailedCustomFields, addNewReceiver, listId, resetForm, loadListReceivers, onPageChange} = this.props;
    addNewReceiver(listId, data, detailedCustomFields).then((res) => {
      // success
      if (res.error) {
        // handle error
        toastr.error(`Error: ${res.error.description}`, {timeOut: 5000});
      } else {
        toastr.success('Receiver successfully added!', {timeOut: 3000});
        loadListReceivers(listId, defaultQueryParams);
        onPageChange(`/app/tenant/lists/${listId}`, defaultQueryParams);
        resetForm('ListAddNewReceiverForm');
      }
    });
  }

  handleAddExistingReceiver = (data) => {
    const {detailedCustomFields, addExistingReceiver, listId,
      resetForm, loadListReceiversNotOnList, onPageChange, addExistingReceiverModal, closeAddExistingReceiverModal} = this.props;
    addExistingReceiver(listId, addExistingReceiverModal.receiver.id , data, detailedCustomFields).then((res) => {
      // success
      if (res.error) {
        // handle error
        toastr.error(`Error: ${res.error.description}`, {timeOut: 5000});
      } else {
        toastr.success('Receiver successfully added!', {timeOut: 3000});
        loadListReceiversNotOnList(listId, defaultQueryParams);
        closeAddExistingReceiverModal();
        // resetForm('AddExistingReceiverForm');
      }
    });
  }

  onTabChange = () => {
    const {setInitialStateQueryParams, listId, loadListReceiversNotOnList, loadListReceivers } = this.props;
    setInitialStateQueryParams(`/app/tenant/lists/${listId}`, defaultQueryParams);
    loadListReceiversNotOnList(listId, defaultQueryParams);
    loadListReceivers(listId, defaultQueryParams);
  }

  newReceiverActions = (receiver) => {

    const {listId, removeReceiver, showAddExistingReceiverModal} = this.props;

    return (
      <span>
        <ButtonToolbar>
          <Button type="button" onClick={(event)=> {
            event.stopPropagation();
            console.log('add user');
            showAddExistingReceiverModal(receiver);
          }}>
            <Glyphicon glyph="plus" />&nbsp;Add
          </Button>
        </ButtonToolbar>
      </span>
    );
  }

  existingReceiverActions = (receiver) => {
    const {
      listId, removeReceiver, setInitialStateQueryParams, loadListReceivers,
      showAddExistingReceiverModal, closeAddExistingReceiverModal } = this.props;
    return (
      <span>
        <ButtonToolbar>
          <Button type="button" onClick={
            (event) => {
              event.stopPropagation();
              toastr.confirm(`Are you sure you want to remove ${receiver.firstName} ${receiver.lastName} from the list?`,
                {
                  onOk: () => {
                    removeReceiver(listId, receiver.id).then((res) => {
                      if (res.error) {
                        toastr.error(`Error: ${res.error.description}`, {timeOut: 5000});
                      } else {
                        toastr.success('Receiver removed.');
                        loadListReceivers(listId, defaultQueryParams);
                        setInitialStateQueryParams(`/app/tenant/lists/${listId}`, defaultQueryParams);
                      }
                    });
                  }
                });
            }}>
            <Glyphicon glyph="minus" />&nbsp;Remove
          </Button>
        </ButtonToolbar>
      </span>
    );
  }

  render() {

    const {list, editing, editList, editCancelList,
      fields: {searchString, searchOrAdd, addType, serachStringNotOnList}, changeFormField,
      staticFieldsCamelCase, customFields, listReceivers, receiversNotOnList, pushState,
      addExistingReceiverModal, closeAddExistingReceiverModal} = this.props;
    const {limit, offset} = this.props.queryParams;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Basic info</h5>
              </div>
              <div className="ibox-content">
                { !editing &&
                  <form className="form-horizontal">
                    <FormControls.Static label="Name" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={list.name}/>
                    <LabelsContainer  label="Import status" labelClassName="col-xs-2" wrapperClassName="col-xs-10">
                      <ListImportStatusLabel status={list.importStatus} linesProcessed={list.linesProcessed} linesTotal={list.linesTotal} />
                    </LabelsContainer>
                    <FormControls.Static label="Total receivers" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={listReceivers.count} />
                    <div className="hr-line-dashed"></div>
                    <div className="row">
                      <div className="col-xs-offset-2 col-xs-10">
                        <ButtonToolbar>
                          <Button onClick={editList}><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                        </ButtonToolbar>
                      </div>
                    </div>
                  </form>
                }
                {editing &&
                  <ListForm onSubmit={this.handleSubmit} handleCancel={editCancelList} receiverCount={listReceivers.count}/>
                }
              </div>
            </div>

            <Tabs options={[{label: 'Browse receivers', value: 'search'}, {label: 'Add receivers', value: 'add'}]} field={searchOrAdd} formName={'ListGridForm'} changeFieldValue={changeFormField} onTabChange={this.onTabChange}>
              <div className="panel-body">
                  {searchOrAdd.value === 'add' &&
                    <span>
                      <LabelsContainer label="" labelClassName="col-xs-0"
                                           wrapperClassName="col-xs-12">
                       <ButtonGroupSelect options={[{label: 'New receiver', value: 'new'}, {label: 'Existing receiver', value: 'existing'}]} field={addType} formName={'ListGridForm'} changeFieldValue={changeFormField}/>
                      </LabelsContainer>
                      <div className="row">
                        <div className="col-xs-12" style={{marginBottom: '100px'}}>
                          {addType.value === 'new' &&
                              <ListAddNewReceiverForm changeFormField={changeFormField} onSubmit={this.handleAddNewReceiver} fields={[...staticFieldsCamelCase, ...customFields]} />
                          }
                          {addType.value === 'existing' &&
                            <span>
                              <br/>
                              <div className="row">
                                <form className="form-horizontal" style={{paddingLeft: '10px'}}>
                                    <Input type="text" label="Search receivers" {...serachStringNotOnList}
                                         labelClassName="col-xs-2" wrapperClassName="col-xs-8"
                                         onChange={this.makeOnChangeInput(serachStringNotOnList)}/> {/* onChange must be on last place*/}

                                </form>
                              </div>
                              <br/>
                              <ReceiversGrid
                              receivers={receiversNotOnList.data}
                              paginationPages={getPagesCount(receiversNotOnList.count, limit)}
                              paginationOnPageChange={this.onPageChangeClickNotOnList}
                              paginationActivePage={getActivePage( limit, offset)}
                              onReceiverClick={(id) => { pushState(null, `/app/tenant/receivers/${id}`, '')}}
                              actions={this.newReceiverActions} />
                              <AddExistingReceiverModal
                                addExistingReceiverModalVisible={addExistingReceiverModal.visible}
                                closeAddExistingReceiverModal={closeAddExistingReceiverModal}
                                fields={[ ...customFields]}
                                onSubmit={this.handleAddExistingReceiver} />
                            </span>
                          }
                        </div>
                      </div>
                    </span>
                  }
                  {
                    searchOrAdd.value === 'search' &&
                  <span>
                    <br/>
                    <div className="row">
                      <form className="form-horizontal" style={{paddingLeft: '10px'}}>
                          <Input type="text" label="Search by email" {...searchString}
                               labelClassName="col-xs-2 text-align-right" wrapperClassName="col-xs-10"
                               onChange={this.makeOnChangeInput(searchString)}/> {/* onChange must be on last place*/}

                      </form>
                    </div>
                    <br/>

                    <ListGrid
                      data={refactorListData(listReceivers.data)}
                      paginationPages={getPagesCount(listReceivers.count, limit)}
                      paginationOnPageChange={this.onPageChangeClick}
                      paginationActivePage={getActivePage( limit, offset)}
                      handleOnReceiverClick={this.handleOnReceiverClick}
                      actions={this.existingReceiverActions}
                    />
                  </span>
                  }
              </div>
            </Tabs>
          </div>
        </div>
      </div>
    );
  }
}
