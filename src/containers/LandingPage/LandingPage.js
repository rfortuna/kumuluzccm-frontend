/**
 * Created by rokfortuna on 4/12/16.
 */

import React, { Component, PropTypes } from 'react';

import {connect} from 'react-redux';

@connect(
  state => ({
    token: state.authentication.token,
  })
)
export default class LandingPage extends Component {

  render() {

    const {token} = this.props;

    return (
      <div>
        <link rel="stylesheet" type="text/css" href="landing/LandingPage.css"/>
        <div className="navbar-wrapper">
          <nav className="navbar navbar-default navbar-fixed-top navbar-scroll" role="navigation">
            <div className="container">
              <div className="navbar-header page-scroll">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <a href="#">
                  <img src="/logo_dark.png" style={{height: 60}} />
                </a>
              </div>
              <div id="navbar" className="navbar-collapse collapse">
                <ul className="nav navbar-nav navbar-right">
                  <li><a className="page-scroll" href="#features"
                         style={{backgroundColor: 'transparent !important'}}>Features</a>
                  </li>
                  <li><a className="page-scroll" href="#contact"
                         style={{backgroundColor: 'transparent !important'}}>Contact</a>
                  </li>
                  {!token &&
                  <li><a className="page-scroll" href="/login"
                         style={{backgroundColor: 'transparent !important'}}>login</a>
                  </li> ||
                  <li><a className="page-scroll" href="/app/tenant"
                         style={{backgroundColor: 'transparent !important'}}>Dashboard</a>
                  </li>
                  }
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <div className="carousel carousel-fade" >
            <div className="carousel-inner" role="listbox">
            <div className="item active">
            <div className="container">
            <div className="carousel-caption">
              <h1>Engage your customers,<br/>
                simpler.
              </h1>
            </div>
          <div className="carousel-image wow zoomIn">
            <img src="landing/laptop.png" alt="laptop"/>
          </div>
            </div>
          <div className="header-back one"></div>
            </div>
            </div>
        </div>

          <section id="features" className="container services">
          <div className="row">
          <div className="col-sm-3">
          <h2>Multiple channels</h2>
        <p>KumuluzCCM supports sending messages to your customers via multiple channels.</p>
        </div>
        <div className="col-sm-3">
          <h2>Reach masses</h2>
          <p>Our system is highly scalable and therefore capable of sending millions of messages with a single click.</p>
        </div>
        <div className="col-sm-3">
          <h2>Dashboard & analytics</h2>
        <p>Monitor the success of your customer interactions using our analytic features.</p>
        </div>
        <div className="col-sm-3">
          <h2>Manage lists</h2>
        <p>Organize your customers into meaningfull groups and use them to target different segments.</p>
        </div>
        </div>
        </section>

    <section  className="container features">
      <div className="row">
      <div className="col-lg-12 text-center">
      <div className="navy-line"></div>
      <h1>Communication channels</h1>
    <p>We support all major communication channels.</p>
  </div>
  </div>
    <div className="row">
      <div className="col-md-3 text-center wow fadeInLeft">
        <div>
          <i className="fa fa-envelope features-icon"></i>
          <h2>Email</h2>
          <p>Send email messages with personalized conent and template of your choice.</p>
        </div>
        <div>
          <i className="fa fa-comments features-icon"></i>
          <h2>SMS</h2>
          <p>Send SMS to anyone, anywhere and notify them of important matters.</p>
        </div>
          </div>
          <div className="col-md-6 text-center  wow zoomIn">
          <img src="landing/perspective.png" alt="dashboard" className="img-responsive" style={{height: '300px', margin: 'auto'}}/>
          </div>
          <div className="col-md-3 text-center wow fadeInRight">
            <div>
              <i className="fa fa-mobile features-icon"></i>
              <h2>Push notifications</h2>
              <p>Send push notifications through your app using our system, simplify your process and enhance your oppurtunities.</p>
            </div>
            <div className="m-t-lg">
              <i className="fa fa-star features-icon"></i>
              <h2>Customer preferred channels</h2>
              <p>Make your customers comfort a top priority and communicate via their own preferred channels.</p>
            </div>
          </div>
          </div>
  </section>
        <section className="timeline">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="navy-line"></div>
              <h1>Communication workflow</h1>
              <p>Sending notifications is easy.</p>
            </div>
          </div>
    <div className="row features-block">

      <div className="col-lg-12">
        <div id="vertical-timeline" className="vertical-container light-timeline center-orientation" style={{marginTop: '0px !important'}}>
          <div className="vertical-timeline-block">
            <div className="vertical-timeline-icon navy-bg">
              <i className="fa fa-road"></i>
            </div>
            <div className="vertical-timeline-content">
              <h2>Communication channel</h2>
              <p>Email, SMS, push notification or preferred channel?</p>
            </div>
          </div>

          <div className="vertical-timeline-block">
            <div className="vertical-timeline-icon navy-bg">
              <i className="fa fa-book"></i>
            </div>
            <div className="vertical-timeline-content">
              <h2>Message details</h2>
              <p>Specify message subject, time of delivery etc.</p>
            </div>
          </div>

          <div className="vertical-timeline-block">
            <div className="vertical-timeline-icon navy-bg">
              <i className="fa fa-file-text-o"></i>
            </div>
            <div className="vertical-timeline-content">
              <h2>Message content</h2>
              <p>Write or import message content from file.</p>
            </div>
          </div>

          <div className="vertical-timeline-block">
            <div className="vertical-timeline-icon navy-bg">
              <i className="fa fa-send-o"></i>
            </div>
            <div className="vertical-timeline-content">
              <h2>Send</h2>
              <p>Preview and send the message.</p>
            </div>
          </div>

          <div className="vertical-timeline-block">
              <div className="vertical-timeline-icon navy-bg">
                  <i className="fa fa-area-chart"></i>
              </div>

              <div className="vertical-timeline-content">
                  <h2>Monitor</h2>
                  <p>
                    Monitor your communication and view statistics that matter.
                  </p>
              </div>
          </div>

          </div>

          </div>
          </div>
          </div>
          </section>

              <section id="contact" className="gray-section contact" style={{marginTop: '0px !important'}}>
              <div className="container">
              <div className="row m-b-lg">
              <div className="col-lg-12 text-center">
              <div className="navy-line"></div>
              <h1>Contact Us</h1>
              <p>Take your enterprise applications to another level.</p>
              </div>
              </div>
              <div className="row m-b-lg">
              <div className="col-lg-3 col-lg-offset-3">
              <address>
              <strong><span className="navy">Sunesis</span></strong><br/>
              Šmartinska cesta 152 <br/>
              1000 Ljubljana<br/>
          </address>
        </div>
        <div className="col-lg-4">
          <p className="text-color">
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12 text-center">
          <a href="mailto:info@sunesis.si" className="btn btn-primary">Send us mail</a>
          <p className="m-t-sm">
            Or follow us on social platform
          </p>
          <ul className="list-inline social-icon">
            <li><a href="#"><i className="fa fa-twitter"></i></a>
              </li>
              <li><a href="#"><i className="fa fa-facebook"></i></a>
              </li>
              <li><a href="#"><i className="fa fa-linkedin"></i></a>
              </li>
              </ul>
              </div>
              </div>
              <div className="row">
              <div className="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
              <p><strong>&copy; 2016 Sunesis</strong><br/></p>
              </div>
              </div>
              </div>
              </section>
        </div>
    );
  }
}
