/**
 * Created by rokfortuna on 3/24/16.
 */
import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import cookie from 'react-cookie';

import styles from './Login.scss';
import {login} from 'redux/modules/authentication';
import {LoginForm} from 'components';
import {load as loadUser, setProfileValues} from 'redux/modules/global/profile';
import {toastr} from 'react-redux-toastr';

@connect(
  state => ({
    authenticationData: state.authentication
  }),
  dispatch => bindActionCreators({...routerActions, login , loadUser, setProfileValues}, dispatch)
)
export default class Login extends Component {

  static propTypes = {
    authenticationData: PropTypes.object,
    login: PropTypes.func,
    loadUser: PropTypes.func,
    pushState: PropTypes.func,
    setProfileValues: PropTypes.func
  };


  handleSubmit = (data) => {
    const {login, loadUser, pushState, setProfileValues} = this.props;

    login(data.username, data.password).then((res) => {
      if (res.error) {
        toastr.error(res.error.error_description);
        return;
      }
      return loadUser(res.result.body.access_token);
    }).then((res) => {
      const user = res.result.body;
      // if user has a tenant, redirect him to tenant page
      if (user.tenants.length > 0) {
        cookie.save('profile_type', 'tenant', { path: '/' });
        cookie.save('profile_id', user.tenants[0].id, { path: '/' });
        return setProfileValues('tenant', user.tenants[0].id);
      } else {
        cookie.save('profile_type', 'user', {path: '/'});
        cookie.save('profile_id', res.result.body.id, {path: '/'});
        pushState(null,'/app/user/tenants');
        return null;
      }
    }).then((res) => {
      if (res) {
        console.log('redirect to tenant');
        pushState(null,'/app/tenant');
      }
    });
  };

  render() {
    const {authenticationData} = this.props;

    return (
      <div className="middle-box text-center loginscreen animated fadeInDown">
        <div>
          <div>
            <img src="/logo.png" className={styles.logo}/>
          </div>
          <div className={styles.loginText}>
            <h3>Welcome to KumuluzCCM.</h3>
            <p>The only Customer Communication Management you will ever need.
            </p>
            <p>Login to see it in action.</p>
          </div>
          <LoginForm onSubmit={this.handleSubmit} authenticationData={authenticationData} />
          <p className="m-t"> <small>KumuluzEE &copy; 2014-2016</small> </p>
        </div>
      </div>
    );
  }
}
