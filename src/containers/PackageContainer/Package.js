/**
 * Created by urbanmarovt on 10/02/16.
 */

import React, { Component, PropTypes } from 'react';
import * as routerActions from 'redux-router';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FormControls} from 'react-bootstrap';
import {reduxForm} from 'redux-form';
import {Input} from 'react-bootstrap';

import {
  load as loadPackage,
  loadMessages as loadPackageMessages
} from 'redux/modules/models/newsletters/details';
import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';
import {MessagesGrid} from 'components';
import {getQueryParams, toggleOrder} from 'utils/queryParams';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  order: 'deliveredAt ASC',
  searchString: ''
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];


  const queryParams = getQueryParams(getState().router, defaultQueryParams);
  const packageId = getState().router.params.packageId;


  promises.push(dispatch(setInitialStateQueryParams(packageId, queryParams)));
  promises.push(dispatch(loadPackage(packageId)));
  promises.push(dispatch(loadPackageMessages(packageId, queryParams)));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    pcg: state.models.packages.details,
    router: state.router,
    queryParams: state.queryParams
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      loadPackageMessages,
      onPageChange,
      onSearchStringChange,
      onOrderChange
    }, dispatch)
)
@reduxForm({
  form: 'messageSearch',
  fields: ['searchString']
})
export default class Package extends Component {

  static propTypes = {
    pcg: PropTypes.object,
    router: PropTypes.object,
    loadPackageMessages: PropTypes.func,
    pushState: PropTypes.func.isRequired,
    queryParams: PropTypes.object,
    onPageChange: PropTypes.func,
    onSearchStringChange: PropTypes.func,
    onOrderChange: PropTypes.func,
    fields: PropTypes.object
  }


  onMessageClick = (id) => {
    this.props.pushState(null, `/messages/${id}`);
  }

  onPageChangeClick = (e, selectedPage) => {
    const { pcg, loadPackageMessages, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange(pcg.package.id, newQueryParams);
    loadPackageMessages(pcg.package.id, newQueryParams);
  }

  onSearchFormStateChange = (field, fieldValue) => {
    if (field.name === 'searchString') {
      const { pcg, loadPackageMessages, queryParams, onSearchStringChange } = this.props;
      const newQueryParams = {
        ...queryParams,
        searchString: fieldValue,
        limit: defaultQueryParams.limit,
        offset: 0
      };
      onSearchStringChange(pcg.package.id, newQueryParams);
      loadPackageMessages(pcg.package.id, newQueryParams);
    }
  }

  onSortClick = (field) => {
    const { pcg, loadPackageMessages, queryParams, onOrderChange } = this.props;
    const newQueryParams = {
      ...queryParams,
      order: toggleOrder(field, queryParams.order),
      limit: defaultQueryParams.limit,
      offset: 0
    };
    onOrderChange(pcg.package.id, newQueryParams);
    loadPackageMessages(pcg.package.id, newQueryParams);
  }

  searchOnChangeUser = (field) => {
    return (event) => {
      field.onChange(event);
      this.onSearchFormStateChange(field, event.target.value);
    };
  }

  render() {
    const { pcg } = this.props;

    const {limit, offset} = this.props.queryParams;

    const {fields: { searchString}} = this.props;

    return (
      <div>
        <div className="panel-body">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Package details</h5>
                </div>
                <div className="ibox-content p-md">
                  <form className="form-horizontal">
                    <FormControls.Static label="Referent" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={pcg.package.referent}/>
                    <FormControls.Static label="Total messages" labelClassName="col-xs-2" wrapperClassName="col-xs-10" value={pcg.package.totalCount}/>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-title">
                  <h5>Messages</h5>
                </div>
                <div className="ibox-content p-md">
                  <form className="form-horizontal">
                    <Input type="text" label="Search by receiver" {...searchString}
                           labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                           onChange={this.searchOnChangeUser(searchString)}/>
                  </form>
                  <br/>
                  <MessagesGrid
                    messages={pcg.messages.data}
                    onMessageClick={this.onMessageClick}
                    paginationOnPageChange={this.onPageChangeClick}
                    paginationPages={getPagesCount(pcg.messages.totalCount, limit)}
                    paginationActivePage={getActivePage( limit, offset)}
                    onSortClick={this.onSortClick}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


