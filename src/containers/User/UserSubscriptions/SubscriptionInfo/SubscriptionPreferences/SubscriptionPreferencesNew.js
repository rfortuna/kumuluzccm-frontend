/**
 * Created by urbanmarovt on 09/02/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {ReceiverPrefsForm} from 'components';

import {create as createSubscriptionPreferences} from 'redux/modules/models/subscriptions/details/preferences/details';

@connect(
  state => ({}),
  dispatch => bindActionCreators({...routerActions, createSubscriptionPreferences}, dispatch)
)
export default class SubscriptionPreferencesNew extends Component {

  static propTypes = {
    createReceiverPrefs: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    params: PropTypes.object,
    location: PropTypes.object,
    createReceiverPreferences: PropTypes.func
  };

  handleSubmit = (data) => {
    const {params: {subscriptionId}, createSubscriptionPreferences} = this.props;
    createSubscriptionPreferences(subscriptionId, data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/app/user/subscriptions/${subscriptionId}`);
      }
    });
  }

  handleCancel = () => {
    const {params: {subscriptionId}, pushState} = this.props;

    pushState(null, `/app/user/subscriptions/${subscriptionId}`);
  };

  render() {
    const {location: {query: {channelType}}} = this.props;

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New receiver preferences</h2>
          </div>
          <div className="col-lg-2">

          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ReceiverPrefsForm initialValues={{channelType: channelType}} onSubmit={this.handleSubmit}
                                     handleCancel={this.handleCancel}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
