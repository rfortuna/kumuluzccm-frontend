/**
 * Created by urbanmarovt on 09/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {ReceiverPrefsForm} from 'components';

import {
  load as loadSubscriptionPreferences,
  update as updateSubscriptionPreferences} from 'redux/modules/models/subscriptions/details/preferences/details';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  const state = getState();

  promises.push(dispatch(loadSubscriptionPreferences(state.router.params.subscriptionId, state.router.params.preferenceId)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    preference: state.models.subscriptions.details.preferences.details.data
  }),
  dispatch => bindActionCreators({...routerActions, updateSubscriptionPreferences}, dispatch)
)
export default class SubscriptionPreferencesUpdate extends Component {

  static propTypes = {
    updateSubscriptionPreferences: PropTypes.func.isRequired,
    preference: PropTypes.object,
    params: PropTypes.object,
    pushState: PropTypes.func
  };

  handleSubmit = (data) => {
    const {params: {subscriptionId, preferenceId}, preference: {priority}, updateSubscriptionPreferences} = this.props;
    updateSubscriptionPreferences(subscriptionId, preferenceId, priority, data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/app/user/subscriptions/${subscriptionId}`);
      }
    });
  };

  handleCancel = () => {
    const {params: {subscriptionId}, pushState} = this.props;

    pushState(null, `/app/user/subscriptions/${subscriptionId}`);
  };

  render() {
    const {preference} = this.props;

    const initialValues = {
      channelType: preference.channelType,
      from: preference.receiveFrom,
      to: preference.receiveTo,
      disabled: preference.channelDisabled,
      ...preference.receiveDays
    };

    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>Update receiver preferences</h2>
          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ReceiverPrefsForm initialValues={ initialValues } onSubmit={this.handleSubmit}
                                     handleCancel={this.handleCancel}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
