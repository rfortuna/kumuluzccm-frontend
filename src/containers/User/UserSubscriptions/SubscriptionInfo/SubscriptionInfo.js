/**
 * Created by urbanmarovt on 31/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import {load as loadSubscription,
  clearState as clearSubscriptionDetails
} from 'redux/modules/models/subscriptions/details/baseInfo'

import {load as loadPreferences} from 'redux/modules/models/subscriptions/details/preferences/list'
import {updatePriority as updateSubscriptionPrefsPriority} from 'redux/modules/models/subscriptions/details/preferences/details'

import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import {ReceiverForm, ReceiverPrefs} from 'components';
import {refactorSubscriptionPrefs} from 'utils/refactor';

import styles from './SubscriptionInfo.scss';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  const state = getState();

  promises.push(dispatch(loadSubscription(state.router.params.subscriptionId)));
  promises.push(dispatch(loadPreferences(state.router.params.subscriptionId)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    subscription: state.models.subscriptions.details.baseInfo.data,
    preferences: state.models.subscriptions.details.preferences.list.data
  }),
  dispatch => bindActionCreators({...routerActions, clearSubscriptionDetails, updateSubscriptionPrefsPriority}, dispatch)
)
export default class SubscriptionInfo extends Component {

  static propTypes = {
    subscription: PropTypes.object,
    preferences: PropTypes.array
  };

  componentWillUnmount() {
    const {clearSubscriptionDetails} = this.props;

    clearSubscriptionDetails();
  }

  onPrefsCreateClick = (channelType) => {
    const {pushState, params: {subscriptionId}} = this.props;

    pushState(null, `/app/user/subscriptions/${subscriptionId}/preferences/new`, {channelType});
  };

  onPrefsUpdateClick = (prefId) => {
    const {pushState, params: {subscriptionId}} = this.props;

    pushState(null, `/app/user/subscriptions/${subscriptionId}/preferences/${prefId}`);
  };

  handleCancel = () => {
    console.log('CANCEL FORM');
  };

  handlePrefsSort = (sortData) => {
    const {updateSubscriptionPrefsPriority, params: {subscriptionId}} = this.props;

    const priority = 3;

    sortData.forEach((channelPrefs, index) => {
      if (channelPrefs.notDefined) {
        return;
      }
      updateSubscriptionPrefsPriority(subscriptionId, channelPrefs.id, priority - index, channelPrefs);
    });
  };

  render() {
    const {subscription, preferences} = this.props;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <img src={subscription.tenant.logo} style={{height: 20, padding: '0 10px'}} /> <span>{subscription.tenant.name}</span>
              </div>
              <div className="ibox-content p-md">
                <div>
                  <form className="form-horizontal">
                    <FormControls.Static label="Email" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{subscription.email}</FormControls.Static>
                    <FormControls.Static label="Mobile phone" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{subscription.mobilePhone}</FormControls.Static>
                    <div className="hr-line-dashed"></div>
                    <div className="row">
                      <div className="col-sm-2">
                        <label className={styles.label}>Channel preferences</label>
                      </div>
                      <div className="col-sm-10">
                        <ReceiverPrefs receiverPrefs={refactorSubscriptionPrefs(preferences)}
                                       onCreateClick={this.onPrefsCreateClick}
                                       onUpdateClick={this.onPrefsUpdateClick}
                                       handleSort={this.handlePrefsSort}/>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
