/**
 * Created by urbanmarovt on 31/03/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import {load as loadSubscriptions} from 'redux/modules/models/subscriptions/list';

import {SubscriptionsGrid} from 'components';

import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';

import {getQueryParams} from 'utils/queryParams';

const defaultQueryParams = {
  limit: 10,
  offset: 0
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  let queryParams = getQueryParams(getState().router, defaultQueryParams);

  promises.push(dispatch(loadSubscriptions(queryParams.limit, queryParams.offset)));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    subscriptionsTotalCount: state.models.subscriptions.list.count,
    subscriptions: state.models.subscriptions.list.data
  }),
  dispatch => bindActionCreators({...routerActions}, dispatch)
)
export default class UserSubscriptions extends Component {

  static propTypes = {
    subscriptions: PropTypes.array,
    pushState: PropTypes.func,
    subscriptionsTotalCount: PropTypes.number
  };

  handleOnSubscriptionClick = (subscriptionId) => {
    const {pushState} = this.props;

    pushState(null, `/app/user/subscriptions/${subscriptionId}`);
  };


  render() {
    const {subscriptions, subscriptionsTotalCount} = this.props;

    const {limit, offset} = {limit: 10, offset: 10};

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
               <SubscriptionsGrid subscriptions={subscriptions}
                                  onSubscriptionClick={this.handleOnSubscriptionClick}
                                  paginationOnPageChange={this.onPageChangeClick}
                                  paginationPages={getPagesCount(subscriptionsTotalCount, limit)}
                                  paginationActivePage={getActivePage(limit, offset)}/>

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
