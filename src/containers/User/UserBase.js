/**
 * Created by urbanmarovt on 31/03/16.
 */
import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';


export default class SendingBase extends Component {

  static propTypes = {
    location: PropTypes.object,
    pathname: PropTypes.string,
    children: PropTypes.object
  };

  render() {

    const {location: {pathname}} = this.props;

    return (
      <div className="wrapper wrapper-content">
        <div className="tabs-container">
          <ul role="tablist" className="nav nav-tabs">
            <li role="presentation" className={pathname === `/app/user/tenants` && 'active'}>
              <Link to={`/app/user/tenants`}>My tenants</Link>
            </li>
            <li role="presentation" className={pathname === `/app/user/subscriptions` && 'active'}>
              <Link to={`/app/user/subscriptions`}>My subscriptions</Link>
            </li>
            <li role="presentation" className={pathname === `/app/user/admin` && 'active'}>
              <Link to={`/app/user/admin`}>Admin</Link>
            </li>
          </ul>
          <div className="tab-content">
            <div role="tabpanel" className="tab-pane active">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
