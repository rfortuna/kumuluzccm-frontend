/**
 * Created by urbanmarovt on 31/03/16.
 */
import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import {reset as resetForm} from 'redux-form';

import {TenantForm, TenantsGrid} from 'components';
import {showLogoPreview, create as createTenant, clear as clearNewTenantFormState} from 'redux/modules/models/tenants/new';
import {load as loadTenants, deleteTenant} from 'redux/modules/models/tenants/list';
import {toastr} from 'react-redux-toastr';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  // TODO remomve hardcoded limit offset
  promises.push(dispatch(loadTenants(100,0)))
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    deletedTenant: state.models.tenants.list.deletedTenant,
    tenants: state.models.tenants.list.data,
    logoFile: state.models.tenants.new.logoFile

  }),
  dispatch => bindActionCreators(
    {...routerActions,
      showLogoPreview,
      createTenant,
      resetForm,
      clearNewTenantFormState,
      deleteTenant
    }, dispatch)
)
export default class UserTenants extends Component {

  static propTypes = {
    showLogoPreview: PropTypes.func,
    createTenant: PropTypes.func,
    resetForm: PropTypes.func,
    clearNewTenantFormState: PropTypes.func,
    deletedTenant: PropTypes.bool,
    deleteTenant: PropTypes.func,
    tenants: PropTypes.array,
    pushState: PropTypes.func,
    logoFile: PropTypes.object
  };

  componentWillReceiveProps(nextProps) {
    const {deletedTenant} = this.props;
    if (!deletedTenant && nextProps.deletedTenant) {
      window.location.reload();
    }
  }

  handleSubmit = (data) => {
    this.props.createTenant(data).then((res) => {
      // success
      if (res.error) {
        // handle error
        toastr.error('There was an error creating new tenant.');
      } else {
        // handle success
        window.location.reload();
      }
    });
  }

  handleFormReset = (formName) => {
    const {resetForm, clearNewTenantFormState} = this.props;
    clearNewTenantFormState();
    resetForm(formName);
  }

  handleTenantDelete = (id) => {

    this.props.deleteTenant(id).then((res) => {
      if (res.error) {
        toastr.error('There was a problem deleting the tenant.');
      } else {
        window.location.reload();
      }
    } );
  }

  onTenantClick = (id) => {

  }


  render() {
    const {showLogoPreview, tenants, logoFile} = this.props;

    const mocktenants = [
      {id: 1, name: 'Abanka', logo: 'http://odpiralnicasi.com/photos/011/089/Abanka-logo-big.jpg'},
      {id: 2, name: 'Mercator', logo: 'http://www.mercatorgroup.si/assets/Logotip-pokoncen/mercator-logotip-rdec-negative-pokoncen.png'}
    ];

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-sm-6">
            <h2>Tenants</h2>
            <TenantsGrid tenants={tenants} onTenantClick={this.onTenantClick} />
          </div>
          <div className="col-sm-6">
            <h2>Add a new Tenant </h2>
            <TenantForm showLogoPreview={showLogoPreview} onSubmit={this.handleSubmit} handleResetForm={this.handleFormReset} logoFile={logoFile} />
          </div>
        </div>

      </div>
    );
  }

}
