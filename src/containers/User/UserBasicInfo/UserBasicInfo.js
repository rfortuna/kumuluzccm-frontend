/**
 * Created by urbanmarovt on 31/03/16.
 */
import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import {ReceiverForm, ReceiverPrefs} from 'components';

import styles from './UserBasicInfo.scss';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({

  }),
  dispatch => bindActionCreators({...routerActions}, dispatch)
)
export default class UserBasicInfo extends Component {

  static propTypes = {

  };

  render() {
    const {} = this.props;

    const receiver = {
      editing: false,
      data : {
        firstName: "Joze",
        lastName: "Novak",
        email: "joze.novak@gmail.com",
        mobilePhone: "+38631775533"
      }
    };

    const receiverPrefs = [
      {
        channelType: "SMS",
        receiveFrom: 1459424220193,
        receiveTo: 1459428220193,
        receiveDays: "1111111",
        channelDisabled: false
      },
      {
        channelType: "EMAIL",
        receiveFrom: 1459424220193,
        receiveTo: 1459428220193,
        receiveDays: "1111111",
        channelDisabled: false
      },
      {
        channelType: "PUSH NOTIFICATION",
        receiveFrom: 1459424220193,
        receiveTo: 1459428220193,
        receiveDays: "1111111",
        channelDisabled: false
      }
    ];

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-content p-md">
                <div>
                  {!receiver.editing &&
                  <form className="form-horizontal">
                    <FormControls.Static label="First name" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.firstName}</FormControls.Static>
                    <FormControls.Static label="Last name" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.lastName}</FormControls.Static>
                    <FormControls.Static label="Email" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.email}</FormControls.Static>
                    <FormControls.Static label="Mobile phone" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.mobilePhone}</FormControls.Static>
                    <div className="row">
                      <div className="col-xs-offset-2 col-xs-10">
                        <ButtonToolbar>
                          <Button><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                        </ButtonToolbar>
                      </div>
                    </div>
                    <div className="hr-line-dashed"></div>
                    <div className="row">
                      <div className="col-sm-2">
                        <label className={styles.label}>Channel preferences</label>
                      </div>
                      <div className="col-sm-10">
                        <ReceiverPrefs receiverPrefs={receiverPrefs}
                                       onCreateClick={this.onPrefsCreateClick}
                                       onUpdateClick={this.onPrefsUpdateClick}
                                       handleSort={this.handlePrefsSort}/>
                      </div>
                    </div>
                  </form>
                  }
                  {receiver.editing &&
                  <ReceiverForm onSubmit={this.handleSubmit} handleCancel={cancelEditingReceiver}/>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
