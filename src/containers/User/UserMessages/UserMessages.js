/**
 * Created by urbanmarovt on 31/03/16.
 */
/**
 * Created by urbanmarovt on 31/03/16.
 */
import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({

  }),
  dispatch => bindActionCreators({...routerActions}, dispatch)
)
export default class UserMessages extends Component {

  static propTypes = {

  };

  render() {
    const {} = this.props;

    return (
      <div className="panel-body">
        MESSAGES
      </div>
    );
  }

}
