/**
 * Created by rokfortuna on 01/06/16.
 */
import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as routerActions from 'redux-router';
import {toastr} from 'react-redux-toastr';
import {Table, Pagination, Button, Glyphicon } from 'react-bootstrap';

import {loadAdminTenants, handleRequestTenantSMSUser} from 'redux/modules/models/tenants/list';
import { getPagesCount, getActivePage, getOffset} from 'utils/pagination';
import {getQueryParams} from 'utils/queryParams';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';
import styles from './UserAdmin.scss';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  searchString: ''
}

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  // TODO remomve hardcoded limit offset
  let queryParams = getQueryParams(getState().router, defaultQueryParams);
  promises.push(dispatch(loadAdminTenants(queryParams, 'PENDING', '')));
  promises.push(dispatch(setInitialStateQueryParams('/user/admin', queryParams)));
  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    tenantsPendingSMSUser: state.models.tenants.list,
    queryParams: state.queryParams
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      onPageChange,
      onSearchStringChange,
      loadAdminTenants,
      handleRequestTenantSMSUser
    }, dispatch)
)
export default class UserAdmin extends Component {

  static propTypes = {
    tenantsPendingSMSUser: PropTypes.object,
    queryParams: PropTypes.object,
    handleRequestTenantSMSUser: PropTypes.func
  };

  onHandleRequestTenantSMSUser = (id, status) => {
    const {handleRequestTenantSMSUser, queryParams, loadAdminTenants} = this.props;
    return () => {
      handleRequestTenantSMSUser(id, status).then(
        (res) => {
          loadAdminTenants(queryParams, 'PENDING', '');
        }
      )
    }
  }

  onPageChangeClick = (error, selectedPage) => {
    const { loadAdminTenants, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange("/app/user/admin", newQueryParams);
    loadAdminTenants(newQueryParams, 'PENDING', '');
  };

  render() {

    const {tenantsPendingSMSUser, queryParams} = this.props;
    const {limit, offset} = queryParams;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-sm-12">
            <h2>Confirm pending SMS name requests</h2>
            <div className={styles.table}>
              <Table>
                <thead>
                <tr>
                  <th>
                    <strong>Tenant name</strong>
                  </th>
                  <th>
                    <strong>Current SMS user</strong>
                  </th>
                  <th>
                    <strong>Pending SMS user</strong>
                  </th>
                  <th>
                    <strong>Actions</strong>
                  </th>
                </tr>
                </thead>
                <tbody>
                { tenantsPendingSMSUser.data.map(
                  (tenant) =>
                    <tr key={tenant.id}>
                      <td>{tenant.name}</td>
                      <td>{tenant.smsUser}</td>
                      <td>{tenant.smsUserPending}</td>
                      <td>
                        <Button type="button" onClick={this.onHandleRequestTenantSMSUser(tenant.id, 'APPROVED')}>
                          <Glyphicon glyph="ok-circle" />&nbsp;Accept
                        </Button> &nbsp;
                        <Button type="button" onClick={this.onHandleRequestTenantSMSUser(tenant.id, 'REJECTED')}>
                          <Glyphicon glyph="remove-circle" />&nbsp;Reject
                        </Button>
                      </td>
                    </tr>
                )}
                </tbody>
              </Table>
              <div className={styles.pagination}>
                <Pagination
                  prev
                  next
                  first
                  last
                  ellipsis
                  boundaryLinks
                  items={getPagesCount(tenantsPendingSMSUser.count, limit )}
                  maxButtons={10}
                  activePage={getActivePage(limit, offset)}
                  onSelect={this.onPageChangeClick} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
