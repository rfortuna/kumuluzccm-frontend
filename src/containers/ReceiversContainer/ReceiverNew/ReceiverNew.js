/**
 * Created by urbanmarovt on 09/02/16.
 */

import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import {ReceiverForm} from 'components';
import {create as createReceiver} from 'redux/modules/models/receivers/new';

@connect(
  state => ({}),
  dispatch => bindActionCreators({...routerActions, createReceiver}, dispatch)
)
export default class ReceiverNew extends Component {

  static propTypes = {
    createReceiver: PropTypes.func.isRequired,
    pushState: PropTypes.func
  };

  handleSubmit = (data) => {
    const {createReceiver} = this.props;
    createReceiver(data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        this.props.pushState(null, `/app/tenant/receivers/${res.result.body.id}`);
      }
    } );
  };

  handleCancel = () => {
    this.props.pushState(null, `/app/tenant/receivers`);
  };

  render() {
    return (
      <div>
        <div className="row wrapper border-bottom white-bg page-heading">
          <div className="col-lg-10">
            <h2>New receiver</h2>
          </div>
          <div className="col-lg-2">

          </div>
        </div>
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
            <div className="col-lg-12">
              <div className="ibox float-e-margins">
                <div className="ibox-content p-md">
                  <ReceiverForm onSubmit={this.handleSubmit} handleCancel={this.handleCancel}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
