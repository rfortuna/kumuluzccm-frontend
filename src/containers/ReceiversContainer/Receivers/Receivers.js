/**
 * Created by urbanmarovt on 19/02/16.
 */
/**
 * Created by urbanmarovt on 19/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import {load as loadReceivers} from 'redux/modules/models/receivers/list';
import {Input} from 'react-bootstrap';
import {ReceiversGrid} from 'components';

import {reduxForm} from 'redux-form';

import {getOffset, getPagesCount, getActivePage} from 'utils/pagination';

import {getQueryParams, toggleOrder} from 'utils/queryParams';
import {
  onPageChange,
  onSearchStringChange,
  onOrderChange,
  setInitialState as setInitialStateQueryParams} from 'redux/modules/queryParams';

const defaultQueryParams = {
  limit: 10,
  offset: 0,
  order: 'lastName',
  searchString: ''
};

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  const queryParams = getQueryParams(getState().router, defaultQueryParams);
  promises.push(dispatch(setInitialStateQueryParams('/app/tenant/receivers', queryParams)));
  promises.push(dispatch(loadReceivers(queryParams)));
  return Promise.all(promises);
}

const readQueryParams = (router) => {
  const {searchString} = getQueryParams(router);

  return {searchString: searchString};
};

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    receivers: state.models.receivers.list.data,
    receiversTotalCount: state.models.receivers.list.count,
    router: state.router,
    queryParams: state.queryParams
  }),
  dispatch => bindActionCreators(
    {...routerActions,
      loadReceivers,
      onPageChange,
      onSearchStringChange,
      onOrderChange
    }, dispatch)
)
@reduxForm({
  form: 'ReceiversFilter',
  fields: ['searchString']
},
state => ({
  initialValues: readQueryParams(state.router)
}))
export default class Receivers extends Component {

  static propTypes = {
    history: PropTypes.object,
    receivers: PropTypes.array,
    router: PropTypes.object,
    loadReceivers: PropTypes.func,
    fields: PropTypes.object,
    receiversTotalCount: PropTypes.number,
    pushState: PropTypes.func,
    queryParams: PropTypes.object,
    onPageChange: PropTypes.func,
    onSearchStringChange: PropTypes.func,
    onOrderChange: PropTypes.func
  };

  onReceiverClick = (id) => {
    const {history: { pushState }} = this.props;
    pushState(null, `/app/tenant/receivers/${id}`, '');
  };

  onPageChangeClick = (e, selectedPage) => {
    const { loadReceivers, queryParams, onPageChange } = this.props;
    const newQueryParams = {...queryParams, offset: getOffset(selectedPage.eventKey, queryParams.limit) };
    onPageChange('/app/tenant/receivers', newQueryParams);
    loadReceivers(newQueryParams);
  };

  onFormStateChange = (fieldValue) => {

    const { loadReceivers, queryParams, onSearchStringChange } = this.props;
    const newQueryParams = {
      ...queryParams,
      searchString: fieldValue,
      limit: defaultQueryParams.limit,
      offset: 0
    };
    onSearchStringChange('/app/tenant/receivers', newQueryParams);
    loadReceivers( newQueryParams);
  };

  onSortClick = (field)=> {
    const { loadReceivers, queryParams, onOrderChange } = this.props;
    const newQueryParams = {
      ...queryParams,
      order: toggleOrder(field, queryParams.order),
      limit: defaultQueryParams.limit,
      offset: 0
    };
    onOrderChange('/app/tenant/receivers', newQueryParams);
    loadReceivers( newQueryParams);
  }

  makeOnChangeInput = (field) => {
    return (event) => {
      field.onChange(event);
      this.onFormStateChange(event.target.value);
    };
  };

  render() {
    const {fields: {searchString}, receivers, receiversTotalCount} = this.props;

    const {limit, offset} = this.props.queryParams;

    return (
        <div>
          <div className="row wrapper border-bottom white-bg page-heading">
            <div className="col-lg-10">
              <h2>Receivers</h2>
            </div>
          </div>
          <div className="wrapper wrapper-content animated fadeInRight p-b-none">
            <div className="row">
              <div className="col-lg-12">
                <div className="ibox float-e-margins">
                  <div className="ibox-content p-md">
                    <form className="form-horizontal">
                      <Input type="text" label="Search by email" {...searchString}
                             labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                             onChange={this.makeOnChangeInput(searchString)}/> {/* onChange must be on last place*/}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="wrapper wrapper-content animated fadeInRight p-t-none">
            <div className="row">
              <div className="col-lg-12">
                <div className="ibox float-e-margins">
                  <div className="ibox-content p-md">
                    <ReceiversGrid
                      receivers={receivers}
                      onReceiverClick={this.onReceiverClick}
                      paginationOnPageChange={this.onPageChangeClick}
                      paginationPages={getPagesCount(receiversTotalCount, limit)}
                      paginationActivePage={getActivePage(limit, offset)}
                      onSortClick={this.onSortClick}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

    );
  }
}
