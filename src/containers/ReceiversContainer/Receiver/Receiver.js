/**
 * Created by urbanmarovt on 19/02/16.
 */

import React, { Component, PropTypes } from 'react';
import connectData from 'helpers/connectData';
import {connect} from 'react-redux';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';

import styles from './Receiver.scss';

import {
  load as loadReceiver,
  edit as editReceiver,
  cancelEditing as cancelEditingReceiver,
  clearState as clearReceiverDetails,
  update as updateReceiver} from 'redux/modules/models/receivers/details';

import { FormControls, Button, Glyphicon, ButtonToolbar} from 'react-bootstrap';
import {ReceiverForm} from 'components';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];
  promises.push(dispatch(loadReceiver(getState())));

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    receiver: state.models.receivers.details
  }),
  dispatch => bindActionCreators(
    {...routerActions, editReceiver, cancelEditingReceiver, clearReceiverDetails, updateReceiver}, dispatch)
)
export default class Receiver extends Component {

  static propTypes = {
    receiver: PropTypes.object,
    editReceiver: PropTypes.func,
    cancelEditingReceiver: PropTypes.func,
    clearReceiverDetails: PropTypes.func,
    pushState: PropTypes.func,
    createReceiver: PropTypes.func,
    updateReceiver: PropTypes.func,
    params: PropTypes.object,
  }

  componentWillUnmount() {
    const {clearReceiverDetails} = this.props;

    clearReceiverDetails();
  }

  handleSubmit = (data) => {
    const {updateReceiver, params: {receiverId}} = this.props;
    updateReceiver(receiverId, data).then((res) => {
      // success
      if (res.error) {
        // handle error
      } else {
        // handle success
        window.location.reload();
      }
    });
  };

  render() {
    const {receiver, editReceiver, cancelEditingReceiver} = this.props;

    return (
      <div className="panel-body">
        <div className="row">
          <div className="col-lg-12">
            <div className="ibox float-e-margins">
              <div className="ibox-title">
                <h5>Basic info</h5>
              </div>
              <div className="ibox-content p-md">
                <div>
                  {!receiver.editing &&
                  <form className="form-horizontal">
                    <FormControls.Static label="First name" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.firstName}</FormControls.Static>
                    <FormControls.Static label="Last name" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.lastName}</FormControls.Static>
                    <FormControls.Static label="Email" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.email}</FormControls.Static>
                    <FormControls.Static label="Mobile phone" labelClassName="col-xs-2" wrapperClassName="col-xs-10">{receiver.data.mobilePhone}</FormControls.Static>
                    <div className="hr-line-dashed"></div>
                    <div className="row">
                      <div className="col-xs-offset-2 col-xs-10">
                        <ButtonToolbar>
                          <Button onClick={editReceiver}><Glyphicon glyph="edit" /> &nbsp;Edit</Button>
                        </ButtonToolbar>
                      </div>
                    </div>
                  </form>
                  }
                  {receiver.editing &&
                    <ReceiverForm onSubmit={this.handleSubmit} handleCancel={cancelEditingReceiver}/>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
