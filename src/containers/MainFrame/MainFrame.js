/**
 * Created by rokfortuna on 3/24/16.
 */

import React, { Component, PropTypes } from 'react';
import styles from './MainFrame.scss';
import {connect} from 'react-redux';
import connectData from 'helpers/connectData';
import * as routerActions from 'redux-router';
import {bindActionCreators} from 'redux';
import cookie from 'react-cookie';
import _ from 'lodash';

import {isDataLoaded as isProfileDataLoaded, loadData as loadProfileData, resetProfileState} from 'redux/modules/global/profile';

import {SideMenu, TopMenu} from 'components';
import {logout} from 'redux/modules/authentication';

function fetchDataDeferred(getState, dispatch) {
  const promises = [];

  if (!isProfileDataLoaded(getState())) {
    promises.push(dispatch(loadProfileData()));
  }

  return Promise.all(promises);
}

@connectData(null, fetchDataDeferred)
@connect(
  state => ({
    token: state.authentication.token,
    profile: state.global.profile,
    routerLocation: state.router.location.pathname
  }),
  dispatch => bindActionCreators(
    {...routerActions, logout, resetProfileState}, dispatch)
)
export default class MainFrame extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    logout: PropTypes.func,
    profile: PropTypes.object,
    pushState: PropTypes.func,
    token: PropTypes.string,
    resetProfileState: PropTypes.func,
    routerLocation: PropTypes.string
  };

  // TODO refactor: redirect if token is not present anymore
  componentDidUpdate(prevProps, prevState) {
    const {token, pushState} = this.props;

    if(prevProps.token && !token) {
      cookie.remove('auth_token', { path: '/' });
      cookie.remove('refresh_token', { path: '/' });
      cookie.remove('token_expires_at', { path: '/' })
      cookie.remove('profile_type', { path: '/' });
      cookie.remove('profile_id', { path: '/' });
      pushState(null, '/login');
    }

  }

  logout = () => {
    const {logout, resetProfileState} = this.props;
    resetProfileState();
    logout();
  };

  handleSelectProfile = (value) => {
    const profileOptions = this.props.profile.options;
    const selectedOption = _.find(profileOptions, function(option) { return option.id === value });

    cookie.save('profile_type', selectedOption.type, { path: '/' });
    cookie.save('profile_id', selectedOption.id, { path: '/' });

    if (value === selectedOption.id) {
      window.location.replace('/app/user/tenants');
    } else {
      window.location.replace('/app');
    }
  };

  render() {
    const { profile: {options, type, id}, profileType, pushState, routerLocation } = this.props;

    const showSideMenu = type !== 'user';

    return (
      <div>
        { showSideMenu &&
        <div id="wrapper">

          <SideMenu pushState={pushState} routerLocation={routerLocation}/>

          <div id="page-wrapper" className="gray-bg">

            <TopMenu
              logout={this.logout}
              profileOptions={options}
              onSelectProfile={this.handleSelectProfile}
              selectedProfileId={id}
              showSideMenu={showSideMenu}
            />

            <div className={`${styles.appContent}`}>
              {this.props.children}
            </div>

            <div className="footer">
              <div>
                <strong>Copyright</strong> KumuluzEE &copy; 2014-2016
              </div>
            </div>
          </div>
        </div>
        }
        { !showSideMenu &&
        <div className={`${styles.wrapperNoSideMenu} gray-bg`}>

          <TopMenu
            logout={this.logout}
            profileOptions={options}
            onSelectProfile={this.handleSelectProfile}
            selectedProfileId={id}
            showSideMenu={showSideMenu}
          />

          <div className={`${styles.appContent}`}>
            {this.props.children}
          </div>

          <div className="footer">
            <div>
              <strong>Copyright</strong> KumuluzEE &copy; 2014-2016
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}
