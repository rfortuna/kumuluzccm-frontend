import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
  App,
  RouterSecureContainer,
  MainFrame,

  NotFound,
  NotAuthorized,

  Campaigns,
  CampaignNew,
  Campaign,
  CampaignBasic,
  CampaignStatistics,
  CampaignLive,
  CampaignNewsletters,
  CampaignTemplates,

  TemplateNew,

  Newsletter,
  NewsletterNew,
  Newsletters,

  ProductNew,
  Products,
  Platforms,
  PlatformNew,
  Platform,

  Receiver,
  ReceiverNew,
  Receivers,

  Dashboard,
  DashboardBasic,
  DashboardLive,
  DashboardHistory,

  SendingBase,
  SendingChannelSetup,
  SendingReceivers,
  SendingSetup,
  SendingContent,
  SendingConfirm,

  Lists,
  List,
  ListNew,

  Settings,

  Login,

  LandingPage,

  UserBase,
  UserSubscriptions,
  UserMessages,
  UserTenants,
  UserAdmin,
  SubscriptionInfo,
  SubscriptionPreferencesNew,
  SubscriptionPreferencesUpdate

  } from 'containers';

export default (store) => {

  const requireLogin = (nextState, replaceState, callback) => {
    const { authentication: { token }} = store.getState();
    if (!token) {
      console.log('redirect to login');
      replaceState({nextPathname: nextState.location.pathname}, '/login');
    }
    callback();
  }

  const redirectIfLoggedIn = (nextState, replaceState, callback) => {
    const { authentication: { token }} = store.getState();
    if (token) {
      replaceState({nextPathname: nextState.location.pathname}, '/app/tenant');
    }
    callback();
  };

  const requireUserSelected = (nextState, replaceState, callback) => {
    const selectedProfileType = store.getState().global.profile.type;
    if (selectedProfileType !== 'user') {
      replaceState({nextPathname: nextState.location.pathname}, '/app/tenant');
    }
    callback();
  };

  const requireTenantSelected = (nextState, replaceState, callback) => {
    const selectedProfileType = store.getState().global.profile.type;
    if (selectedProfileType !== 'tenant') {
      replaceState({nextPathname: nextState.location.pathname}, '/app/user/tenants');
    }
    callback();
  };


  return (

    <Route path="/" component={App}>

      <IndexRoute component={LandingPage} />

      {/* Pages that require login. */}
      <Route path="app" component={MainFrame} onEnter={requireLogin}>

        <Route path="tenant" component={RouterSecureContainer} onEnter={requireTenantSelected}>

          <Route component={Dashboard}>
            <IndexRoute component={DashboardBasic}/>
            <Route path="history" component={DashboardHistory} />
            <Route path="live" component={DashboardLive} />
          </Route>

          <Route path="campaigns" component={Campaigns}/>
          <Route path="campaigns/new" component={CampaignNew}/>
          <Route path="campaigns/:campaignId" component={Campaign}>
            <IndexRoute component={CampaignBasic} />
            <Route path="statistics" component={CampaignStatistics} />
            <Route path="live" component={CampaignLive} />
            <Route path="newsletters" component={CampaignNewsletters} />
            <Route path="templates" component={CampaignTemplates} />
          </Route>

          <Route path="templates/new" component={TemplateNew} />

          <Route path="newsletter/new" component={NewsletterNew}/>
          <Route path="campaigns/:campaignId/newsletters/:newsletterId" component={Newsletter}/>
          <Route path="receivers" component={Receivers} />
          <Route path="receivers/new" component={ReceiverNew} />
          <Route path="receivers/:receiverId" component={Receiver}/>


          <Route path="newsletters/:newsletterId" component={Newsletter}/>
          <Route path="newsletters" component={Newsletters}/>

          <Route path="sending" component={SendingBase}>
            <Route path="channelSetup" component={SendingChannelSetup} />
            <Route path="receivers" component={SendingReceivers} />
            <Route path="setup" component={SendingSetup} />
            <Route path="content" component={SendingContent} />
            <Route path="confirm" component={SendingConfirm} />
          </Route>

          <Route path="lists" component={Lists}/>
          <Route path="lists/new" component={ListNew}/>
          <Route path="lists/:listId" component={List}/>

          <Route path="products" component={Products} />
          <Route path="products/new" component={ProductNew}/>
          <Route path="products/:productId/platforms" component={Platforms} />
          <Route path="products/newPlatform" component={PlatformNew} />
          <Route path="products/:productId/platforms/:platformId" component={Platform} />

          <Route path="settings" component={Settings} />

        </Route>

        {/* Pages without left menu */}
        <Route path="user" component={RouterSecureContainer} onEnter={requireUserSelected}>

          <Route component={UserBase}>
            <Route path="messages"component={UserMessages} />
            <Route path="subscriptions" component={UserSubscriptions} />
            <Route path="tenants" component={UserTenants} />
            <Route path="admin" component={UserAdmin} />
          </Route>

          <Route path="subscriptions/:subscriptionId" component={SubscriptionInfo} />
          <Route path="subscriptions/:subscriptionId/preferences/new" component={SubscriptionPreferencesNew} />
          <Route path="subscriptions/:subscriptionId/preferences/:preferenceId" component={SubscriptionPreferencesUpdate} />
        </Route>

      </Route>

      {/* Pages that do not require login. */}
      <Route path="login" component={Login} onEnter={redirectIfLoggedIn}/>
      <Route path="notAuthorized" component={NotAuthorized} status={401} />
      <Route path="*" component={NotFound} status={404} />

    </Route>

  );
};
